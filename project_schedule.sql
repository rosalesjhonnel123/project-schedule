-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 27, 2022 at 01:34 PM
-- Server version: 10.3.35-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `statutor_SignsDb22o3`
--

-- --------------------------------------------------------

--
-- Table structure for table `ds03_custom_signs`
--

CREATE TABLE `ds03_custom_signs` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `custom_sign_name` varchar(255) NOT NULL,
  `plate_size_id` int(11) NOT NULL,
  `finish_material_id` int(11) NOT NULL,
  `writing_option_id` int(11) NOT NULL,
  `image` text DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `is_approved` int(11) DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ds03_finish_material`
--

CREATE TABLE `ds03_finish_material` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ds03_finish_material`
--

INSERT INTO `ds03_finish_material` (`id`, `name`, `status`) VALUES
(1, 'BLACK', 1),
(2, 'BRUSHED SILVER', 1),
(8, 'RED', 1),
(9, 'WHITE', 1),
(10, 'YELLOW', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ds03_finish_material_templates`
--

CREATE TABLE `ds03_finish_material_templates` (
  `id` int(11) NOT NULL,
  `plate_size_id` int(11) NOT NULL,
  `finish_material_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ds03_finish_material_templates`
--

INSERT INTO `ds03_finish_material_templates` (`id`, `plate_size_id`, `finish_material_id`, `image`, `status`) VALUES
(1, 1, 1, '1655090476.png', 1),
(2, 7, 1, '1655090492.png', 1),
(3, 8, 1, '1655090510.png', 1),
(4, 9, 1, '1655090528.png', 1),
(5, 10, 1, '1655090549.png', 1),
(6, 11, 1, '1655090562.png', 1),
(7, 12, 1, '1655090577.png', 1),
(8, 13, 1, '1655090588.png', 1),
(9, 14, 1, '1655090601.png', 1),
(10, 15, 1, '1655090613.png', 1),
(11, 16, 1, '1655090623.png', 1),
(12, 17, 1, 'default.png', 1),
(13, 18, 1, '1655090676.png', 1),
(14, 19, 1, '1655090693.png', 1),
(15, 25, 1, '1655090704.png', 1),
(16, 1, 8, '1655090732.png', 1),
(17, 7, 8, '1655090741.png', 1),
(18, 8, 8, '1655090755.png', 1),
(19, 9, 8, '1655090765.png', 1),
(20, 10, 8, '1655090776.png', 1),
(21, 11, 8, '1655090787.png', 1),
(22, 12, 8, '1655090796.png', 1),
(23, 13, 8, '1655090807.png', 1),
(24, 14, 8, '1655090817.png', 1),
(25, 15, 8, '1655090828.png', 1),
(26, 16, 8, '1655090839.png', 1),
(27, 17, 8, '1655090859.png', 1),
(28, 18, 8, '1655090870.png', 1),
(29, 19, 8, '1655090881.png', 1),
(30, 25, 8, '1655090890.png', 1),
(31, 1, 9, '1655090912.png', 1),
(32, 7, 9, '1655090927.png', 1),
(33, 8, 9, '1655091014.png', 1),
(34, 9, 9, '1655091055.png', 1),
(35, 10, 9, '1655091069.png', 1),
(36, 11, 9, '1655091119.png', 1),
(37, 12, 9, '1655091129.png', 1),
(38, 13, 9, '1655091156.png', 1),
(39, 14, 9, '1655091168.png', 1),
(40, 15, 9, '1655091184.png', 1),
(41, 16, 9, '1655091192.png', 1),
(42, 17, 9, '1655091206.png', 1),
(43, 18, 9, '1655091216.png', 1),
(44, 19, 9, '1655091226.png', 1),
(45, 25, 9, '1655091242.png', 1),
(46, 1, 10, '1655091287.png', 1),
(47, 7, 10, '1655091297.png', 1),
(48, 8, 10, '1655091306.png', 1),
(49, 9, 10, '1655091318.png', 1),
(50, 10, 10, '1655091328.png', 1),
(51, 11, 10, '1655091338.png', 1),
(52, 12, 10, '1655091352.png', 1),
(53, 13, 10, '1655091362.png', 1),
(54, 14, 10, '1655091371.png', 1),
(55, 15, 10, '1655091381.png', 1),
(56, 16, 10, '1655091391.png', 1),
(57, 17, 10, '1655091403.png', 1),
(58, 18, 10, '1655091414.png', 1),
(59, 19, 10, '1655091422.png', 1),
(60, 25, 10, '1655091431.png', 1),
(61, 1, 2, '1655253820.jpg', 1),
(62, 7, 2, '1655253832.jpg', 1),
(63, 8, 2, '1655253847.jpg', 1),
(64, 9, 2, '1655253858.jpg', 1),
(65, 10, 2, '1655253869.jpg', 1),
(66, 11, 2, '1655253879.jpg', 1),
(67, 12, 2, '1655253886.jpg', 1),
(68, 13, 2, '1655253896.jpg', 1),
(69, 14, 2, '1655253907.jpg', 1),
(70, 15, 2, '1655253914.jpg', 1),
(71, 16, 2, '1655253924.jpg', 1),
(72, 17, 2, '1655253940.jpg', 1),
(73, 18, 2, '1655253956.jpg', 1),
(74, 19, 2, '1655253967.jpg', 1),
(75, 25, 2, '1655253977.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ds03_levels`
--

CREATE TABLE `ds03_levels` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ds03_levels`
--

INSERT INTO `ds03_levels` (`id`, `name`, `status`) VALUES
(1, 'Basement 1', 1),
(2, 'Basement 2', 1),
(3, 'Basement 3', 1),
(4, 'Ground', 1),
(5, 'Level 1', 1),
(6, 'Level 2', 1),
(7, 'Level 3', 1),
(8, 'Level 4', 1),
(9, 'Level 5', 1),
(10, 'Level 6', 1),
(11, 'Level 7', 1),
(12, 'Level 8', 1),
(13, 'Level 9', 1),
(14, 'Level 10', 1),
(15, 'Level 11', 1),
(16, 'Level 12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ds03_level_signs`
--

CREATE TABLE `ds03_level_signs` (
  `project_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `sign_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `is_custom` int(11) DEFAULT 0,
  `location` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ds03_permissions`
--

CREATE TABLE `ds03_permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `view_name` varchar(255) NOT NULL,
  `is_admin` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ds03_permissions`
--

INSERT INTO `ds03_permissions` (`id`, `name`, `view_name`, `is_admin`) VALUES
(1, 'view project list', 'projects', 0),
(2, 'edit project', 'projects.edit', 0),
(3, 'create project', 'projects.create', 0),
(4, 'view project', 'projects.view', 0),
(5, 'view project quantities', 'projects.view_quantities', 0),
(6, 'print project', 'projects.print', 0),
(7, 'view customer list', 'customers', 1),
(8, 'edit customer', 'customers.update', 1),
(9, 'create customer', 'customers.create', 1),
(10, 'view customer', 'customers.getByid', 1),
(11, 'view level list', 'levels', 1),
(12, 'edit level', 'levels.update', 1),
(13, 'create level', 'levels.create', 1),
(14, 'view level', 'levels.getByid', 1),
(15, 'view plate size list', 'platesizes', 1),
(16, 'edit plate size', 'platesizes.update', 1),
(17, 'create plate size', 'platesizes.create', 1),
(18, 'view plate size', 'platesizes.getByid', 1),
(19, 'view finish material list', 'finishmaterials', 1),
(20, 'edit finish material', 'finishmaterials.update', 1),
(21, 'create finish material', 'finishmaterials.create', 1),
(22, 'view finish material', 'finishmaterials.getByid', 1),
(23, 'view writing option list', 'writingoptions', 1),
(24, 'edit writing option', 'writingoptions.update', 1),
(25, 'create writing option', 'writingoptions.create', 1),
(26, 'view writing option', 'writingoptions.getByid', 1),
(27, 'view finish material template list', 'finishmaterialtemplates', 1),
(28, 'edit finish material template', 'finishmaterialtemplates.edit', 1),
(29, 'create finish material template', 'finishmaterialtemplates.create', 1),
(30, 'view finish material template', 'finishmaterialtemplates.getByid', 1),
(31, 'view custom sign list', 'customsigns', 0),
(32, 'edit custom sign', 'customsigns.update', 0),
(33, 'create custom sign', 'customsigns.create', 0),
(34, 'view custom sign', 'customsigns.getByid', 0),
(35, 'approved custom sign', 'customsigns.update_status', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ds03_plate_size`
--

CREATE TABLE `ds03_plate_size` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ds03_plate_size`
--

INSERT INTO `ds03_plate_size` (`id`, `name`, `status`) VALUES
(1, '400MM X 75MM', 1),
(7, '400MM X 100MM', 1),
(8, '450MM X 75MM', 1),
(9, '450MM X 100MM', 1),
(10, '450MM X 150MM', 1),
(11, '500MM X 75MM', 1),
(12, '500MM X 100MM', 1),
(13, '550MM X 75MM', 1),
(14, '550MM X 100MM', 1),
(15, '600MM X 100MM', 1),
(16, '600MM X 200MM', 1),
(17, '297MM X 420MM', 1),
(18, '297MM X 210MM', 1),
(19, '500MM X 200MM', 1),
(25, '420MM X 297MM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ds03_projects`
--

CREATE TABLE `ds03_projects` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_address` varchar(255) NOT NULL,
  `is_approved` int(11) DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0,
  `comments` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ds03_project_levels`
--

CREATE TABLE `ds03_project_levels` (
  `project_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ds03_user_permissions`
--

CREATE TABLE `ds03_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ds03_writing_option`
--

CREATE TABLE `ds03_writing_option` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ds03_writing_option`
--

INSERT INTO `ds03_writing_option` (`id`, `name`, `status`) VALUES
(1, 'DIRECT UV PRINTED - STANDARD', 1),
(79, 'ENGRAVED', 1),
(80, 'VINYL LETTERING', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ds03_custom_signs`
--
ALTER TABLE `ds03_custom_signs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ds03_finish_material`
--
ALTER TABLE `ds03_finish_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ds03_finish_material_templates`
--
ALTER TABLE `ds03_finish_material_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ds03_levels`
--
ALTER TABLE `ds03_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ds03_permissions`
--
ALTER TABLE `ds03_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ds03_plate_size`
--
ALTER TABLE `ds03_plate_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ds03_projects`
--
ALTER TABLE `ds03_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ds03_writing_option`
--
ALTER TABLE `ds03_writing_option`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ds03_custom_signs`
--
ALTER TABLE `ds03_custom_signs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ds03_finish_material`
--
ALTER TABLE `ds03_finish_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `ds03_finish_material_templates`
--
ALTER TABLE `ds03_finish_material_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `ds03_levels`
--
ALTER TABLE `ds03_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `ds03_permissions`
--
ALTER TABLE `ds03_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `ds03_plate_size`
--
ALTER TABLE `ds03_plate_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `ds03_projects`
--
ALTER TABLE `ds03_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ds03_writing_option`
--
ALTER TABLE `ds03_writing_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
