<?php
class Finishmaterials extends Controller{
	
	function __construct(){
		parent::__construct();
		$this->finishmaterialmodel = new finishmaterialmodel();
		$this->permissionsmodel = new permissionsmodel();
		$this->customsignsmodel = new customsignsmodel();
		$this->finishmaterialtemplatesmodel = new finishmaterialtemplatesmodel();
		$this->authmodel = new authmodel();
		$this->authmodel->isLoggedIn();
		$this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
		$this->has_permission = $this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
	}

	public function index(){
		$status = $this->input->get('status');
		$role = $this->permissionsmodel->get_role_by_id(get_current_user_id());
		$this->view->assign('title','Finish Materials');
		$this->view->assign('role', $role);
		$this->view->assign('status', $status);
		$this->view->display('admin/finishmaterials/index');
	}

	public function allFinishMaterials(){
		$status = $this->input->get('status') ?? 'active';
		$list = $this->finishmaterialmodel->allFinishMaterials($status);
		echo json_encode(array("data" => $list));
	}

	public function getByID(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->get('id');
		$result = $this->finishmaterialmodel->getByID($id);
		echo json_encode(array($result));

	}

	public function update(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->post('id');
		$name = $this->input->post('finish_material_name');
		$status = $this->input->post('status');

		$finishmaterial = $this->finishmaterialtemplatesmodel->getByFinishMaterialID($id);
		$finishmaterial2 = $this->customsignsmodel->getByFinishMaterialID($id);

		if (!empty($finishmaterial) && !empty($finishmaterial2)) {
			$arr = array('err' => 1, 'msg' => 'We cannot continue this process. This finish material is use by another feature.');	
			echo json_encode($arr);
			die();
		}

		if ($name === '') {
			$arr = array('err' => 1, 'msg' => 'Name is required.');
		} else {
			$data = array('name' => $name, 'status' => $status);

			$result = $this->finishmaterialmodel->update($id,$data);

			if ( is_wp_error( $result  ) ) {
			    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Finish Material was successfully updated.');
			}
		}
		echo json_encode($arr);
	}

	public function create(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$name = $this->input->post('finish_material_name') ?? '';
		
		if ($name === '') {
			$arr = array('err' => 1, 'msg' => 'Name is required.');
		} else {
			$data = array('name' => $name, 'status' => 1);

			$result = $this->finishmaterialmodel->create($data);

			if( is_wp_error( $result  ) ) {
			    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Finish Material was successfully added.');
			}	
		}
		
		echo json_encode($arr);
	}

	public function delete(){
		$id = $this->input->post('id');
		$finishmaterial = $this->finishmaterialtemplatesmodel->getByFinishMaterialID($id);
		$finishmaterial2 = $this->customsignsmodel->getByFinishMaterialID($id);

		if (empty($finishmaterial) && empty($finishmaterial2)) {
			$delete = $this->finishmaterialmodel->delete($id);
			if( is_wp_error( $delete  ) ) {
		    	$arr = array('err' => 1, 'msg' => $delete->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Finish Material was deleted successfully.');
			}
			echo json_encode($arr);
			die();
		} else {
			$arr = array('err' => 1, 'msg' => 'We cannot continue this process. This finish material is use by another feature.');	
		}
		echo json_encode($arr);
	}
}

?>