<?php

require 'vendor/autoload.php';
require_once 'vendor/fpdf/fpdf/original/fpdf.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use Dompdf\Dompdf;
use Dompdf\Options;

class Projects extends Controller{	
	function __construct(){
		parent::__construct();
		$this->projectsmodel = new projectsmodel();
		$this->levelsmodel = new levelsmodel();
		$this->levelsignsmodel = new levelsignsmodel();
		$this->projectlevelsmodel = new projectlevelsmodel();
		$this->permissionsmodel = new permissionsmodel();
		$this->customersmodel = new customersmodel();
		$this->customsignsmodel = new customsignsmodel();
		$this->finishmaterialmodel = new finishmaterialmodel();
		$this->platesizemodel = new platesizemodel();
		$this->writingoptionmodel = new writingoptionmodel();
		$this->authmodel = new authmodel();
		$this->authmodel->isLoggedIn();
		$this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
		$this->has_permission = $this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
	}

	public function index()
	{
		$is_admin = $this->permissionsmodel->get_role_by_id(get_current_user_id()) == 'administrator' ? true : false;
		$role = $this->permissionsmodel->get_role_by_id(get_current_user_id());
		$status = $this->input->get('status');
		$arr = ['err' => 0, 'msg' => ''];
		if($this->session->get_userdata('error') == true)
		{
			
			$arr = [
				'err' => 1, 
				'msg' => $this->session->get_userdata('msg')
			];
			$this->session->unset_userdata('error');
			$this->session->unset_userdata('msg');
		}

		$this->view->assign('title','Project List');
		$this->view->assign('error', $arr);
		$this->view->assign('is_admin', $is_admin);
		$this->view->assign('role', $role);
		$this->view->assign('status', $status);
		$this->view->display('admin/projects/index');
	}

	public function getByID(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->get('id');
		$result = $this->projectsmodel->getProjectByID($id);
		echo json_encode(array($result));

	}

	public function allProjects()
	{
		$status = $this->input->get('status') ?? 'active';
		$customer_id = $this->permissionsmodel->get_role_by_id(get_current_user_id()) == 'project_customer' ? get_current_user_id() : ''; 	
		$list = $this->projectsmodel->allProjects($customer_id, $status);
		echo json_encode(['data' => $list]);
	}

	public function getProjectsByCustomerId()
	{
		$id = $this->input->get('id');
		$list = $this->projectsmodel->allActiveProjects($id);
		echo json_encode(['data' => $list]);
	}


	public function create()
	{	
		$customers = [];
		$list = $this->levelsmodel->allActiveLevels();
		$plate_sizes = $this->platesizemodel->allActivePlateSizes();
		$finish_materials = $this->finishmaterialmodel->allActiveFinishMaterials();
		$writing_options = $this->writingoptionmodel->allActiveWritingOptions();
		
		if ($this->permissionsmodel->get_role_by_id(get_current_user_id()) == 'administrator') {
			$customers = $this->customersmodel->allCustomers();
		}

		$this->view->assign('title','Create Project');
		$this->view->assign('plate_sizes', $plate_sizes);
		$this->view->assign('finish_materials', $finish_materials);
		$this->view->assign('writing_options', $writing_options);
		$this->view->assign('levels', $list);

		$this->view->assign('customers', $customers);
		$this->view->assign('role', $this->permissionsmodel->get_role_by_id(get_current_user_id()));
		$this->view->display('admin/projects/create');
	}

	public function getCategories()
	{
		$msg = '';
		if (isset($_GET['current_page'])) {
	        $page = $_GET['current_page'];
	        $cur_page = $page;
	        $page -= 1;
	        // Set the number of results to display
	        $per_page = 30;
	        $previous_btn = true;
	        $next_btn = true;
	        $first_btn = true;
	        $last_btn = true;
	        $start = $page * $per_page;

			$taxonomy     = 'product_cat';
			$orderby      = 'name';  
			$show_count   = 0;      // 1 for yes, 0 for no
			$pad_counts   = 0;      // 1 for yes, 0 for no
			$hierarchical = 1;      // 1 for yes, 0 for no  
			$title        = '';  
			$empty        = 0;

			$args = array(
	         	'taxonomy'     => $taxonomy,
	         	'orderby'      => $orderby,
	         	'show_count'   => $show_count,
	         	'pad_counts'   => $pad_counts,
	         	'hierarchical' => $hierarchical,
	         	'title_li'     => $title,
	         	'hide_empty'   => $empty,
	         	'number' => $per_page,
			    'offset' => $start,
			);

			$categories = get_categories( $args );

			$args2 = array(
			    'taxonomy'     => $taxonomy,
	         	'orderby'      => $orderby,
	         	'show_count'   => $show_count,
	         	'pad_counts'   => $pad_counts,
	         	'hierarchical' => $hierarchical,
	         	'title_li'     => $title,
	         	'hide_empty'   => $empty,
			);

			$total_categories = get_categories($args2) ? count(get_categories($args2)) : 0;

			if ($total_categories > 0) {
				$msg .= '<h3 class="text-center" style="margin-bottom:30px;">SELECT CATEGORY</h3><div class="col-md-4 col-sm-4 sign-container"><a href="javascript:;" id="0" title="Select this sign" class="select_category" custom="1">Custom Sign</a></div><div class="col-md-4 col-sm-4 sign-container"><a href="javascript:;" title="Add Custom Sign" class="add_custom_sign">Add Custom Sign</a></div>';
			} else {
				$msg .= '<p>No result found.</p>';
			}
			foreach($categories as $category) {
				$msg .= '<div class="col-md-4 col-sm-4 sign-container"><a href="javascript:;" id="'.$category->term_id.'" title="Select this sign" class="select_category" custom="0">'.$category->name.'</a></div>'; 
			}

	        $no_of_paginations = ceil($total_categories / $per_page);

	        if ($cur_page >= 4) {
	            $start_loop = $cur_page - 3;
	            if ($no_of_paginations > $cur_page + 3)
	                $end_loop = $cur_page + 3;
	            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 3) {
	                $start_loop = $no_of_paginations - 3;
	                $end_loop = $no_of_paginations;
	            } else {
	                $end_loop = $no_of_paginations;
	            }
	        } else {
	            $start_loop = 1;
	            if ($no_of_paginations > 4)
	                $end_loop = 4;
	            else
	                $end_loop = $no_of_paginations;
	        }
 
	        $pag_container .= "
	        <div class='woocommerce-pagination' style='margin-top:30px;' id='categories'>
	            <ul class='category-page-numbers page-numbers'>";

	        if ($previous_btn && $cur_page > 1) {
	            $pre = $cur_page - 1;
	            $pag_container .= "<li><a p='$pre' class='page-numbers active' href='javascript:void(0)'>←</a></li>";
	        } else if ($previous_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>←</a></li>";
	        }
	        for ($i = $start_loop; $i <= $end_loop; $i++) {

	            if ($cur_page == $i)
	                $pag_container .= "<li><span p='$i' class='page-numbers inactive'>{$i}</span></li>";
	            else
	                $pag_container .= "<li><a p='$i' class = 'page-numbers active' href='javascript:void(0)'>{$i}</a></li>";
	        }

	        if ($next_btn && $cur_page < $no_of_paginations) {
	            $nex = $cur_page + 1;
	            $pag_container .= "<li><a p='$nex' class='page-numbers active' href='javascript:void(0)'>→</a></li>";
	        } else if ($next_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>→</span></li>";
	        }

	        $pag_container = $pag_container . "
	            </ul>
	        </div>";

	        if ($total_categories == 0) {
	        	$pag_container = '';
	        }
	        
	        echo $msg . $pag_container ;
		}
	}

	public function getBySign()
	{
		$msg = '';
		if (isset($_GET['current_page'])) {
			$search_text = $_GET['cat_name'];
	        $page = $_GET['current_page'];
	        $cur_page = $page;
	        $page -= 1;
	        // Set the number of results to display
	        $per_page = 30;
	        $previous_btn = true;
	        $next_btn = true;
	        $first_btn = true;
	        $last_btn = true;
	        $start = $page * $per_page;

			$taxonomy     = 'product_cat';
			$orderby      = 'name';  
			$show_count   = 0;      // 1 for yes, 0 for no
			$pad_counts   = 0;      // 1 for yes, 0 for no
			$hierarchical = 1;      // 1 for yes, 0 for no  
			$title        = '';  
			$empty        = 0;

			$args = array(
	         	'taxonomy'      => 'product_cat', // taxonomy name
			    'orderby'       => 'id', 
			    'order'         => 'ASC',
			    'hide_empty'    => true,
			    'fields'        => 'all',
			    'name__like'    => $search_text,
	         	'number' => $per_page,
			    'offset' => $start,
			);

			$categories = get_terms( $args );

			$args2 = array(
			    'taxonomy'      => 'product_cat', // taxonomy name
			    'orderby'       => 'id', 
			    'order'         => 'ASC',
			    'hide_empty'    => true,
			    'fields'        => 'all',
			    'name__like'    => $search_text
			);

			$total_categories = get_terms($args2) ? count(get_terms($args2)) : 0;

			if ($total_categories > 0) {
				$msg .= '<h3 class="text-center" style="margin-bottom:30px;">SELECT CATEGORY</h3><div class="col-md-4 col-sm-4 sign-container"><a href="javascript:;" id="0" title="Select this sign" class="select_category" custom="1">Custom Sign</a></div><div class="col-md-4 col-sm-4 sign-container"><a href="javascript:;" title="Add Custom Sign" class="add_custom_sign">Add Custom Sign</a></div>';
			} else {
				$msg .= '<p>No result found.</p>';
			}
			foreach($categories as $category) {
				$msg .= '<div class="col-md-4 col-sm-4 sign-container"><a href="javascript:;" id="'.$category->term_id.'" title="Select this sign" class="select_category" custom="0">'.$category->name.'</a></div>'; 
			}

	        $no_of_paginations = ceil($total_categories / $per_page);

	        if ($cur_page >= 4) {
	            $start_loop = $cur_page - 3;
	            if ($no_of_paginations > $cur_page + 3)
	                $end_loop = $cur_page + 3;
	            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 3) {
	                $start_loop = $no_of_paginations - 3;
	                $end_loop = $no_of_paginations;
	            } else {
	                $end_loop = $no_of_paginations;
	            }
	        } else {
	            $start_loop = 1;
	            if ($no_of_paginations > 4)
	                $end_loop = 4;
	            else
	                $end_loop = $no_of_paginations;
	        }
 
	        $pag_container .= "
	        <div class='woocommerce-pagination' style='margin-top:30px;' id='categories-search'>
	            <ul class='category-page-numbers page-numbers'>";

	        if ($previous_btn && $cur_page > 1) {
	            $pre = $cur_page - 1;
	            $pag_container .= "<li><a p='$pre' class='page-numbers active' href='javascript:void(0)'>←</a></li>";
	        } else if ($previous_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>←</a></li>";
	        }
	        for ($i = $start_loop; $i <= $end_loop; $i++) {

	            if ($cur_page == $i)
	                $pag_container .= "<li><span p='$i' class='page-numbers inactive'>{$i}</span></li>";
	            else
	                $pag_container .= "<li><a p='$i' class = 'page-numbers active' href='javascript:void(0)'>{$i}</a></li>";
	        }

	        if ($next_btn && $cur_page < $no_of_paginations) {
	            $nex = $cur_page + 1;
	            $pag_container .= "<li><a p='$nex' class='page-numbers active' href='javascript:void(0)'>→</a></li>";
	        } else if ($next_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>→</span></li>";
	        }

	        $pag_container = $pag_container . "
	            </ul>
	        </div>";

	        if ($total_categories == 0) {
	        	$pag_container = '';
	        }
	        
	        echo $msg . $pag_container ;
		}
	}

	public function store()
	{
		$data['project_name'] = $_POST['project_name'];
		$data['project_address'] = $_POST['project_address'];
		$data['status'] = 1;
		$data['customer_id'] = $_POST['customer_id'] ?? get_current_user_id();

		if ($data['project_name'] == '') {
			$arr = array('err' => 1, 'msg' => 'Project name is required.');
			echo json_encode($arr);
			die();
		}

		$project = $this->projectsmodel->createProject($data);

		if( is_wp_error( $project  ) ) {
		    $arr = array('err' => 1, 'msg' => $project->get_error_message());		
		}
		else {
			$this->customsignsmodel->updateProjectId($data['customer_id'], ['project_id' => $project]);
			$levels = $_POST['level'];
			$level_signs = explode(',', $_POST['level_signs']);
			$project_levels = [];
			$project_level_signs = [];
			foreach ($levels as $key => $value) {
				if (!isset($project_levels[$project.$value])) {
					$level['project_id'] = $project;
					$level['level_id'] = $value;		
					$this->projectlevelsmodel->create($level);
					$project_levels[$project.$value] = 1;
				} else {
					$project_levels[$project.$value] += 1;
				}

				$signs = explode('-', $level_signs[$key]);
				foreach ($signs as $k => $v) {
					$sign = explode(':', $signs[$k]);
	                $sign_info = explode('custom', $sign[0]);
	                $qty = $sign[1];
	                $location = explode('location', $sign_info[1]);
	                $level_sign['project_id'] = $project;
	                $level_sign['level_id'] = $value;
	                $level_sign['sign_id'] = $sign_info[0];
	                $level_sign['is_custom'] = $location[0];
	                $level_sign['quantity'] = $qty;
	                $level_sign['location'] = $location[1];

					$result =$this->levelsignsmodel->getByProjectIdLevelIdSignId($project_id, $value, $sign_info[0], $location[0]);
					if ($result) {
						$level_sign['quantity'] = $qty + $result->quantity;
						$this->levelsignsmodel->update($project, $value, $sign_info[0], $level_sign);
					} else {
						$this->levelsignsmodel->create($level_sign);
					}
				}
			}
			$arr = array('err' => 0, 'msg' => 'Project was successfully added.');
		}

		echo json_encode($arr);
	}

	public function searchProductsByCategory()
	{
		$msg = '';
		if (isset($_GET['current_page'])) {
	        $page = $_GET['current_page'];
	        $cur_page = $page;
	        $page -= 1;
	        // Set the number of results to display
	        $per_page = 3;
	        $previous_btn = true;
	        $next_btn = true;
	        $first_btn = true;
	        $last_btn = true;
	        $start = $page * $per_page;

			$category = $_GET['category_id'];
			$args = array(
			    'post_type' => 'product',
			    'posts_per_page'        => $per_page,
			    'offset'            	=> $start,
			    'tax_query'             => array(
			        array(
			            'taxonomy'  => 'product_cat',
			            'field'     => 'term_id',
			            'terms'     => array($category),
			            'operator'  => 'IN',
			        )
			    ),
			);

			$products = get_posts($args);

			$args2 = array(
			    'post_type' => 'product',
			    'posts_per_page'        => -1,
			    'tax_query'             => array(
			        array(
			            'taxonomy'  => 'product_cat',
			            'field'     => 'term_id',
			            'terms'     => array($category),
			            'operator'  => 'IN',
			        )
			    ),
			);

			$total_products = get_posts($args2);

			if (count($total_products) > 0) {
				$msg .= '<h3 class="text-center" style="margin-bottom:30px;">SELECT SIGNS</h3>';
			} else {
				$msg .= '<p>No result found.</p>';
			}
			foreach($products as $product) {
				$img = '';
				$img_content = '';
				if (has_post_thumbnail( $product->ID ) ) {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'woocommerce_thumbnail' );
					$img = $image[0];
				}
				if ($img) {
					$image_content = '<div class="product-image mt-3"><img src="'.$img.'" width="250" height="139" style="width:100%;max-width:250px;height: auto;display:block;margin: 0px auto 8px;"></div>';
				}

				$msg .= '<div class="col-md-4 col-sm-4 sign-container">'.$image_content.'<a href="javascript:void(0)" id="'.$product->ID.'" class="select_sign" is-custom="0">'.$product->post_title.'</a><span class="product-span d-block"><p>'.$product->post_excerpt.'</p></span></div>'; 
			}

	        $no_of_paginations = ceil(count($total_products) / $per_page);

	        if ($cur_page >= 4) {
	            $start_loop = $cur_page - 3;
	            if ($no_of_paginations > $cur_page + 3)
	                $end_loop = $cur_page + 3;
	            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 3) {
	                $start_loop = $no_of_paginations - 3;
	                $end_loop = $no_of_paginations;
	            } else {
	                $end_loop = $no_of_paginations;
	            }
	        } else {
	            $start_loop = 1;
	            if ($no_of_paginations > 4)
	                $end_loop = 4;
	            else
	                $end_loop = $no_of_paginations;
	        }
 
	        $pag_container .= "
	        <div class='woocommerce-pagination' style='margin-top:30px;' id='product-categories'>
	            <ul class='product-page-numbers page-numbers'>";

	        if ($previous_btn && $cur_page > 1) {
	            $pre = $cur_page - 1;
	            $pag_container .= "<li><a p='$pre' class='page-numbers active' href='javascript:void(0)' ino='{$category}'>←</a></li>";
	        } else if ($previous_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>←</a></li>";
	        }
	        for ($i = $start_loop; $i <= $end_loop; $i++) {

	            if ($cur_page == $i)
	                $pag_container .= "<li><span p='$i' class='page-numbers inactive'>{$i}</span></li>";
	            else
	                $pag_container .= "<li><a p='$i' class = 'page-numbers active' href='javascript:void(0)' ino='{$category}'>{$i}</a></li>";
	        }

	        if ($next_btn && $cur_page < $no_of_paginations) {
	            $nex = $cur_page + 1;
	            $pag_container .= "<li><a p='$nex' class='page-numbers active' href='javascript:void(0)' ino='{$category}'>→</a></li>";
	        } else if ($next_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>→</span></li>";
	        }

	        $pag_container = $pag_container . "
	            </ul>
	        </div>";

	        if (count($total_products) == 0) {
	        	$pag_container = '';
	        }

	        echo $msg . $pag_container ;
		}
	}

	public function searchProductsByName()
	{
		$msg = '';
		if (isset($_GET['current_page'])) {
	        $page = $_GET['current_page'];
	        $cur_page = $page;
	        $page -= 1;
	        // Set the number of results to display
	        $per_page = 3;
	        $previous_btn = true;
	        $next_btn = true;
	        $first_btn = true;
	        $last_btn = true;
	        $start = $page * $per_page;

	        $category = $_GET['category_id'];
			$search_text = $_GET['product_name'];
			$args = array(
				's'         => $search_text,
			    'post_type' => 'product',
			    'posts_per_page'        => $per_page,
			    'offset'            	=> $start,
			    'tax_query'             => array(
			        array(
			            'taxonomy'  => 'product_cat',
			            'field'     => 'term_id',
			            'terms'     => array($category),
			            'operator'  => 'IN',
			        )
			    ),
			);

			$products = get_posts($args);

			$args2 = array(
				's'         => $search_text,
			    'post_type' => 'product',
			    'posts_per_page'        => -1,
			    'tax_query'             => array(
			        array(
			            'taxonomy'  => 'product_cat',
			            'field'     => 'term_id',
			            'terms'     => array($category),
			            'operator'  => 'IN',
			        )
			    ),
			);

			$total_products = get_posts($args2);

			if (count($total_products) > 0) {
				$msg .= '<h3 class="text-center" style="margin-bottom:30px;">SELECT SIGNS</h3>';
			} else {
				$msg .= '<p>No result found.</p>';
			}
			foreach($products as $product) {
				$img = '';
				$img_content = '';
				if (has_post_thumbnail( $product->ID ) ) {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'woocommerce_thumbnail' );
					$img = $image[0];
				}
				if ($img) {
					$image_content = '<div class="product-image mt-3"><img src="'.$img.'" width="250" height="139" style="width:100%;max-width:250px;height: auto;display:block;margin: 0px auto 8px;"></div>';
				}

				$msg .= '<div class="col-md-4 col-sm-4 sign-container">'.$image_content.'<a href="javascript:void(0)" id="'.$product->ID.'" class="select_sign" is-custom="0">'.$product->post_title.'</a><span class="product-span d-block"><p>'.$product->post_excerpt.'</p></span></div>'; 
			}

	        $no_of_paginations = ceil(count($total_products) / $per_page);

	        if ($cur_page >= 4) {
	            $start_loop = $cur_page - 3;
	            if ($no_of_paginations > $cur_page + 3)
	                $end_loop = $cur_page + 3;
	            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 3) {
	                $start_loop = $no_of_paginations - 3;
	                $end_loop = $no_of_paginations;
	            } else {
	                $end_loop = $no_of_paginations;
	            }
	        } else {
	            $start_loop = 1;
	            if ($no_of_paginations > 4)
	                $end_loop = 4;
	            else
	                $end_loop = $no_of_paginations;
	        }
 
	        $pag_container .= "
	        <div class='woocommerce-pagination' style='margin-top:30px;' id='product-categories-search-product'>
	            <ul class='product-page-numbers page-numbers'>";

	        if ($previous_btn && $cur_page > 1) {
	            $pre = $cur_page - 1;
	            $pag_container .= "<li><a p='$pre' class='page-numbers active' href='javascript:void(0)' ino='{$category}'>←</a></li>";
	        } else if ($previous_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>←</a></li>";
	        }
	        for ($i = $start_loop; $i <= $end_loop; $i++) {

	            if ($cur_page == $i)
	                $pag_container .= "<li><span p='$i' class='page-numbers inactive'>{$i}</span></li>";
	            else
	                $pag_container .= "<li><a p='$i' class = 'page-numbers active' href='javascript:void(0)' ino='{$category}'>{$i}</a></li>";
	        }

	        if ($next_btn && $cur_page < $no_of_paginations) {
	            $nex = $cur_page + 1;
	            $pag_container .= "<li><a p='$nex' class='page-numbers active' href='javascript:void(0)' ino='{$category}'>→</a></li>";
	        } else if ($next_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>→</span></li>";
	        }

	        $pag_container = $pag_container . "
	            </ul>
	        </div>";

	        if (count($total_products) == 0) {
	        	$pag_container = '';
	        }

	        echo $msg . $pag_container ;
		}
	}

	public function edit()
	{
		$id = $this->input->get('project_id');
		
		$project = $this->projectsmodel->getProjectByID($id);
		
		$list = $this->levelsmodel->allActiveLevels();
		$project_levels = $this->projectlevelsmodel->getByID($id);
		$level_signs = $this->levelsignsmodel->getByID($id);
		$sign_list = [];
        $customers = [];

        if ($this->permissionsmodel->get_role_by_id(get_current_user_id()) == 'administrator') {
            $customers = $this->customersmodel->allCustomers();
        }

		foreach ($level_signs as $level_sign) {
			if ($level_sign->is_custom) {
				$sign = $this->customsignsmodel->getByID($level_sign->sign_id);
				$tmp['id'] = $level_sign->sign_id;
			   	$tmp['name'] = $sign->custom_sign_name;
			   	$tmp['image'] = IMG_DIR.$sign->image;
			   	$tmp['post_excerpt'] = $sign->plate_size_name;
			   	$tmp['level_id'] = $level_sign->level_id;
			   	$tmp['sign_id'] = $level_sign->sign_id;
			   	$tmp['quantity'] = $level_sign->quantity;
			   	$tmp['is_custom'] = $level_sign->is_custom;
			   	$tmp['location'] = $level_sign->location;
			   	$sign_list[] = $tmp;
			} else {
				$product = wc_get_product($level_sign->sign_id);
				$img = '';
				if (has_post_thumbnail( $level_sign->sign_id ) ) {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $level_sign->sign_id ), 'woocommerce_thumbnail' );
					$img = $image[0];
				}
				$tmp['id'] = $level_sign->sign_id;
			   	$tmp['name'] = $product->name;
			   	$tmp['image'] = $img;
			   	$tmp['post_excerpt'] = $product->short_description;
			   	$tmp['level_id'] = $level_sign->level_id;
			   	$tmp['sign_id'] = $level_sign->sign_id;
			   	$tmp['quantity'] = $level_sign->quantity;
			   	$tmp['is_custom'] = $level_sign->is_custom;
			   	$tmp['location'] = $level_sign->location;
			   	$sign_list[] = $tmp;
			}	
		}

		$plate_sizes = $this->platesizemodel->allActivePlateSizes();
		$finish_materials = $this->finishmaterialmodel->allActiveFinishMaterials();
		$writing_options = $this->writingoptionmodel->allActiveWritingOptions();

		$this->view->assign('plate_sizes', $plate_sizes);
		$this->view->assign('finish_materials', $finish_materials);
		$this->view->assign('writing_options', $writing_options);
		$this->view->assign('title','Edit Project');
		$this->view->assign('levels', $list);
		$this->view->assign('project', $project);
		$this->view->assign('project_levels', $project_levels);
		$this->view->assign('level_signs', $sign_list);
        $this->view->assign('customers', $customers);
        $this->view->assign('role', $this->permissionsmodel->get_role_by_id(get_current_user_id()));
		$this->view->display('admin/projects/edit');
	}

	public function update()
	{
		$project_id = $_POST['project_id'];
		$data['project_name'] = $_POST['project_name'];
		$data['project_address'] = $_POST['project_address'];
		$data['status'] = $_POST['status'];
		$data['customer_id'] = $_POST['customer_id'] ?? get_current_user_id();
        
        if ($data['project_name'] == '') {
			$arr = array('err' => 1, 'msg' => 'Project name is required.');
			echo json_encode($arr);
			die();
		}
		
		$project = $this->projectsmodel->updateProject($project_id, $data);
		$this->projectlevelsmodel->delete($project_id);
		$this->levelsignsmodel->delete($project_id);
		$levels = $_POST['level'];
		$level_signs = explode(',', $_POST['level_signs']);
		foreach ($levels as $key => $value) {
			if (!isset($project_levels[$project.$value])) {
				$level['project_id'] = $project_id;
				$level['level_id'] = $value;		
				$this->projectlevelsmodel->create($level);
				$project_levels[$project.$value] = 1;
			} else {
				$project_levels[$project.$value] += 1;
			}

			$signs = explode('-', $level_signs[$key]);
			foreach ($signs as $k => $v) {
				$sign = explode(':', $signs[$k]);
                $sign_info = explode('custom', $sign[0]);
                $qty = $sign[1];
                $location = explode('location', $sign_info[1]);
                $level_sign['project_id'] = $project_id;
                $level_sign['level_id'] = $value;
                $level_sign['sign_id'] = $sign_info[0];
                $level_sign['is_custom'] = $location[0];
                $level_sign['quantity'] = $qty;
                $level_sign['location'] = $location[1];
         		
				$result =$this->levelsignsmodel->getByProjectIdLevelIdSignId($project_id, $value, $sign_info[0], $location[0]);
				if ($result) {
					$level_sign['quantity'] = $qty + $result->quantity;
					$this->levelsignsmodel->update($project_id, $value, $sign_info[0], $level_sign);
				} else {
					$this->levelsignsmodel->create($level_sign);
				}
			}
		}

		if( is_wp_error( $project  ) ) {
		    $arr = array('err' => 1, 'msg' => $project->get_error_message());		
		}
		else {
			$arr = array('err' => 0, 'msg' => 'Project was updated successfully.');
		}

		echo json_encode($arr);
	}

	public function view()
	{
		$current_user = wp_get_current_user();

		$id = $this->input->get('project_id');
		
		$project = $this->projectsmodel->getProjectByID($id);
		$role = $this->permissionsmodel->get_role_by_id(get_current_user_id());

		$list = $this->levelsmodel->allActiveLevels();
		$project_levels = $this->projectlevelsmodel->getByID($id);
		$level_signs = $this->levelsignsmodel->getByID($id);
		$sign_list = [];
		foreach ($level_signs as $level_sign) {
			if ($level_sign->is_custom) {
				$sign = $this->customsignsmodel->getByID($level_sign->sign_id);
				$tmp['id'] = $level_sign->sign_id;
			   	$tmp['name'] = $sign->custom_sign_name;
			   	$tmp['image'] = IMG_DIR.$sign->image;
			   	$tmp['post_excerpt'] = $sign->plate_size_name;
			   	$tmp['level_id'] = $level_sign->level_id;
			   	$tmp['sign_id'] = $level_sign->sign_id;
			   	$tmp['quantity'] = $level_sign->quantity;
			   	$tmp['is_custom'] = $level_sign->is_custom;
			   	$sign_list[] = $tmp;
			} else {
				$product = wc_get_product($level_sign->sign_id);
				$img = '';
				if (has_post_thumbnail( $level_sign->sign_id ) ) {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $level_sign->sign_id ), 'woocommerce_thumbnail' );
					$img = $image[0];
				}
				$tmp['id'] = $level_sign->sign_id;
			   	$tmp['name'] = $product->name;
			   	$tmp['image'] = $img;
			   	$tmp['post_excerpt'] = $product->short_description;
			   	$tmp['level_id'] = $level_sign->level_id;
			   	$tmp['sign_id'] = $level_sign->sign_id;
			   	$tmp['quantity'] = $level_sign->quantity;
			   	$tmp['is_custom'] = $level_sign->is_custom;
			   	$sign_list[] = $tmp;
			}	
		}

		$this->view->assign('title','View Schedule');
		$this->view->assign('levels', $list);
		$this->view->assign('project', $project);
		$this->view->assign('project_levels', $project_levels);
		$this->view->assign('level_signs', $sign_list);
		$this->view->assign('current_user', $current_user);
		$this->view->assign('role', $role);
		$this->view->display('admin/projects/view');
	}

	public function view_quantities()
	{
		$id = $this->input->get('project_id');
		$type = $this->input->get('type') ? $this->input->get('type'): 'product';
		$role = $this->permissionsmodel->get_role_by_id(get_current_user_id());
		$project = $this->projectsmodel->getProjectByID($id);
		
		$level_signs = $this->levelsignsmodel->getByID($id);
		$sign_list = [];

		if ($type == 'product') {
			foreach ($level_signs as $level_sign) {
				if ($level_sign->is_custom) {
					$sign = $this->customsignsmodel->getByID($level_sign->sign_id);
					$sign_image = IMG_DIR.$sign->image;
					if(!isset($sign_list[$sign->custom_sign_name."|".$sign_image])) {
				   		$sign_list[$sign->custom_sign_name."|".$sign_image] = $level_sign->quantity;
				   	} else {
				   		$sign_list[$sign->custom_sign_name."|".$sign_image] += $level_sign->quantity;
				   	}
				} else {
					$product = wc_get_product($level_sign->sign_id);
					$img = '';
					if (has_post_thumbnail( $product->id ) ) {
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'woocommerce_thumbnail' );
						$img = $image[0];
					}
				   	if(!isset($sign_list[$product->name."|".$img])) {
				   		$sign_list[$product->name."|".$img] = $level_sign->quantity;
				   	} else {
				   		$sign_list[$product->name."|".$img] += $level_sign->quantity;
				   	}
				}
			}
		} else {
			$sign_list = [];
			foreach ($level_signs as $level_sign) {
				if ($level_sign->is_custom) {
					$sign = $this->customsignsmodel->getByID($level_sign->sign_id);
					$sign_image = IMG_DIR.$sign->image;
					if(!isset($sign_list[$level_sign->name])) {
				   		$sign_list[$level_sign->name] = [['product_name' => $sign->custom_sign_name, 'product_image' => $sign_image, 'quantity' => $level_sign->quantity]];
				   	} else {
				   		$sign_list[$level_sign->name][]= ['product_name' => $sign->custom_sign_name, 'product_image' => $sign_image, 'quantity' => $level_sign->quantity];
				   	}
				} else {
					$product = wc_get_product($level_sign->sign_id);
					$img = '';
					if (has_post_thumbnail( $product->id ) ) {
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'woocommerce_thumbnail' );
						$img = $image[0];
					}
				   	if(!isset($sign_list[$level_sign->name])) {
				   		$sign_list[$level_sign->name] = [['product_name' => $product->name, 'product_image' => $img, 'quantity' => $level_sign->quantity]];
				   	} else {
				   		$sign_list[$level_sign->name][]= ['product_name' => $product->name, 'product_image' => $img, 'quantity' => $level_sign->quantity];
				   	}
				}
			}
		}
		
		$this->view->assign('title','View Quantities');
		$this->view->assign('project', $project);
		$this->view->assign('level_signs', $sign_list);
		$this->view->assign('type', $type);
		$this->view->assign('role', $role);
		$this->view->display('admin/projects/view_quantities');
	}

	public function export_file()
	{
		$id = $this->input->get('project_id');
		$type = $this->input->get('type') ? $this->input->get('type'): 'product';
		$project = $this->projectsmodel->getProjectByID($id);
		
		$level_signs = $this->levelsignsmodel->getByID($id);
		$sign_list = [];

		if ($type == 'product') {
			foreach ($level_signs as $level_sign) {
				if ($level_sign->is_custom) {
					$sign = $this->customsignsmodel->getByID($level_sign->sign_id);
					$sign_image = IMG_DIR.$sign->image;
					if(!isset($sign_list[$sign->custom_sign_name."|".$sign_image])) {
				   		$sign_list[$sign->custom_sign_name."|".$sign_image] = $level_sign->quantity;
				   	} else {
				   		$sign_list[$sign->custom_sign_name."|".$sign_image] += $level_sign->quantity;
				   	}
				} else {
					$product = wc_get_product($level_sign->sign_id);
					$img = '';
					if (has_post_thumbnail( $product->id ) ) {
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'woocommerce_thumbnail' );
						$img = $image[0];
					}
				   	if(!isset($sign_list[$product->name."|".$img])) {
				   		$sign_list[$product->name."|".$img] = $level_sign->quantity;
				   	} else {
				   		$sign_list[$product->name."|".$img] += $level_sign->quantity;
				   	}
				}
			}

			$data[] = ['Product Image' => '', 'Product Name' => '', 'Qty' => ''];
			foreach ($sign_list as $key => $value) {
				$info = explode("|", $key);
				$tmp['Product Image'] = $info[1];
				$tmp['Product Name'] = $info[0];
				$tmp['Qty'] = $value;
				$data[] = $tmp;
			}

			$spreadsheet = new Spreadsheet();
	        $sheet = $spreadsheet->getActiveSheet();
	        unset($data[0]);
	        $sheet->getColumnDimension('F')->setWidth(80);
	        $sheet->setCellValue('A1', 'Product Image');
	        $sheet->setCellValue('F1', 'Product Name');
		    $sheet->setCellValue('G1', 'Qty');

	        foreach ($data as $key => $value) {
	        	$k = (int) $key + 1;
	        	// $sheet->setCellValue('A'.$k, $value['Product Image']);
				$sheet->setCellValue('F'.$k, $value['Product Name']);
				$sheet->setCellValue('G'.$k, $value['Qty']);
				$IMG = $value['Product Image'];
							if (isset($IMG) && !empty($IMG)) {
				    $imageType = "png";

				    if (strpos($IMG, ".png") === false) {
				        $imageType = "jpg";
				    }

				    $drawing = new MemoryDrawing();
				    $sheet->getRowDimension($k)->setRowHeight(140);
				 
				    $sheet->mergeCells('A1:E1');
				    $sheet->mergeCells('A'.$k.':E'.$k);

				    $gdImage = ($imageType == 'png') ? imagecreatefrompng($IMG) : imagecreatefromjpeg($IMG);
				    $drawing->setResizeProportional(false);
				    $drawing->setImageResource($gdImage);
				    $drawing->setRenderingFunction(\PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::RENDERING_JPEG);
				    $drawing->setMimeType(\PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::MIMETYPE_DEFAULT);
				    $drawing->setWidth(250);
				    $drawing->setHeight(139);
				    $drawing->setCoordinates('A'.$k);
				    $drawing->setWorksheet($spreadsheet->getActiveSheet());
				}
	        }
		} else {
			$sign_list = [];
			foreach ($level_signs as $level_sign) {
				if ($level_sign->is_custom) {
					$sign = $this->customsignsmodel->getByID($level_sign->sign_id);
					$sign_image = IMG_DIR.$sign->image;
					if(!isset($sign_list[$level_sign->name])) {
				   		$sign_list[$level_sign->name] = [['product_name' => $sign->custom_sign_name, 'product_image' => $sign_image, 'quantity' => $level_sign->quantity, 'location' => $level_sign->location, 'comments' => $sign->comments]];
				   	} else {
				   		$sign_list[$level_sign->name][]= ['product_name' => $sign->custom_sign_name, 'product_image' => $sign_image, 'quantity' => $level_sign->quantity, 'location' => $level_sign->location, 'comments' => $sign->comments];
				   	}
				} else {
					$product = wc_get_product($level_sign->sign_id);
					$img = '';
					if (has_post_thumbnail( $product->id ) ) {
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'woocommerce_thumbnail' );
						$img = $image[0];
					}
				   	if(!isset($sign_list[$level_sign->name])) {
				   		$sign_list[$level_sign->name] = [['product_name' => $product->name, 'product_image' => $img, 'quantity' => $level_sign->quantity, 'location' => $level_sign->location, 'comments' => '']];
				   	} else {
				   		$sign_list[$level_sign->name][]= ['product_name' => $product->name, 'product_image' => $img, 'quantity' => $level_sign->quantity, 'location' => $level_sign->location, 'comments' => ''];
				   	}
				}
			}

			$data[] = ['Level', 'Product Image' => '', 'Product Name' => '', 'Qty' => ''];
			foreach ($sign_list as $key => $value) {
				$tmp['Level'] = $key;
				$tmp['Location'] = '';
				$tmp['Product Image'] = '';
				$tmp['Product Name'] = '';
				$tmp['Qty'] = '';
				$tmp['Comments'] = '';
				$data[] = $tmp;
				foreach ($value as $k => $v) {
					$tmp['Level'] = '';
					$tmp['Location'] = $v['location'];
					$tmp['Product Image'] = $v['product_image'];
					$tmp['Product Name'] = $v['product_name'];
					$tmp['Qty'] = $v['quantity'];
					$tmp['Comments'] = $v['comments'];
					$data[] = $tmp;
				}		
			}

			$spreadsheet = new Spreadsheet();
	        $sheet = $spreadsheet->getActiveSheet();
	        unset($data[0]);
	        $sheet->getColumnDimension('A')->setWidth(30);
	        $sheet->getColumnDimension('H')->setWidth(80);
	        $sheet->getColumnDimension('J')->setWidth(100);
	        $sheet->setCellValue('A1', 'Level');
	        $sheet->setCellValue('B1', 'Location');
	        $sheet->setCellValue('C1', 'Product Image');
	        $sheet->setCellValue('H1', 'Product Name');
		    $sheet->setCellValue('I1', 'Qty');
		    $sheet->setCellValue('J1', 'Comments');

	        foreach ($data as $key => $value) {
	        	$k = (int) $key + 1;
	        	$sheet->setCellValue('A'.$k, $value['Level']);
	        	$sheet->setCellValue('B'.$k, $value['Location']);
				$sheet->setCellValue('H'.$k, $value['Product Name']);
				$sheet->setCellValue('I'.$k, $value['Qty']);
				$sheet->setCellValue('J'.$k, $value['Comments']);
				$IMG = $value['Product Image'];
							if (isset($IMG) && !empty($IMG)) {
				    $imageType = "png";

				    if (strpos($IMG, ".png") === false) {
				        $imageType = "jpg";
				    }

				    $drawing = new MemoryDrawing();
				    $sheet->getRowDimension($k)->setRowHeight(140);
				 
				    $sheet->mergeCells('C1:G1');
				    $sheet->mergeCells('C'.$k.':G'.$k);

				    $gdImage = ($imageType == 'png') ? imagecreatefrompng($IMG) : imagecreatefromjpeg($IMG);
				    $drawing->setResizeProportional(false);
				    $drawing->setImageResource($gdImage);
				    $drawing->setRenderingFunction(\PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::RENDERING_JPEG);
				    $drawing->setMimeType(\PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::MIMETYPE_DEFAULT);
				    $drawing->setWidth(250);
				    $drawing->setHeight(139);
				    $drawing->setCoordinates('C'.$k);
				    $drawing->setWorksheet($spreadsheet->getActiveSheet());
				}
	        }
		}

		$writer = new Xlsx($spreadsheet);
		 
		$file_name = $type.'_view_quantities.xlsx';
		$writer->save($file_name);

		echo json_encode(['err' => 0, 'msg' => 'success']);
	}

	public function print()
	{
		$id = $this->input->get('project_id');
		$project = $this->projectsmodel->getProjectByID($id);
		$user_meta = get_user_meta($project->customer_id);
		$first_name = !empty($user_meta['first_name']) ? $user_meta['first_name'][0] : '';
		$last_name = !empty($user_meta['last_name']) ? $user_meta['last_name'][0] : '';
		$full_name = $first_name. ' ' .$last_name;;
		$level_signs = $this->levelsignsmodel->getByID($id);
		$sign_list = [];
		foreach ($level_signs as $level_sign) {
			if ($level_sign->is_custom) {
				$sign = $this->customsignsmodel->getByID($level_sign->sign_id);
				$sign_image = IMG_DIR.$sign->image;
				if(!isset($sign_list[$level_sign->name])) {
			   		$sign_list[$level_sign->name] = [['product_name' => $sign->custom_sign_name, 'product_image' => $sign_image, 'quantity' => $level_sign->quantity, 'post_excerpt' => $sign->plate_size_name]];
			   	} else {
			   		$sign_list[$level_sign->name][]= ['product_name' => $sign->custom_sign_name, 'product_image' => $sign_image, 'quantity' => $level_sign->quantity, 'post_excerpt' => $sign->plate_size_name];
			   	}
			} else {
				$product = wc_get_product($level_sign->sign_id);
				$img = '';
				if (has_post_thumbnail( $product->id ) ) {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'woocommerce_thumbnail' );
					$img = $image[0];
				}
			   	if(!isset($sign_list[$level_sign->name])) {
			   		$sign_list[$level_sign->name] = [['product_name' => $product->name, 'product_image' => $img, 'quantity' => $level_sign->quantity, 'post_excerpt' => $product->short_description]];
			   	} else {
			   		$sign_list[$level_sign->name][]= ['product_name' => $product->name, 'product_image' => $img, 'quantity' => $level_sign->quantity, 'post_excerpt' => $product->short_description];
			   	}
			}
		}

		foreach ($sign_list as $key => $value) {
			$tmp['Level'] = $key;
			$tmp['Product Image'] = '';
			$tmp['Product Name'] = '';
			$tmp['Post Excerpt'] = '';
			$tmp['Qty'] = '';
			$data[] = $tmp;
			foreach ($value as $k => $v) {
				$tmp['Level'] = '';
				$tmp['Product Image'] = $v['product_image'];
				$tmp['Product Name'] = $v['product_name'];
				$tmp['Qty'] = $v['quantity'];
				$tmp['Post Excerpt'] = $v['post_excerpt'];
				$data[] = $tmp;
			}		
		}

		$dompdf = new Dompdf();
		$options = new Options();
		$options->setIsRemoteEnabled(true);

		$dompdf->setOptions($options);
		$html = '<!DOCTYPE html><html><head><style>tr, td{border: 1px solid black;}table { width: 100%; border-collapse: collapse; }</style></head><body>';

		$html .= '<table><tr><td colspan="4">Project Name: '.$project->project_name.'</td></tr><tr><td colspan="4">Project Address: '.$project->project_address.'</td><tr><td colspan="4">Customer Name: '.$full_name.'</td></tr>';
		$html .= "<tr><td>Level</td><td style='width:20%;'>Product Image</td><td style='width:50%;'>Product Name</td><td style='width:10%;'>Qty</td></tr>";
		foreach ($data as $key => $value) {
			if ($value['Level'] === '') {
				$html .= "<tr><td>".$value['Level']."</td><td><img src='".$value['Product Image']."' width='250'/></td><td><p style='display:block;'>".$value['Product Name']."</p><span style='border-bottom: solid 1px #000;'>".$value['Post Excerpt']."</span></td><td>".$value['Qty']."</td></tr>";
			} else {
				$html .= "<tr><td colspan='4'>".$value['Level']."</td></tr>";
			}
			
		}
		$html .= "</table>";
		$html .= "</body></html>";
		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
		$dompdf->render();

		// Output the generated PDF to Browser
		$dompdf->stream("view_project.pdf", array("Attachment" => false));
	}

	public function update_status()
	{
		$project_id = $_POST['project_id'];
		$data['is_approved'] = $_POST['is_approved'];
		$data['comments'] = $_POST['comments'];
		
		$project = $this->projectsmodel->updateProject($project_id, $data);

		if( is_wp_error( $project  ) ) {
		    $arr = array('err' => 1, 'msg' => $project->get_error_message());		
		}
		else {
			$arr = array('err' => 0, 'msg' => 'Project Status was updated successfully.');
		}

		echo json_encode($arr);
	}

	public function searchBySignName()
	{
		$msg = '';
		if (isset($_GET['current_page'])) {
	        $page = $_GET['current_page'];
	        $cur_page = $page;
	        $page -= 1;
	        // Set the number of results to display
	        $per_page = 3;
	        $previous_btn = true;
	        $next_btn = true;
	        $first_btn = true;
	        $last_btn = true;
	        $start = $page * $per_page;

	        $search_text = $_GET['sign_name'];
			$products = $this->customsignsmodel->searchByNameLimitOffset($search_text, $per_page, $start);
			$sign = $this->customsignsmodel->searchByName($search_text);
			$total_products = $sign ? count($sign) : 0;

			if ($total_products > 0) {
				$msg .= '<h3 class="text-center" style="margin-bottom:30px;">SELECT SIGNS</h3>';
			} else {
				$msg .= '<p>No result found.</p>';
			}
			foreach($products as $product) {
				$img = '';
				$img_content = '';
				if ($product->image) {
					$img = IMG_DIR.$product->image;
				}
				if ($img) {
					$image_content = '<div class="product-image mt-3"><img src="'.$img.'" width="250" height="139" style="width:100%;max-width:250px;height: auto;display:block;margin: 0px auto 8px;"></div>';
				}

				$msg .= '<div class="col-md-4 col-sm-4 sign-container">'.$image_content.'<a href="javascript:void(0)" id="'.$product->id.'" class="select_sign" is-custom="0">'.$product->custom_sign_name.'</a><span class="product-span d-block"><p>'.$product->plate_size_name.'</p></span></div>'; 
			}

	        $no_of_paginations = ceil($total_products / $per_page);

	        if ($cur_page >= 4) {
	            $start_loop = $cur_page - 3;
	            if ($no_of_paginations > $cur_page + 3)
	                $end_loop = $cur_page + 3;
	            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 3) {
	                $start_loop = $no_of_paginations - 3;
	                $end_loop = $no_of_paginations;
	            } else {
	                $end_loop = $no_of_paginations;
	            }
	        } else {
	            $start_loop = 1;
	            if ($no_of_paginations > 4)
	                $end_loop = 4;
	            else
	                $end_loop = $no_of_paginations;
	        }
 
	        $pag_container .= "
	        <div class='woocommerce-pagination' style='margin-top:30px;' id='product-categories-search-sign'>
	            <ul class='product-page-numbers page-numbers'>";

	        if ($previous_btn && $cur_page > 1) {
	            $pre = $cur_page - 1;
	            $pag_container .= "<li><a p='$pre' class='page-numbers active' href='javascript:void(0)' ino='{$category}'>←</a></li>";
	        } else if ($previous_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>←</a></li>";
	        }
	        for ($i = $start_loop; $i <= $end_loop; $i++) {

	            if ($cur_page == $i)
	                $pag_container .= "<li><span p='$i' class='page-numbers inactive'>{$i}</span></li>";
	            else
	                $pag_container .= "<li><a p='$i' class = 'page-numbers active' href='javascript:void(0)' ino='{$category}'>{$i}</a></li>";
	        }

	        if ($next_btn && $cur_page < $no_of_paginations) {
	            $nex = $cur_page + 1;
	            $pag_container .= "<li><a p='$nex' class='page-numbers active' href='javascript:void(0)' ino='{$category}'>→</a></li>";
	        } else if ($next_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>→</span></li>";
	        }

	        $pag_container = $pag_container . "
	            </ul>
	        </div>";

	        if ($total_products == 0) {
	        	$pag_container = '';
	        }

	        echo $msg . $pag_container ;
	    }
	}

	public function create_custom_sign(){
		$customer_id = $this->input->post('custom_sign_customer_id');
		if ($this->permissionsmodel->get_role_by_id(get_current_user_id()) == 'project_customer') {
			$customer_id = get_current_user_id();
		}
		$name = $this->input->post('custom_sign_name') ?? '';
		$plate_size_id = $this->input->post('plate_size_id');
		$comments = $this->input->post('comments');
		$finish_material_id = $this->input->post('finish_material_id');
		$writing_option_id = $this->input->post('writing_option_id');
		$project_id = $this->input->post('project_id') ?? 0;

		$image = $this->input->file('image');

		if ($name === '') {
			$arr = array('err' => 1, 'msg' => 'Custom Sign Name is required.');
		} else {
			if ($image['name'] == '') {
				$filename = 'default.png';
				$data = array('custom_sign_name' => $name, 'plate_size_id' => $plate_size_id, 'finish_material_id' => $finish_material_id, 'writing_option_id' => $writing_option_id, 'is_approved' => 0, 'image' => $filename, 'customer_id' => $customer_id, 'project_id' => $project_id, 'comments' => $comments);

				$result = $this->customsignsmodel->create($data);
				if( is_wp_error( $result  ) ) {
				    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
				} else {
					$custom_sign = $this->customsignsmodel->getByID($result);
					$arr = array('err' => 0, 'msg' => 'Custom Sign was successfully added.', 'data' => $custom_sign);
				}	
			}
			else{
				$checkfile = $this->upload->check($image);
				if($checkfile){
					$arr = array('err' => 1, 'msg' => 'File already exist.');
				}
				else{
					$type = explode('/', $image['type']);
					$image['name'] = '.'.end($type);
					$valid = $this->upload->isValidImage($image);
					if($valid){
						$upload = $this->upload->process($image);
						if($upload != ''){
							$data = array('custom_sign_name' => $name, 'plate_size_id' => $plate_size_id, 'finish_material_id' => $finish_material_id, 'writing_option_id' => $writing_option_id, 'is_approved' => 0, 'image' => $upload, 'customer_id' => $customer_id, 'project_id' => $project_id, 'comments' => $comments);
							$result = $this->customsignsmodel->create($data);
							if( is_wp_error( $result  ) ) {
							    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
							} else {
								$custom_sign = $this->customsignsmodel->getByID($result);
								$arr = array('err' => 0, 'msg' => 'Custom Sign was successfully added.', 'data' => $custom_sign);
							}	
						}
						else{
							$arr = array('err' => 1, 'msg' => 'Failed to upload file.');
						}
					}
					else{
						$arr = array('err' => 1, 'msg' => 'Invalid file size or type.');
					}
				}
			}
		}	
		echo json_encode($arr);
	}

	public function deleteProject(){
		$id = $this->input->post('id');
		$data = $this->projectsmodel->getProjectByID($id);
		if ($data) {
			$delete = $this->projectsmodel->deleteProject($id);
			if( is_wp_error( $delete  ) ) {
		    	$arr = array('err' => 1, 'msg' => $delete->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Project was deleted successfully.');
			}
			echo json_encode($arr);
			die();
		} else {
			$arr = array('err' => 1, 'msg' => 'This project does not exists.');	
		}
		echo json_encode($arr);
	}

	public function send_email(){
		$id = $this->input->post('id');
		
		$project = $this->projectsmodel->getProjectByID($id);
		$user_meta = get_user_meta($project->customer_id);
		$first_name = !empty($user_meta['first_name']) ? $user_meta['first_name'][0] : '';
		$last_name = !empty($user_meta['last_name']) ? $user_meta['last_name'][0] : '';
		$full_name = $first_name. ' ' .$last_name;;
		$level_signs = $this->levelsignsmodel->getByID($id);
		$sign_list = [];
		foreach ($level_signs as $level_sign) {
			if ($level_sign->is_custom) {
				$sign = $this->customsignsmodel->getByID($level_sign->sign_id);
				$sign_image = IMG_DIR.$sign->image;
				if(!isset($sign_list[$level_sign->name])) {
			   		$sign_list[$level_sign->name] = [['product_name' => $sign->custom_sign_name, 'product_image' => $sign_image, 'quantity' => $level_sign->quantity, 'post_excerpt' => $sign->plate_size_name]];
			   	} else {
			   		$sign_list[$level_sign->name][]= ['product_name' => $sign->custom_sign_name, 'product_image' => $sign_image, 'quantity' => $level_sign->quantity, 'post_excerpt' => $sign->plate_size_name];
			   	}
			} else {
				$product = wc_get_product($level_sign->sign_id);
				$img = '';
				if (has_post_thumbnail( $product->id ) ) {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'woocommerce_thumbnail' );
					$img = $image[0];
				}
			   	if(!isset($sign_list[$level_sign->name])) {
			   		$sign_list[$level_sign->name] = [['product_name' => $product->name, 'product_image' => $img, 'quantity' => $level_sign->quantity, 'post_excerpt' => $product->short_description]];
			   	} else {
			   		$sign_list[$level_sign->name][]= ['product_name' => $product->name, 'product_image' => $img, 'quantity' => $level_sign->quantity, 'post_excerpt' => $product->short_description];
			   	}
			}
		}

		foreach ($sign_list as $key => $value) {
			$tmp['Level'] = $key;
			$tmp['Product Image'] = '';
			$tmp['Product Name'] = '';
			$tmp['Post Excerpt'] = '';
			$tmp['Qty'] = '';
			$data[] = $tmp;
			foreach ($value as $k => $v) {
				$tmp['Level'] = '';
				$tmp['Product Image'] = $v['product_image'];
				$tmp['Product Name'] = $v['product_name'];
				$tmp['Qty'] = $v['quantity'];
				$tmp['Post Excerpt'] = $v['post_excerpt'];
				$data[] = $tmp;
			}		
		}

		$dompdf = new Dompdf();
		$options = new Options();
		$options->setIsRemoteEnabled(true);

		$dompdf->setOptions($options);
		$html = '<!DOCTYPE html><html><head><style>tr, td{border: 1px solid black;}table { width: 100%; border-collapse: collapse; }</style></head><body>';

		$html .= '<table><tr><td colspan="4">Project Name: '.$project->project_name.'</td></tr><tr><td colspan="4">Project Address: '.$project->project_address.'</td><tr><td colspan="4">Customer Name: '.$full_name.'</td></tr>';
		$html .= "<tr><td>Level</td><td style='width:20%;'>Product Image</td><td style='width:50%;'>Product Name</td><td style='width:10%;'>Qty</td></tr>";
		foreach ($data as $key => $value) {
			if ($value['Level'] === '') {
				$html .= "<tr><td>".$value['Level']."</td><td><img src='".$value['Product Image']."' width='250'/></td><td><p style='display:block;'>".$value['Product Name']."</p><span style='border-bottom: solid 1px #000;'>".$value['Post Excerpt']."</span></td><td>".$value['Qty']."</td></tr>";
			} else {
				$html .= "<tr><td colspan='4'>".$value['Level']."</td></tr>";
			}
			
		}
		$html .= "</table>";
		$html .= "</body></html>";
		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
		$dompdf->render();

		$dompdf->output();
		$output = $dompdf->output();
    	file_put_contents('project.pdf', $output);
		
		$from_email = $this->input->post('from_email'); 
		$to_email = $this->input->post('to_email'); 
		$message = $this->input->post('message');
		$user_name = $this->input->post('user_name');
		$attachments = array(ABSPATH . 'project.pdf');

		$subject = "Project";
		$headers = 'From: '.$user_name. ' <'. $from_email .'>'. "\r\n" .
		    'Reply-To: ' . $from_email . "\r\n";

		//Here put your Validation and send mail
		$sent = wp_mail($to_email, $subject, strip_tags($message), $headers, $attachments);
	    if ($sent) {
	    	$arr = array('err' => 0, 'msg' => 'Email sent successfully.');
	    } else {
	    	$arr = array('err' => 1, 'msg' => 'Failed to send email.');
	    }

	    echo json_encode($arr);
	}
}

?>