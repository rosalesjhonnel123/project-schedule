<?php
class CustomSigns extends Controller{
	
	function __construct(){
		parent::__construct();
		$this->customsignsmodel = new customsignsmodel();
		$this->finishmaterialmodel = new finishmaterialmodel();
		$this->platesizemodel = new platesizemodel();
		$this->writingoptionmodel = new writingoptionmodel();
		$this->permissionsmodel = new permissionsmodel();
		$this->customersmodel = new customersmodel();
		$this->projectsmodel = new projectsmodel();
		$this->levelsignsmodel = new levelsignsmodel();
		$this->authmodel = new authmodel();
		$this->authmodel->isLoggedIn();
		$this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
		$this->has_permission = $this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
	}

	public function index(){
		$plate_sizes = $this->platesizemodel->allActivePlateSizes();
		$finish_materials = $this->finishmaterialmodel->allActiveFinishMaterials();
		$writing_options = $this->writingoptionmodel->allActiveWritingOptions();
		$customers = [];
		$projects = [];
		if ($this->permissionsmodel->get_role_by_id(get_current_user_id()) == 'project_customer') {
			$projects = $this->projectsmodel->allActiveProjects(get_current_user_id());
		}
		if ($this->permissionsmodel->get_role_by_id(get_current_user_id()) == 'administrator') {
			$customers = $this->customersmodel->allCustomers();
		}

		$status = $this->input->get('status');
		
		$this->view->assign('title', 'Custom Signs');
		$this->view->assign('plate_sizes', $plate_sizes);
		$this->view->assign('finish_materials', $finish_materials);
		$this->view->assign('writing_options', $writing_options);
		$this->view->assign('projects', $projects);
		$this->view->assign('customers', $customers);
		$this->view->assign('role', $this->permissionsmodel->get_role_by_id(get_current_user_id()));
		$this->view->assign('status', $status);
		$this->view->assign('title', 'Custom Signs');

		$this->view->display('admin/customsigns/index');
	}

	public function allCustomSignsAjax(){
		
		$customer_id = $this->permissionsmodel->get_role_by_id(get_current_user_id()) == 'project_customer' ? get_current_user_id() : ''; 
		$status = $this->input->get('status') ?? 'active';
		$list = $this->customsignsmodel->allCustomSigns($customer_id, $status);
		echo json_encode(array("data" => $list));
	}
	public function allCustomSigns(){
		
		$msg = '';
		if (isset($_GET['current_page'])) {
	        $page = $_GET['current_page'];
	        $cur_page = $page;
	        $page -= 1;
	        // Set the number of results to display
	        $per_page = 3;
	        $previous_btn = true;
	        $next_btn = true;
	        $first_btn = true;
	        $last_btn = true;
	        $start = $page * $per_page;

			$customer_id = $this->permissionsmodel->get_role_by_id(get_current_user_id()) == 'project_customer' ? get_current_user_id() : ''; 
			$status = $this->input->get('status') ?? 'active';
			$products = $this->customsignsmodel->allCustomSignsLimitOffset($customer_id, $status, $per_page, $start);
			$signs = $this->customsignsmodel->allCustomSigns($customer_id, $status);
			$total_products = $signs ? count($signs) : 0;

			if ($total_products > 0) {
				$msg .= '<h3 class="text-center" style="margin-bottom:30px;">SELECT SIGNS</h3>';
			} else {
				$msg .= '<p>No result found.</p>';
			}
			foreach($products as $product) {
				$img = '';
				$img_content = '';
				if ($product->image) {
					$img = IMG_DIR.$product->image;
				}
				if ($img) {
					$image_content = '<div class="product-image mt-3"><img src="'.$img.'" width="250" height="139" style="width:100%;max-width:250px;height: auto;display:block;margin: 0px auto 8px;"></div>';
				}

				$msg .= '<div class="col-md-4 col-sm-4 sign-container">'.$image_content.'<a href="javascript:void(0)" id="'.$product->id.'" class="select_sign" is-custom="0">'.$product->custom_sign_name.'</a><span class="product-span d-block"><p>'.$product->plate_size_name.'</p></span></div>'; 
			}

	        $no_of_paginations = ceil($total_products / $per_page);

	        if ($cur_page >= 4) {
	            $start_loop = $cur_page - 3;
	            if ($no_of_paginations > $cur_page + 3)
	                $end_loop = $cur_page + 3;
	            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 3) {
	                $start_loop = $no_of_paginations - 3;
	                $end_loop = $no_of_paginations;
	            } else {
	                $end_loop = $no_of_paginations;
	            }
	        } else {
	            $start_loop = 1;
	            if ($no_of_paginations > 4)
	                $end_loop = 4;
	            else
	                $end_loop = $no_of_paginations;
	        }
 
	        $pag_container .= "
	        <div class='woocommerce-pagination' style='margin-top:30px;' id='product-categories'>
	            <ul class='product-page-numbers page-numbers'>";

	        if ($previous_btn && $cur_page > 1) {
	            $pre = $cur_page - 1;
	            $pag_container .= "<li><a p='$pre' class='page-numbers active' href='javascript:void(0)' ino='{$category}'>←</a></li>";
	        } else if ($previous_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>←</a></li>";
	        }
	        for ($i = $start_loop; $i <= $end_loop; $i++) {

	            if ($cur_page == $i)
	                $pag_container .= "<li><span p='$i' class='page-numbers inactive'>{$i}</span></li>";
	            else
	                $pag_container .= "<li><a p='$i' class = 'page-numbers active' href='javascript:void(0)' ino='{$category}'>{$i}</a></li>";
	        }

	        if ($next_btn && $cur_page < $no_of_paginations) {
	            $nex = $cur_page + 1;
	            $pag_container .= "<li><a p='$nex' class='page-numbers active' href='javascript:void(0)' ino='{$category}'>→</a></li>";
	        } else if ($next_btn) {
	            $pag_container .= "<li><span class='page-numbers inactive'>→</span></li>";
	        }

	        $pag_container = $pag_container . "
	            </ul>
	        </div>";

	        if ($total_products == 0) {
	        	$pag_container = '';
	        }

	        echo $msg . $pag_container ;
	    }
	}

	public function getByID(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->get('id');
		$result = $this->customsignsmodel->getByID($id);
		echo json_encode(array($result));

	}

	public function update(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => $this->has_permission));
			exit;
		}
		$id = $this->input->post('id');
		$name = $this->input->post('custom_sign_name');
		$comments = $this->input->post('comments');
		$plate_size_id = $this->input->post('plate_size_id');
		$finish_material_id = $this->input->post('finish_material_id');
		$writing_option_id = $this->input->post('writing_option_id');
		$image = $this->input->file('image');
		$customer_id = $this->input->post('customer_id');
		$status = $this->input->post('status');
		$customsign = $this->customsignsmodel->getByID($id);

		if ($this->permissionsmodel->get_role_by_id(get_current_user_id()) == 'project_customer') {
			$customer_id = get_current_user_id();
		}
		$project_id = $this->input->post('project_id') ?? '';

		if ($name === '') {
			$arr = array('err' => 1, 'msg' => 'Custom Sign Name is required.');
		} 
		else if ($project_id === '') {
			$arr = array('err' => 1, 'msg' => 'Project is required.');
		} else {
			if ($customsign->project_id != $project_id) {
				$sign = $this->levelsignsmodel->getByProjectIdSignId($customsign->project_id, $id, 1);
				if ($sign) {
					echo json_encode(array('err' => 1, 'msg' => 'This sign is already use by other project.'));
					exit(-1);
				}
			}
			if (empty($image['name'])) {
				$filename = $this->input->post('currentimage');
				$data = array('custom_sign_name' => $name, 'plate_size_id' => $plate_size_id, 'finish_material_id' => $finish_material_id, 'writing_option_id' => $writing_option_id, 'image' => $filename, 'customer_id' => $customer_id, 'project_id' => $project_id, 'status' => $status, 'comments' => $comments);

				$result = $this->customsignsmodel->update($id, $data);
				if( is_wp_error( $result  ) ) {
				    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
				} else {
					$arr = array('err' => 0, 'msg' => 'Custom Sign was successfully updated.');
				}
			}
			else{
				$checkfile = $this->upload->check($image);
				if($checkfile){
					$arr = array('err' => 1, 'msg' => 'File already exist.');
				}
				else{

					$valid = $this->upload->isValidImage($image);
					if($valid){
						$upload = $this->upload->process($image);
						if($upload != ''){
							$data = array('custom_sign_name' => $name, 'plate_size_id' => $plate_size_id, 'finish_material_id' => $finish_material_id, 'writing_option_id' => $writing_option_id, 'is_approved' => 0, 'image' => $upload, 'customer_id' => $customer_id, 'project_id' => $project_id, 'status' => $status, 'comments' => $comments);
							$result = $this->customsignsmodel->update($id, $data);
							if( is_wp_error( $result  ) ) {
							    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
							} else {
								$arr = array('err' => 0, 'msg' => 'Custom Sign was successfully updated.');
							}
						}
						else{
							$arr = array('err' => 1, 'msg' => 'Failed to upload file.');
						}
					}
					else{
						$arr = array('err' => 1, 'msg' => 'Invalid file size or type.');
					}
				}
			}
		}
		echo json_encode($arr);
	}

	public function create(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$customer_id = $this->input->post('customer_id');
		if ($this->permissionsmodel->get_role_by_id(get_current_user_id()) == 'project_customer') {
			$customer_id = get_current_user_id();
		}
		$name = $this->input->post('custom_sign_name') ?? '';
		$comments = $this->input->post('comments');
		$plate_size_id = $this->input->post('plate_size_id');
		$finish_material_id = $this->input->post('finish_material_id');
		$writing_option_id = $this->input->post('writing_option_id');
		$project_id = $this->input->post('project_id') ?? '';
		
		$image = $this->input->file('image');
		if ($name === '') {
			$arr = array('err' => 1, 'msg' => 'Custom Sign Name is required.');
		}
		else if ($project_id === '') {
			$arr = array('err' => 1, 'msg' => 'Project is required.');
		} else {
			if (empty($image['name'])) {
				$filename = 'default.png';
				$data = array('custom_sign_name' => $name, 'plate_size_id' => $plate_size_id, 'finish_material_id' => $finish_material_id, 'writing_option_id' => $writing_option_id, 'is_approved' => 0, 'image' => $filename, 'customer_id' => $customer_id, 'project_id' => $project_id, 'status' => 1, 'comments' => $comments);

				$result = $this->customsignsmodel->create($data);
				if( is_wp_error( $result  ) ) {
				    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
				} else {
					$custom_sign = $this->customsignsmodel->getByID($result);
					$arr = array('err' => 0, 'msg' => 'Custom Sign was successfully added.', 'data' => $custom_sign);
				}	
			}
			else{
				$checkfile = $this->upload->check($image);
				if($checkfile){
					$arr = array('err' => 1, 'msg' => 'File already exist.');
				}
				else{
					$type = explode('/', $image['type']);
					$image['name'] = '.'.end($type);
					$valid = $this->upload->isValidImage($image);
					if($valid){
						$upload = $this->upload->process($image);
						if($upload != ''){
							$data = array('custom_sign_name' => $name, 'plate_size_id' => $plate_size_id, 'finish_material_id' => $finish_material_id, 'writing_option_id' => $writing_option_id, 'is_approved' => 0, 'image' => $upload, 'customer_id' => $customer_id, 'project_id' => $project_id, 'status' => 1, 'comments' => $comments);
							$result = $this->customsignsmodel->create($data);
							if( is_wp_error( $result  ) ) {
							    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
							} else {
								$custom_sign = $this->customsignsmodel->getByID($result);
								$arr = array('err' => 0, 'msg' => 'Custom Sign was successfully added.', 'data' => $custom_sign);
							}	
						}
						else{
							$arr = array('err' => 1, 'msg' => 'Failed to upload file.');
						}
					}
					else{
						$arr = array('err' => 1, 'msg' => 'Invalid file size or type.');
					}
				}
			}
		}	
		echo json_encode($arr);
	}

	public function update_status()
	{
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$custom_sign_id = $this->input->post('custom_sign_id');
		$is_approved = $this->input->post('is_approved');
		$comments = $this->input->post('comments');
		
		$data = ['is_approved' => $is_approved, 'comments' => $comments];
		
		$custom_sign = $this->customsignsmodel->update($custom_sign_id, $data);

		if( is_wp_error( $custom_sign  ) ) {
		    $arr = array('err' => 1, 'msg' => $custom_sign->get_error_message());		
		}
		else {
			$arr = array('err' => 0, 'msg' => 'Custom Sign Status was updated successfully.');
		}

		echo json_encode($arr);
	}

	public function delete(){
		$id = $this->input->post('id');
		
		$custom_sign = $this->customsignsmodel->getByID($id);
		$level_sign = $this->levelsignsmodel->getByCustomSignId($custom_sign->id);

		if (empty($level_sign)) {
			$delete = $this->customsignsmodel->delete($id);
			if( is_wp_error( $delete  ) ) {
		    	$arr = array('err' => 1, 'msg' => $delete->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Custom sign was deleted successfully.');
			}
			echo json_encode($arr);
			die();
		} else {
			$arr = array('err' => 1, 'msg' => 'We cannot continue this process. This custom sign is use by another feature.');	
		}
		echo json_encode($arr);
	}
}

?>