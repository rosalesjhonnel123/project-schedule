<?php
class Writingoptions extends Controller{
	
	function __construct(){
		parent::__construct();
		$this->writingoptionmodel = new writingoptionmodel();
		$this->permissionsmodel = new permissionsmodel();
		$this->customsignsmodel = new customsignsmodel();
		$this->authmodel = new authmodel();
		$this->authmodel->isLoggedIn();
		$this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
		$this->has_permission = $this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
	}

	public function index(){
		$role = $this->permissionsmodel->get_role_by_id(get_current_user_id());
		$status = $this->input->get('status');
		
		$this->view->assign('title','Writing Options');
		$this->view->assign('role', $role);
		$this->view->assign('status', $status);
		$this->view->display('admin/writingoptions/index');
	}

	public function allWritingOptions(){
		$status = $this->input->get('status') ?? 'active';
		$list = $this->writingoptionmodel->allWritingOptions($status);
		echo json_encode(array("data" => $list));
	}

	public function getByID(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->get('id');
		$result = $this->writingoptionmodel->getByID($id);
		echo json_encode(array($result));

	}

	public function update(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->post('id');
		$name = $this->input->post('writing_option_name');
		$status = $this->input->post('status');

		$writingoption = $this->customsignsmodel->getByWritingOptionID($id);

		if (!empty($writingoption)) {
			$arr = array('err' => 1, 'msg' => 'We cannot continue this process. This writing option is use by another feature.');	
			echo json_encode($arr);
			die();
		}

		if ($name === '') {
			$arr = array('err' => 1, 'msg' => 'Name is required.');
		} else {
			$data = array('name' => $name, 'status' => $status);

			$result = $this->writingoptionmodel->update($id,$data);
			if( is_wp_error( $result  ) ) {
			    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Writing option was successfully updated.');
			}
		}
		echo json_encode($arr);
	}

	public function create(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$name = $this->input->post('writing_option_name') ?? '';
		
		if ($name === '') {
			$arr = array('err' => 1, 'msg' => 'Name is required.');
		} else {
			$data = array('name' => $name, 'status' => 1);

			$result = $this->writingoptionmodel->create($data);

			if( is_wp_error( $result  ) ) {
			    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Writing option was successfully added.');
			}
 		}
		
		echo json_encode($arr);
	}

	public function delete(){
		$id = $this->input->post('id');
		$writingoption = $this->customsignsmodel->getByWritingOptionID($id);

		if (empty($writingoption)) {
			$delete = $this->writingoptionmodel->delete($id);
			if( is_wp_error( $delete  ) ) {
		    	$arr = array('err' => 1, 'msg' => $delete->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Writing option was deleted successfully.');
			}
			echo json_encode($arr);
			die();
		} else {
			$arr = array('err' => 1, 'msg' => 'We cannot continue this process. This writing option is use by another feature.');	
		}
		echo json_encode($arr);
	}
}

?>