<?php
class Finishmaterialtemplates extends Controller{
	
	function __construct(){
		parent::__construct();
		$this->finishmaterialmodel = new finishmaterialmodel();
		$this->customsignsmodel = new customsignsmodel();
		$this->platesizemodel = new platesizemodel();
		$this->finishmaterialtemplatesmodel = new finishmaterialtemplatesmodel();
		$this->permissionsmodel = new permissionsmodel();
		$this->authmodel = new authmodel();
		$this->authmodel->isLoggedIn();
		$this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
		$this->has_permission = $this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
	}

	public function index(){
		$finish_materials = $this->finishmaterialmodel->allActiveFinishMaterials();
		$plate_sizes = $this->platesizemodel->allActivePlateSizes();
		$role = $this->permissionsmodel->get_role_by_id(get_current_user_id());
		$status = $this->input->get('status');

		$this->view->assign('title','Finish Materials Templates');
		$this->view->assign('plate_sizes', $plate_sizes);
		$this->view->assign('finish_materials', $finish_materials);
		$this->view->assign('role', $role);
		$this->view->assign('status', $status);
		$this->view->display('admin/finishmaterialtemplates/index');
	}

	public function allFinishMaterialTemplates(){
		$status = $this->input->get('status') ?? 'active';
		$list = $this->finishmaterialtemplatesmodel->allFinishMaterialTemplates($status);
		echo json_encode(array("data" => $list));
	}

	public function getByPlateSizeIdFinishMaterialId(){

		$finish_material_id = $this->input->get('finish_material_id') ?? '';
		$plate_size_id = $this->input->get('plate_size_id') ?? '';
		$result = $this->finishmaterialtemplatesmodel->getByPlateSizeIdFinishMaterialId($plate_size_id, $finish_material_id);
		echo json_encode(array($result));

	}

	public function getByID(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->get('id');
		$result = $this->finishmaterialtemplatesmodel->getByID($id);
		echo json_encode(array($result));

	}

	public function update(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->post('id');
		$finish_material_id = $this->input->post('finish_material_id') ?? '';
		$plate_size_id = $this->input->post('plate_size_id') ?? '';
		$image = $this->input->file('image');
		$status = $this->input->post('status');

		$template = $this->finishmaterialtemplatesmodel->getByID($id);
		$custom_sign = $this->customsignsmodel->getByPlateSizeIDFinishMaterialID($template->plate_size_id, $template->finish_material_id);
		if (!empty($custom_sign)) {
			$arr = array('err' => 1, 'msg' => 'We cannot continue this process. This finish material template is use by another feature.');	
			echo json_encode($arr);
			die();
		}
		
		if (empty($image['name'])) {
			$filename = $this->input->post('currentimage');
			$data = array('plate_size_id' => $plate_size_id, 'finish_material_id' => $finish_material_id, 'image' => $filename, 'status' => $status);

			$result = $this->finishmaterialtemplatesmodel->update($id, $data);
			if( is_wp_error( $result  ) ) {
			    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Finish Material Template was successfully updated.');
			}
		}
		else{
			$checkfile = $this->upload->check($image);
			if($checkfile){
				$arr = array('err' => 1, 'msg' => 'File already exist.');
			}
			else{

				$valid = $this->upload->isValidImage($image);
				if($valid){
					$upload = $this->upload->process($image);
					if($upload != ''){
						$data = array('plate_size_id' => $plate_size_id, 'finish_material_id' => $finish_material_id, 'image' => $upload, 'status' => $status);
						$result = $this->finishmaterialtemplatesmodel->update($id, $data);
						if( is_wp_error( $result  ) ) {
						    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
						} else {
							$arr = array('err' => 0, 'msg' => 'Finish Material Template was successfully updated.');
						}
					}
					else{
						$arr = array('err' => 1, 'msg' => 'Failed to upload file.');
					}
				}
				else{
					$arr = array('err' => 1, 'msg' => 'Invalid file size or type.');
				}
			}
		}
		echo json_encode($arr);
	}

	public function create(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$finish_material_id = $this->input->post('finish_material_id') ?? '';
		$plate_size_id = $this->input->post('plate_size_id') ?? '';
		$image = $this->input->file('image');

		if (empty($image['name'])) {
			$filename = 'default.png';
			$data = array('plate_size_id' => $plate_size_id, 'finish_material_id' => $finish_material_id, 'image' => $filename, 'status' => 1);

			$result = $this->finishmaterialtemplatesmodel->create($data);

			if( is_wp_error( $result  ) ) {
			    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Finish Material Template was successfully added.');
			}
		}
		else{
			$checkfile = $this->upload->check($image);
			if($checkfile){
				$arr = array('err' => 1, 'msg' => 'File already exist.');
			}
			else{

				$valid = $this->upload->isValidImage($image);
				if($valid){
					$upload = $this->upload->process($image);
					if($upload != ''){
						$data = array('plate_size_id' => $plate_size_id, 'finish_material_id' => $finish_material_id, 'image' => $upload, 'status' => 1);
						$result = $this->finishmaterialtemplatesmodel->create($data);

						if( is_wp_error( $result  ) ) {
						    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
						} else {
							$arr = array('err' => 0, 'msg' => 'Finish Material Template was successfully added.');
						}
					}
					else{
						$arr = array('err' => 1, 'msg' => 'Failed to upload file.');
					}
				}
				else{
					$arr = array('err' => 1, 'msg' => 'Invalid file size or type.');
				}
			}
		}
		
		echo json_encode($arr);
	}

	public function delete(){
		$id = $this->input->post('id');
		$template = $this->finishmaterialtemplatesmodel->getByID($id);
		$custom_sign = $this->customsignsmodel->getByPlateSizeIDFinishMaterialID($template->plate_size_id, $template->finish_material_id);
		if (empty($custom_sign)) {
			$delete = $this->finishmaterialtemplatesmodel->delete($id);
			if( is_wp_error( $delete  ) ) {
		    	$arr = array('err' => 1, 'msg' => $delete->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Finish Material Template was deleted successfully.');
			}
			echo json_encode($arr);
			die();
		} else {
			$arr = array('err' => 1, 'msg' => 'We cannot continue this process. This finish material template is use by another feature.');	
		}
		echo json_encode($arr);
	}
}

?>