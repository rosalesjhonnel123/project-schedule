<?php
class Home extends Controller{	
	function __construct(){
		parent::__construct();
		$this->permissionsmodel = new permissionsmodel();
		$this->authmodel = new authmodel();
		$this->authmodel->isLoggedIn();
		$this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
		$this->has_permission = $this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
	}

	public function index()
	{
		if (is_user_logged_in()) {
			$id = get_current_user_id();
			$permissions = $this->permissionsmodel->getByID($id);
			if (count($permissions)) {
				$this->redirect('projects');
			} else {
				header('Location:'.site_url());
			}
			
		} else {
			header('Location:'.site_url());
		}
		
	}
}

?>