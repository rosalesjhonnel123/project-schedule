<?php
class Platesizes extends Controller{
	
	function __construct(){
		parent::__construct();
		$this->platesizemodel = new platesizemodel();
		$this->customsignsmodel = new customsignsmodel();
		$this->finishmaterialtemplatesmodel = new finishmaterialtemplatesmodel();
		$this->permissionsmodel = new permissionsmodel();
		$this->authmodel = new authmodel();
		$this->authmodel->isLoggedIn();
		$this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
		$this->has_permission = $this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
	}

	public function index(){
		$role = $this->permissionsmodel->get_role_by_id(get_current_user_id());
		$status = $this->input->get('status');

		$this->view->assign('title','Plate Sizes');
		$this->view->assign('role', $role);
		$this->view->assign('status', $status);
		$this->view->display('admin/platesizes/index');
	}

	public function allPlateSizes(){
		$status = $this->input->get('status') ?? 'active';
		$list = $this->platesizemodel->allPlateSizes($status);
		echo json_encode(array("data" => $list));
	}

	public function getByID(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->get('id');
		$result = $this->platesizemodel->getByID($id);
		echo json_encode(array($result));

	}

	public function update(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->post('id');
		$name = $this->input->post('plate_size_name');
		$status = $this->input->post('status');
		$platesize = $this->finishmaterialtemplatesmodel->getByPlateSizeID($id);
		$platesize2 = $this->customsignsmodel->getByPlateSizeID($id);

		if (!empty($platesize) && !empty($platesize2)) {
			$arr = array('err' => 1, 'msg' => 'We cannot continue this process. This plate size is use by another feature.');	
			echo json_encode($arr);
			die();
		}

		if ($name === '') {
			$arr = array('err' => 1, 'msg' => 'Name is required.');
		} else {
			$data = array('name' => $name, 'status' => $status);

			$result = $this->platesizemodel->update($id,$data);

			if( is_wp_error( $result  ) ) {
			    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
			} {
				$arr = array('err' => 0, 'msg' => 'Plate size was successfully updated.');
			}
		}
		echo json_encode($arr);
	}

	public function create(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$name = $this->input->post('plate_size_name') ?? '';
		
		if ($name === '') {
			$arr = array('err' => 1, 'msg' => 'Name is required.');
		} else {
			$data = array('name' => $name, 'status' => 1);

			$result = $this->platesizemodel->create($data);

			if( is_wp_error( $result  ) ) {
			    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Plate size was successfully added.');
			}
 		}
		
		echo json_encode($arr);
	}

	public function delete(){
		$id = $this->input->post('id');
		$platesize = $this->finishmaterialtemplatesmodel->getByPlateSizeID($id);
		$platesize2 = $this->customsignsmodel->getByPlateSizeID($id);

		if (empty($platesize) && empty($platesize2)) {
			$delete = $this->platesizemodel->delete($id);
			if( is_wp_error( $delete  ) ) {
		    	$arr = array('err' => 1, 'msg' => $delete->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Plate size was deleted successfully.');
			}
			echo json_encode($arr);
			die();
		} else {
			$arr = array('err' => 1, 'msg' => 'We cannot continue this process. This plate size is use by another feature.');	
		}
		echo json_encode($arr);
	}
}

?>