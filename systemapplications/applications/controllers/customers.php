<?php
require_once(ABSPATH.'wp-admin/includes/user.php');

class Customers extends Controller{	
	function __construct(){
		parent::__construct();
		$this->customersmodel = new customersmodel();
		$this->permissionsmodel = new permissionsmodel();
		$this->projectsmodel = new projectsmodel();
		$this->authmodel = new authmodel();
		$this->authmodel->isLoggedIn();
		$this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
		$this->has_permission = $this->permissionsmodel->has_permission($this->uri->segment(2), $this->uri->segment(3));
	}

	public function index()
	{
		$role = $this->permissionsmodel->get_role_by_id(get_current_user_id());
		$permissions = $this->permissionsmodel->allPermissions();
		$this->view->assign('title','Customer List');
		$this->view->assign('permissions', $permissions);
		$this->view->assign('role', $role);
		$this->view->display('admin/customers/index');
	}

	public function allCustomers()
	{
		$customers = $this->customersmodel->allCustomers();

		echo json_encode(['data' => $customers]);
	}


	public function getByID(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->get('id');
		$result = $this->customersmodel->getById($id);
		$permission = $this->permissionsmodel->get_user_permission_by_id($id);
		$data = [];
		foreach ($permission as $value) {
			$data[] = $value->permission_id;
		}
		$result['permission'] = $data;
		echo json_encode($result);
	}

	public function update(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$id = $this->input->post('id');
		$username = $this->input->post('username');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$contact_no = $this->input->post('contact_no');
		$email = $this->input->post('email');
		$company_name = $this->input->post('company_name');
		$company_address = $this->input->post('company_address');
		$user_nicename = ($first_name) ? strtolower(str_replace(" ", "-", $first_name)) : '';
		$permission = $_POST['permission'] ?? [];
		$customer_data = [
			'ID' => $id,
			'user_email' => $email,
			'user_nicename' => $user_nicename,
			'display_name' => $first_name,
			'user_login' => $username,
		];

		$customer_meta_data = [
			'first_name' => $first_name,
			'last_name' => $last_name,
			'contact_no' => $contact_no,
			'company_name' => $company_name,
			'company_address' => $company_address,
		];

		$result = $this->customersmodel->update($id, $customer_data, $customer_meta_data);
		
		$this->permissionsmodel->delete_user_permission($id);
		
		if (!empty($permission)) {
			foreach ($permission as $value) {
				$this->permissionsmodel->create_user_permission(
					[
						'user_id' => $id,
						'permission_id' => $value
					]
				);
			}
		}
		

		if( is_wp_error( $result  ) ) {
		    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
		}
		else {
			$arr = array('err' => 0, 'msg' => 'Customer was updated successfully.');
		}

		echo json_encode($arr);
	}

	public function create(){
		if (!$this->has_permission && $this->permissionsmodel->get_role_by_id(get_current_user_id()) != 'administrator') {
			echo json_encode(array('err' => 1, 'msg' => "You don't have permission to proceed this action."));
			exit;
		}
		$username = $this->input->post('username');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$contact_no = $this->input->post('contact_no');
		$email = $this->input->post('email');
		$company_name = $this->input->post('company_name');
		$company_address = $this->input->post('company_address');
		$user_nicename = ($first_name) ? strtolower(str_replace(" ", "-", $first_name)) : '';
		$permission = $_POST['permission'] ?? [];
		$password = md5(wp_generate_password(12));
		$customer_data = [
			'user_email' => $email,
			'user_nicename' => $user_nicename,
			'display_name' => $first_name,
			'user_login' => $username,
			'user_pass' => $password,
			'role' => 'project_customer',
		];

		$customer_meta_data = [
			'first_name' => $first_name,
			'last_name' => $last_name,
			'contact_no' => $contact_no,
			'company_name' => $company_name,
			'company_address' => $company_address,
			'pw_user_status' => 'approved',
		];

		$result = $this->customersmodel->create($customer_data, $customer_meta_data);
		$this->permissionsmodel->create_user_permission(
			[
				'user_id' => $result,
				'permission_id' => 1
			]
		);
		if (!empty($permission)) {
			foreach ($permission as $value) {
				$this->permissionsmodel->create_user_permission(
					[
						'user_id' => $result,
						'permission_id' => $value
					]
				);
			}
		}

		if( is_wp_error( $result  ) ) {
		    $arr = array('err' => 1, 'msg' => $result->get_error_message());		
		}
		else {
			$user_login = $username;
	        $user_email = $email;
	        $from_email = get_option('admin_email');
	        // The blogname option is escaped with esc_html on the way into the database in sanitize_option
	        // we want to reverse this for the plain text arena of emails.
	        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

	        $message  = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
	        $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
	        $message .= sprintf(__('E-mail: %s'), $user_email) . "\r\n";

	        $message  = sprintf(__('Username: %s'), $user_login) . "\r\n";
	        $message .= sprintf(__('Password: %s'), $password) . "\r\n";
	        $message .= site_url() ."/my-account". "\r\n";

			$headers = 'From: '.$first_name. ' <'. $from_email .'>'. "\r\n" .
		    'Reply-To: ' . $from_email . "\r\n";

	        wp_mail($user_email, sprintf(__('[%s] Your username and password'), $blogname), $message, $headers);
				$arr = array('err' => 0, 'msg' => 'Customer was successfully added.');
		}

		echo json_encode($arr);
	}

	public function delete(){
		$id = $this->input->post('id');
		$data = $this->projectsmodel->getProjectByCustomerId($id);

		if (empty($data)) {
			$delete = $this->customersmodel->delete($id);
			if( is_wp_error( $delete  ) ) {
		    	$arr = array('err' => 1, 'msg' => $delete->get_error_message());		
			} else {
				$arr = array('err' => 0, 'msg' => 'Customer was deleted successfully.');
			}
			echo json_encode($arr);
			die();
		} else {
			$arr = array('err' => 1, 'msg' => 'We cannot continue this process. This customer is use by another feature.');	
		}
		echo json_encode($arr);
	}
}