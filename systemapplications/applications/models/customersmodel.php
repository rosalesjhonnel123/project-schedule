<?php
class Customersmodel extends Model{

	public function allCustomers()
	{
		$args = [
		    'role__in' => ['project_customer'],
		    'orderby' => 'nicename',
		    'order' => 'ASC',
		    'fields' => 'all',
		];
		$users = get_users($args);
		$data = [];
		foreach ($users as $user) {
			$user_meta = get_user_meta($user->ID);
			$tmp['id'] = $user->ID;
			$tmp['email'] = $user->user_email;
			$tmp['first_name'] = !empty($user_meta['first_name']) ? $user_meta['first_name'][0] : '';
			$tmp['last_name'] = !empty($user_meta['last_name']) ? $user_meta['last_name'][0] : '';
			$tmp['full_name'] = $tmp['first_name']. ' ' .$tmp['last_name'];
			$tmp['company_name'] = !empty($user_meta['company_name']) ? $user_meta['company_name'][0] : '';
			$data[] = $tmp;
		}

		return $data;
	}

	public function allActiveCustomers()
	{
		$args = [
		    'role__in' => ['project_customer'],
		    'orderby' => 'nicename',
		    'order' => 'ASC',
		    'fields' => 'all',
		];
		$users = get_users($args);
		$data = [];
		foreach ($users as $user) {
			$user_meta = get_user_meta($user->ID);
			$tmp['id'] = $user->ID;
			$tmp['email'] = $user->user_email;
			$tmp['first_name'] = !empty($user_meta['first_name']) ? $user_meta['first_name'][0] : '';
			$tmp['last_name'] = !empty($user_meta['last_name']) ? $user_meta['last_name'][0] : '';
			$tmp['full_name'] = $tmp['first_name']. ' ' .$tmp['last_name'];
			$tmp['company_name'] = !empty($user_meta['company_name']) ? $user_meta['company_name'][0] : '';
			$data[] = $tmp;
		}

		return $data;
	}

	public function getByID($id)
	{
		$customer = get_user_by( 'id', $id);
		$customer_meta = get_user_meta($id);
		$customer_info['id'] = $customer->ID;
		$customer_info['email'] = $customer->user_email;
		$customer_info['username'] = $customer->user_login;
		$customer_info['first_name'] = !empty($customer_meta['first_name']) ? $customer_meta['first_name'][0] : '';
		$customer_info['last_name'] = !empty($customer_meta['last_name']) ? $customer_meta['last_name'][0] : '';
		$customer_info['company_name'] = !empty($customer_meta['company_name']) ? $customer_meta['company_name'][0] : '';
		$customer_info['company_address'] = !empty($customer_meta['company_address']) ? $customer_meta['company_address'][0] : '';
		$customer_info['contact_no'] = !empty($customer_meta['contact_no']) ? $customer_meta['contact_no'][0] : '';

		return $customer_info;
	}

	public function create($data, $meta_data){
		$user = wp_insert_user($data);
		foreach($meta_data as $key => $value) {
		    update_user_meta($user, $key, $value);
		}

		return $user;
	}

	public function update($id, $data, $meta_data){
		foreach($meta_data as $key => $value) {
		    update_user_meta($id, $key, $value);
		}
		return  wp_update_user($data);
	}

	public function delete($id){
		$meta_data = get_user_meta($id);
		foreach($meta_data as $key => $value) {
		    delete_user_meta($id, $key, $value);
		}
		
		$this->db->delete('ds03_user_permissions',array('user_id' => $id));

		return wp_delete_user($id);
	}
}	

?>