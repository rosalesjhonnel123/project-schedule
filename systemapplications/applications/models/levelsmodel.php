<?php
class Levelsmodel extends Model{
	public function allLevels($status = ''){
		$query = "SELECT * FROM ds03_levels";
		if ($status) {
			$status = $status == 'active' ? 1 : 0;
			$query .= ' WHERE status='.$status;
		}
		return $this->db->get_results($query, OBJECT);
	}

	public function allActiveLevels(){
		return $this->db->get_results("SELECT * FROM ds03_levels WHERE STATUS = 1", OBJECT);
	}

	public function getByID($id){
		return $this->db->get_row("SELECT * FROM ds03_levels WHERE id = $id");
	}

	public function create($data){
		$result = $this->db->insert('ds03_levels',$data);
		return $result;
	}

	public function update($id,$data){
		$result = $this->db->update('ds03_levels', $data, array('id' => $id));
		return $result;
	}

	public function delete($id){
		$result = $this->db->delete('ds03_levels',array('id' => $id));
		return $result;
	}
}	

?>