<?php
class Finishmaterialtemplatesmodel extends Model{
	public function allFinishMaterialTemplates($status = ''){
		$query = "SELECT ds03_finish_material_templates.*, ds03_finish_material.name as finish_material, ds03_plate_size.name as plate_size FROM ds03_finish_material_templates JOIN ds03_finish_material ON ds03_finish_material_templates.finish_material_id = ds03_finish_material.id JOIN ds03_plate_size ON ds03_finish_material_templates.plate_size_id = ds03_plate_size.id";
		if ($status) {
			$status = $status == 'active' ? 1 : 0;
			$query .= ' WHERE ds03_finish_material_templates.status='.$status;
		}
		return $this->db->get_results($query, OBJECT);
	}

	public function allActiveFinishMaterialTemplates(){
		return $this->db->get_results("SELECT ds03_finish_material_templates.*, ds03_finish_material.name as finish_material, ds03_plate_size.name as plate_size FROM ds03_finish_material_templates JOIN ds03_finish_material ON ds03_finish_material_templates.finish_material_id = ds03_finish_material.id JOIN ds03_plate_size ON ds03_finish_material_templates.plate_size_id = ds03_plate_size.id WHERE ds03_finish_material_templates.status = 1", OBJECT);
	}

	public function getByID($id){
		return $this->db->get_row("SELECT * FROM ds03_finish_material_templates WHERE id = $id");
	}

	public function create($data){
		$result = $this->db->insert('ds03_finish_material_templates', $data);
		return $result;
	}

	public function update($id,$data){
		$result = $this->db->update('ds03_finish_material_templates', $data, array('id' => $id));
		return $result;
	}

	public function getByPlateSizeIdFinishMaterialId($plate_size_id, $finish_material_id){
		return $this->db->get_row("SELECT ds03_finish_material_templates.*, ds03_finish_material.name as finish_material, ds03_plate_size.name as plate_size FROM ds03_finish_material_templates JOIN ds03_finish_material ON ds03_finish_material_templates.finish_material_id = ds03_finish_material.id JOIN ds03_plate_size ON ds03_finish_material_templates.plate_size_id = ds03_plate_size.id WHERE ds03_finish_material_templates.plate_size_id = $plate_size_id AND  ds03_finish_material_templates.finish_material_id = $finish_material_id");
	}

	public function getByPlateSizeID($plate_size_id){
		return $this->db->get_row("SELECT * FROM ds03_finish_material_templates WHERE plate_size_id = $plate_size_id");
	}

	public function getByFinishMaterialID($finish_material_id){
		return $this->db->get_row("SELECT * FROM ds03_finish_material_templates WHERE finish_material_id = $finish_material_id");
	}

	public function delete($id){
		$result = $this->db->delete('ds03_finish_material_templates',array('id' => $id));
		return $result;
	}
}	

?>