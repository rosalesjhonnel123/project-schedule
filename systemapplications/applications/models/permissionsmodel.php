<?php
class Permissionsmodel extends Model{

	public function allPermissions(){
		$result = $this->db->get_results("SELECT * FROM ds03_permissions WHERE is_admin != 1",OBJECT);
		return $result;
	}
	
	public function getByID($id){
		return $this->db->get_row("SELECT * FROM ds03_permissions WHERE id = $id");
	}

	public function getByViewName($view_name){
		return $this->db->get_row("SELECT id FROM ds03_permissions WHERE view_name = '$view_name'");
	}

	public function create($data){
		$result = $this->db->insert('ds03_permissions',$data);
		return $result;
	}

	public function update($id,$data){
		$result = $this->db->update('ds03_permissions', $data, array('id' => $id));
		return $result;
	}

	public function get_user_permission_by_id($id){
		return $this->db->get_results("SELECT * FROM ds03_user_permissions WHERE user_id = $id");
	}

	public function create_user_permission($data){
		return $this->db->insert('ds03_user_permissions',$data);
	}

	public function delete_user_permission($id){
		return $this->db->delete('ds03_user_permissions',array('user_id' => $id));
	}

	public function has_permission($controller, $method){
		$has_permission = false;
		$method = $method ? '.'.$method : '';

		$result = $this->getByViewName($controller.$method);

		$id = get_current_user_id();
		$permissions = $this->db->get_results("SELECT permission_id FROM ds03_user_permissions WHERE user_id = $id", OBJECT);
		foreach ($permissions as $permission) {
			if ($result->id == $permission->permission_id) {
				$has_permission = true;
				break;
			}
		}

		$is_admin = $this->get_role_by_id(get_current_user_id()) == 'administrator' ? true : false;

		if (!$is_admin) {
			if (!$has_permission) {
				if ($result->id == 1) {
					header("Location:".site_url());
				} else {
					if(!$this->isAjax()) {
						$this->redirect('home');
						$this->session->set_userdata('error', true);
						$this->session->set_userdata('msg', "You don't have permission to proceed this action.");
					} else {
						return $has_permission;
					}
				}
			} 
		}

		return $has_permission;
	}

	public function isAjax() {
	  return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
	}

	public function get_role_by_id($id){
	    if ( !is_user_logged_in() ) { return false; }

	    $oUser = get_user_by( 'id', $id );
	    $aUser = get_object_vars( $oUser );
	    $sRole = $aUser['roles'][0];
	    return $sRole;
	}
}	

?>