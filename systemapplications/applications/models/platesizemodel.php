<?php
class Platesizemodel extends Model{

	public function allPlateSizes($status = ''){
		$query = "SELECT * FROM ds03_plate_size";
		if ($status) {
			$status = $status == 'active' ? 1 : 0;
			$query .= ' WHERE status='.$status;
		}
		return $this->db->get_results($query, OBJECT);
	}

	public function allActivePlateSizes(){
		return $this->db->get_results("SELECT * FROM ds03_plate_size WHERE STATUS = 1", OBJECT);
	}

	public function getByID($id){
		return $this->db->get_row("SELECT * FROM ds03_plate_size WHERE id = $id");
	}

	public function create($data){
		$result = $this->db->insert('ds03_plate_size', $data);
		return $result;
	}

	public function update($id,$data){
		$result = $this->db->update('ds03_plate_size', $data, array('id' => $id));
		return $result;
	}

	public function delete($id){
		$result = $this->db->delete('ds03_plate_size',array('id' => $id));
		return $result;
	}
}	

?>