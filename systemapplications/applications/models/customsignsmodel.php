<?php
class Customsignsmodel extends Model{
	
	public function allCustomSigns($customer_id = '', $status = ''){
		$query = '';
		if ($customer_id) {
			$query .= ' WHERE ds03_projects.customer_id='.$customer_id;

			if ($status) {
				$status = $status == 'active' ? 1 : 0;
				$query .= ' AND ds03_custom_signs.status='.$status;
			}
		} else {
			if ($status) {
				$status = $status == 'active' ? 1 : 0;
				$query .= ' WHERE ds03_custom_signs.status='.$status;
			}
		}

		return $this->db->get_results("SELECT ds03_custom_signs.*, ds03_plate_size.name as plate_size_name, ds03_writing_option.name as writing_option_name, ds03_finish_material.name as finish_material_name, ds03_projects.project_name FROM ds03_custom_signs JOIN ds03_plate_size ON ds03_plate_size.id = ds03_custom_signs.plate_size_id JOIN ds03_finish_material ON ds03_finish_material.id = ds03_custom_signs.finish_material_id JOIN ds03_writing_option ON ds03_writing_option.id = ds03_custom_signs.writing_option_id JOIN ds03_projects ON ds03_projects.id = ds03_custom_signs.project_id".$query, OBJECT);
	}

	public function allCustomSignsLimitOffset($customer_id = '', $status = '', $limit, $offset){
		$query = '';
		if ($customer_id) {
			$query .= ' WHERE ds03_projects.customer_id='.$customer_id;

			if ($status) {
				$status = $status == 'active' ? 1 : 0;
				$query .= ' AND ds03_custom_signs.status='.$status;
				$query .= ' LIMIT $offset, $limit';
			}
		} else {
			if ($status) {
				$status = $status == 'active' ? 1 : 0;
				$query .= ' WHERE ds03_custom_signs.status='.$status;
				$query .= ' LIMIT $offset, $limit';
			}
		}



		return $this->db->get_results("SELECT ds03_custom_signs.*, ds03_plate_size.name as plate_size_name, ds03_writing_option.name as writing_option_name, ds03_finish_material.name as finish_material_name, ds03_projects.project_name FROM ds03_custom_signs JOIN ds03_plate_size ON ds03_plate_size.id = ds03_custom_signs.plate_size_id JOIN ds03_finish_material ON ds03_finish_material.id = ds03_custom_signs.finish_material_id JOIN ds03_writing_option ON ds03_writing_option.id = ds03_custom_signs.writing_option_id JOIN ds03_projects ON ds03_projects.id = ds03_custom_signs.project_id".$query, OBJECT);
	}

	public function allActiveCustomSigns(){
		return $this->db->get_results("SELECT ds03_custom_signs.*, ds03_plate_size.name as plate_size_name, ds03_writing_option.name as writing_option_name, ds03_finish_material.name as finish_material_name, ds03_projects.project_name FROM ds03_custom_signs JOIN ds03_plate_size ON ds03_plate_size.id = ds03_custom_signs.plate_size_id JOIN ds03_finish_material ON ds03_finish_material.id = ds03_custom_signs.finish_material_id JOIN ds03_writing_option ON ds03_writing_option.id = ds03_custom_signs.writing_option_id JOIN ds03_projects ON ds03_projects.id = ds03_custom_signs.project_id WHERE ds03_custom_signs.status = 1", OBJECT);
	}

	public function getByID($id){
		return $this->db->get_row("SELECT ds03_custom_signs.*, ds03_plate_size.name as plate_size_name, ds03_writing_option.name as writing_option_name, ds03_finish_material.name as finish_material_name FROM ds03_custom_signs JOIN ds03_plate_size ON ds03_plate_size.id = ds03_custom_signs.plate_size_id JOIN ds03_finish_material ON ds03_finish_material.id = ds03_custom_signs.finish_material_id JOIN ds03_writing_option ON ds03_writing_option.id = ds03_custom_signs.writing_option_id WHERE ds03_custom_signs.id = $id");
	}

	public function searchByName($name){
		return $this->db->get_results("SELECT ds03_custom_signs.*, ds03_plate_size.name as plate_size_name, ds03_writing_option.name as writing_option_name, ds03_finish_material.name as finish_material_name FROM ds03_custom_signs JOIN ds03_plate_size ON ds03_plate_size.id = ds03_custom_signs.plate_size_id JOIN ds03_finish_material ON ds03_finish_material.id = ds03_custom_signs.finish_material_id JOIN ds03_writing_option ON ds03_writing_option.id = ds03_custom_signs.writing_option_id WHERE ds03_custom_signs.custom_sign_name LIKE ('%$name%')", OBJECT);
	}

	public function searchByNameLimitOffset($name, $limit, $offset){
		return $this->db->get_results("SELECT ds03_custom_signs.*, ds03_plate_size.name as plate_size_name, ds03_writing_option.name as writing_option_name, ds03_finish_material.name as finish_material_name FROM ds03_custom_signs JOIN ds03_plate_size ON ds03_plate_size.id = ds03_custom_signs.plate_size_id JOIN ds03_finish_material ON ds03_finish_material.id = ds03_custom_signs.finish_material_id JOIN ds03_writing_option ON ds03_writing_option.id = ds03_custom_signs.writing_option_id WHERE ds03_custom_signs.custom_sign_name LIKE ('%$name%') LIMIT $offset, $limit", OBJECT);
	}


	public function create($data){
		$result = $this->db->insert('ds03_custom_signs', $data);
		return  $this->db->insert_id;
	}

	public function update($id,$data){
		$result = $this->db->update('ds03_custom_signs', $data, array('id' => $id));
		return $result;
	}

	public function updateProjectId($customer_id,$data){
		$result = $this->db->update('ds03_custom_signs', $data, array('customer_id' => $customer_id, 'project_id' => 0));
		return $result;
	}

	public function getByPlateSizeID($plate_size_id){
		return $this->db->get_row("SELECT * FROM ds03_custom_signs WHERE plate_size_id = $plate_size_id");
	}

	public function getByFinishMaterialID($finish_material_id){
		return $this->db->get_row("SELECT * FROM ds03_custom_signs WHERE finish_material_id = $finish_material_id");
	}

	public function getByWritingOptionID($writing_option_id){
		return $this->db->get_row("SELECT * FROM ds03_custom_signs WHERE writing_option_id = $writing_option_id");
	}

	public function getByPlateSizeIDFinishMaterialID($plate_size_id, $finish_material_id){
		return $this->db->get_row("SELECT * FROM ds03_custom_signs WHERE plate_size_id = $plate_size_id AND finish_material_id = $finish_material_id");
	}

	public function delete($id){
		$result = $this->db->delete('ds03_custom_signs',array('id' => $id));
		return $result;
	}
}	

?>