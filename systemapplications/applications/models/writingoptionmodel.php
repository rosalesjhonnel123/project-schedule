<?php
class Writingoptionmodel extends Model{
	public function allWritingOptions($status = ''){
		$query = "SELECT * FROM ds03_writing_option";
		if ($status) {
			$status = $status == 'active' ? 1 : 0;
			$query .= ' WHERE status='.$status;
		}
		return $this->db->get_results($query, OBJECT);
	}

	public function allActiveWritingOptions(){
		return $this->db->get_results("SELECT * FROM ds03_writing_option WHERE status = 1", OBJECT);
	}


	public function getByID($id){
		return $this->db->get_row("SELECT * FROM ds03_writing_option WHERE id = $id");
	}

	public function create($data){
		$result = $this->db->insert('ds03_writing_option', $data);
		return $result;
	}

	public function update($id,$data){
		$result = $this->db->update('ds03_writing_option', $data, array('id' => $id));
		return $result;
	}

	public function delete($id){
		$result = $this->db->delete('ds03_writing_option',array('id' => $id));
		return $result;
	}
}	

?>