<?php
class Projectlevelsmodel extends Model{
	
	public function create($data){
		$result = $this->db->insert('ds03_project_levels', $data);
		return $result;
	}

	public function getByID($id){
		$result = $this->db->get_results("SELECT ds03_levels.*, ds03_project_levels.* FROM ds03_project_levels JOIN ds03_levels ON ds03_levels.id = ds03_project_levels.level_id WHERE project_id = $id", OBJECT);
		return $result;
	}

	public function delete($id){
		$result = $this->db->delete('ds03_project_levels',array('project_id' => $id));
		return $result;
	}

	public function getByLevelID($id){
		$result = $this->db->get_results("SELECT * FROM ds03_project_levels WHERE level_id = $id", OBJECT);
		return $result;
	}
}	

?>