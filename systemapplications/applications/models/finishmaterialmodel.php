<?php
class Finishmaterialmodel extends Model{
	public function allFinishMaterials($status = ''){
		$query = "SELECT * FROM ds03_finish_material";
		if ($status) {
			$status = $status == 'active' ? 1 : 0;
			$query .= ' WHERE status='.$status;
		}
		return $this->db->get_results($query, OBJECT);
	}

	public function allActiveFinishMaterials(){
		return $this->db->get_results("SELECT * FROM ds03_finish_material WHERE status = 1", OBJECT);
	}

	public function getByID($id){
		return $this->db->get_row("SELECT * FROM ds03_finish_material WHERE id = $id");
	}

	public function create($data){
		$result = $this->db->insert('ds03_finish_material', $data);
		return $result;
	}

	public function update($id,$data){
		$result = $this->db->update('ds03_finish_material', $data, array('id' => $id));
		return $result;
	}

	public function delete($id){
		$result = $this->db->delete('ds03_finish_material',array('id' => $id));
		return $result;
	}
}	

?>