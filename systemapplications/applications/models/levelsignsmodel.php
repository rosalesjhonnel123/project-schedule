<?php
class Levelsignsmodel extends Model{
	
	public function create($data){
		$result = $this->db->insert('ds03_level_signs', $data);
		return $result;
	}

	public function update($project_id, $level_id, $sign_id, $data){
		$result = $this->db->update('ds03_level_signs', $data, array('project_id' => $project_id, 'level_id' => $level_id, 'sign_id' => $sign_id));
		return $result;
	}

	public function getByID($id){
		$result = $this->db->get_results("SELECT ds03_level_signs.*, ds03_levels.name FROM ds03_level_signs JOIN ds03_levels ON ds03_levels.id = ds03_level_signs.level_id WHERE ds03_level_signs.project_id = $id", OBJECT);
		return $result;
	}

	public function delete($id){
		$result = $this->db->delete('ds03_level_signs',array('project_id' => $id));
		return $result;
	}

	public function getByProjectIdLevelIdSignId($project_id, $level_id, $sign_id, $is_custom){
		$result = $this->db->get_row("SELECT * FROM ds03_level_signs WHERE project_id = $project_id AND level_id = $level_id AND sign_id = $sign_id AND is_custom = $is_custom", OBJECT);
		return $result;
	}

	public function getByProjectIdSignId($project_id, $sign_id, $is_custom){
		$result = $this->db->get_row("SELECT * FROM ds03_level_signs WHERE project_id = $project_id AND sign_id = $sign_id AND is_custom = $is_custom", OBJECT);
		return $result;
	}

	public function deleteByLevelId($level_id){
		$result = $this->db->delete('ds03_level_signs',array('level_id' => $level_id));
		return $result;
	}

	public function getByCustomSignId($sign_id)
	{
		$result = $this->db->get_row("SELECT * FROM ds03_level_signs WHERE is_custom = 1 AND sign_id = $sign_id", OBJECT);
		return $result;
	}
}	

?>