<?php
class Projectsmodel extends Model{

	public function allProjects($customer_id = '', $status = ''){
		$query = "SELECT * FROM ds03_projects";
		if ($customer_id) {
			$query .= ' WHERE customer_id='.$customer_id;

			if ($status) {
				$status = $status == 'active' ? 1 : 0;
				$query .= ' AND status='.$status;
			}
		} else {
			if ($status) {
				$status = $status == 'active' ? 1 : 0;
				$query .= ' WHERE status='.$status;
			}
		}

		$result = $this->db->get_results($query, OBJECT);
		$data = [];
		
		foreach ($result as $value) {
			$user_meta = get_user_meta($value->customer_id);
			$first_name = !empty($user_meta['first_name']) ? $user_meta['first_name'][0] : '';
			$last_name = !empty($user_meta['last_name']) ? $user_meta['last_name'][0] : '';
			$tmp['id'] = $value->id;
			$tmp['is_approved'] = $value->is_approved;
			$tmp['full_name'] = $first_name. ' ' .$last_name;
			$tmp['project_name'] = $value->project_name;
			$tmp['project_address'] = $value->project_address;
			$tmp['created_at'] = $value->created_at;
			$tmp['status'] = $value->status;
			$data[] = $tmp;
		}

		return $data;
	}

	public function allActiveProjects($customer_id = ''){
		$query = "SELECT * FROM ds03_projects WHERE status = 1";
		if ($customer_id) {
			$query .= ' AND customer_id='.$customer_id;
		}
		$result = $this->db->get_results($query, OBJECT);
		$data = [];
		
		foreach ($result as $value) {
			$user_meta = get_user_meta($value->customer_id);
			$first_name = !empty($user_meta['first_name']) ? $user_meta['first_name'][0] : '';
			$last_name = !empty($user_meta['last_name']) ? $user_meta['last_name'][0] : '';
			$tmp['id'] = $value->id;
			$tmp['is_approved'] = $value->is_approved;
			$tmp['full_name'] = $first_name. ' ' .$last_name;
			$tmp['project_name'] = $value->project_name;
			$tmp['project_address'] = $value->project_address;
			$tmp['created_at'] = $value->created_at;
			$tmp['status'] = $value->status;
			$data[] = $tmp;
		}

		return $data;
	}
	
	public function getProjectByID($id){
		$result = $this->db->get_row("SELECT * FROM ds03_projects WHERE id = $id");
		return $result;
	}

	public function getProjectByCustomerId($id){
		$result = $this->db->get_row("SELECT * FROM ds03_projects WHERE customer_id = $id");
		return $result;
	}

	public function createProject($data){
		$result = $this->db->insert('ds03_projects',$data);
		return  $this->db->insert_id;
	}

	public function updateProject($id,$data){

		$row = $this->getProjectByID($id);
		$result = $this->db->update('ds03_projects',$data,array('id' => $id));
		return $result;
	}

	public function deleteProject($id){
		$row = $this->getProjectByID($id);
		$result = $this->db->delete('ds03_projects',array('id' => $id));
		$this->db->delete('ds03_project_levels',array('project_id' => $id));
		$this->db->delete('ds03_level_signs',array('project_id' => $id));
		return $result;
	}
}	

?>