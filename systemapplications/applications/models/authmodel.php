<?php
class Authmodel extends Model{

    public function isLoggedIn()
    {
           
        $is_logged_in = is_user_logged_in();

        if(!isset($is_logged_in) || $is_logged_in!==TRUE)
        {
            $this->redirect('/');
            exit;
        }
    }
}
?>