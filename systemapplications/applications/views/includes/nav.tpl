<nav class="woocommerce-MyAccount-navigation mt-8 mb-5" style="width:100%;">
	<ul>
		<li><a href="{$base}projects/create">ADD PROJECT</a></li>
		<li><a href="{$base}projects">VIEW SAVE PROJECT</a></li>
		{if $role == 'administrator'}
		<li><a href="{$base}customers">CUSTOMERS</a></li>
		<li><a href="{$base}levels">LEVELS</a></li>
		<li><a href="{$base}platesizes">PLATE SIZES</a></li>
		<li><a href="{$base}finishmaterials">FINISH MATERIALS</a></li>
		<li><a href="{$base}writingoptions">WRITING OPTIONS</a></li>
		<li><a href="{$base}finishmaterialtemplates">FINISH MATERIAL TEMPLATES</a></li>
		{/if}
		<li><a href="{$base}customsigns">CUSTOM SIGNS</a></li>
	</ul>
</nav>