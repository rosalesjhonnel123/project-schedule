<section class="page-section woocommerce-account woocommerce-page woocommerce-edit-account">
	<div class="container woocommerce">			
		<div class="col-md-9">
			<div id="table-holder">
				<h1 class="text-left mb-5 font-weight-700">{$title}</h1>
				<button type="button" class="btn btn-info add create-btn mb-5">CREATE PERMISSION</button>
				<div id="loader" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message" hidden></div>
				<table id="example" class="table table-hover dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Name</th>
							<th>Action</th>
						</tr>
						<tbody>
							
						</tbody>	
					</thead>	
				</table>	
			</div>	
		</div>
		<div class="col-md-3">
			{include file='includes/nav.tpl'}
		</div>
	</div>
</section>
<div class="modal fade" id="addPermission" tabindex="-1" role="dialog" aria-labelledby="addPermission" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="add-permisssion-form" role="form" method="post" action="">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel">Create Permission</h4>
			</div>
			<div class="modal-body">
				<div id="loader2" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message2" hidden></div>
				<div class="form-group">
					 <label class="control-label" for="permission_name">Name</label>  
					  <input id="permission_name" name="permission_name" type="text" placeholder="Enter Name here" class="form-control input-md">
				</div>
			</div>		
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">SUBMIT
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<div class="modal fade" id="updatePermission" tabindex="-1" role="dialog" aria-labelledby="updatePermission" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="update-permission-form" role="form" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel">Edit Permission</h4>
			</div>
			<div class="modal-body">
				<div id="loader3" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message3" hidden></div>
				<input type="hidden" name="id" id="id"></input>
				<div class="form-group">
					 <label class="control-label" for="permission_name">Name</label>  
					  <input id="permission_name2" name="permission_name" type="text" placeholder="Enter name here" class="form-control input-md">
				</div>
			</div>		
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">UPDATE
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="{$template_base_url}/datatables/jquery.dataTables.min.js"></script>
<script src="{$template_base_url}/datatables/dataTables.bootstrap.min.js"></script> 
<script src="{$template_base_url}/datatables/dataTables.responsive.min.js"></script> 
<script src="{$template_base_url}/datatables/responsive.bootstrap.min.js"></script>  
<script src="{$template_base_url}/datatables/dataTables.searchHighlight.min.js"></script>    
<script src="{$template_base_url}/datatables/jquery.highlight.js"></script> 
<script src="{$template_base_url}/js/fnReloadAjax.js"></script> 
<script src="{$template_base_url}/js/main.js"></script> 
<script type="text/javascript">

$(document).ready(function(){	
	var table = $('#example');

  		table.dataTable( {
  			searchHighlight: true,
  			"order": [1,'desc'],
  			oLanguage: {
  				"bDeferRender": true,
				"bProcessing": true,	
       			sProcessing: "<img src='{$img_dir}ajax-loader.gif'>"
    		},
			"sAjaxSource": '{$base}permissions/allPermissions',
			"bDeferRender": true,
			"bProcessing": true,
			"aoColumns": [
			{
				"mDataProp": "name"
			},	
			{
				"mDataProp": "id",
				"mRender": function(data, type, row) {
					return '<a href="javascript:;" class="btn btn-sm btn-warning edit" title="Edit" id="'+row.id+'"><i class="fa fa-edit"></i></a>';
				}	
			}

			]});	

		$('body').on('click','.add',function(e){
			e.preventDefault();
			
			$('#addPermission').modal('show');
		});

		$('body').on('click','.edit',function(e){
			e.preventDefault();
			
			$('#loader').show();
			var target = $(this);
			var id = target.attr('id');
			var data = convertToObject(id);
			$.getJSON("{$base}permissions/getByID",data,function(result){
				$('#loader').hide();
				$('#id').val(result[0].id);
				$('#permission_name2').val(result[0].name);
				$('#updatePermission').modal('show');
			});

		});

		$('#add-permisssion-form').submit(function(e){
			e.preventDefault();

			$('#loader2').show();

			$.post("{$base}permissions/create",$(this).serialize(),function(result){
				$('#loader2').hide();
				if(result.err == 0){
					$('#message2').html(successMsg(result.msg)).fadeIn(1000,function(){
						$("#addPermission").scrollTop(0).animate(1000);
						table.fnReloadAjax();
					});
				}
				else{
					$('#message2').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#addPermission").scrollTop(0).animate(1000);
					});
				}
			},"json");
		});

		$('#update-permission-form').submit(function(e){
			e.preventDefault();

			$('#loader3').show();

			$.post("{$base}permissions/update",$(this).serialize(),function(result){
				$('#loader3').hide();
				if(result.err == 0){
					$('#message3').html(successMsg(result.msg)).fadeIn(1000,function(){
						$("#updatePermission").scrollTop(0).animate(1000);
						table.fnReloadAjax();
					});
				}
				else{
					$('#message3').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#updatePermission").scrollTop(0).animate(1000);
					});
				}
				
			},"json");
		});
	
});	

</script>