<section class="page-section woocommerce-account woocommerce-page woocommerce-edit-account">
	<div class="container woocommerce">			
		<div class="row">
			<div class="col-md-9">
				<div id="table-holder">
					<h1 class="text-left mb-5 font-weight-700">{$title}</h1>
					<button type="button" class="btn btn-info add create-btn mb-5">CREATE CUSTOMER</button>
					<div id="loader" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
					<div id="message" hidden></div>
					<table id="example" class="table table-hover dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Company Name</th>
								<th>Action</th>
							</tr>
							<tbody>
								
							</tbody>	
						</thead>	
					</table>	
				</div>	
			</div>
			<div class="col-md-3">
				{include file='includes/nav.tpl'}
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="addCustomer" tabindex="-1" role="dialog" aria-labelledby="addCustomer" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="add-customer-form" role="form" method="post" action="">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel">Create Customer</h4>
			</div>
			<div class="modal-body">
				<div id="loader2" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message2" hidden></div>
				<div class="form-group">
					 <label class="control-label" for="company_name">Company Name</label>  
					  <input id="company_name" name="company_name" type="text" placeholder="Enter company name here" class="form-control input-md">
				</div>	
				<div class="form-group">
					 <label class="control-label" for="company_address">Company Address</label>  
					  <input id="company_address" name="company_address" type="text" placeholder="Enter company address here" class="form-control input-md">
				</div>
				<div class="form-group">
					 <label class="control-label" for="first_name">First Name</label>  
					  <input id="first_name" name="first_name" type="text" placeholder="Enter first name here" class="form-control input-md">
				</div>	
				<div class="form-group">
					 <label class="control-label" for="last_name">Last Name</label>  
					  <input id="last_name" name="last_name" type="text" placeholder="Enter last name here" class="form-control input-md">
				</div>
				<div class="form-group">
					 <label class="control-label" for="username">Login Name</label>  
					  <input id="username" name="username" type="text" placeholder="Enter login name here" class="form-control input-md">
				</div>
				<div class="form-group">
					 <label class="control-label" for="email">Email</label>  
					  <input id="email" name="email" type="email" placeholder="Enter email here" class="form-control input-md">
				</div>
				<div class="form-group">
					 <label class="control-label" for="contact_no">Contact No.</label>  
					  <input id="contact_no" name="contact_no" type="text" placeholder="Enter contact no. here" class="form-control input-md">
				</div>
				<div class="form-group" style="height: 180px;">
					<label class="control-label d-block" for="contact_no">Select user permissions</label> 
					{foreach from = $permissions item = permission key = key} 
						{if $permission->id != 1}
						<span class="d-block col-md-6 col-sm-6"><input id="permission" name="permission[]" type="checkbox" value="{$permission->id}"> {ucwords($permission->name)}</span>
						{/if}
					{/foreach}
				</div>	
			</div>	
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Submit
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="Close">Close
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<div class="modal fade" id="updateCustomer" tabindex="-1" role="dialog" aria-labelledby="updateCustomer" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="update-customer-form" role="form" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel">Edit Customer</h4>
			</div>
			<div class="modal-body">
				<div id="loader3" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message3" hidden></div>
				<input type="hidden" name="id" id="id"></input>
				<div class="form-group">
					 <label class="control-label" for="company_name">Company Name</label>  
					  <input id="company_name2" name="company_name" type="text" placeholder="Enter company name here" class="form-control input-md">
				</div>	
				<div class="form-group">
					 <label class="control-label" for="company_address">Company Address</label>  
					  <input id="company_address2" name="company_address" type="text" placeholder="Enter company address here" class="form-control input-md">
				</div>
				<div class="form-group">
					 <label class="control-label" for="first_name">First Name</label>  
					  <input id="first_name2" name="first_name" type="text" placeholder="Enter first name here" class="form-control input-md">
				</div>	
				<div class="form-group">
					 <label class="control-label" for="last_name">Last Name</label>  
					  <input id="last_name2" name="last_name" type="text" placeholder="Enter last name here" class="form-control input-md">
				</div>
				<div class="form-group">
					 <label class="control-label" for="username">Login Name</label>  
					  <input id="username2" name="username" type="text" placeholder="Enter login name here" class="form-control input-md">
				</div>
				<div class="form-group">
					 <label class="control-label" for="email">Email</label>  
					  <input id="email2" name="email" type="email" placeholder="Enter email here" class="form-control input-md">
				</div>
				<div class="form-group">
					 <label class="control-label" for="contact_no">Contact No.</label>  
					  <input id="contact_no2" name="contact_no" type="text" placeholder="Enter contact no. here" class="form-control input-md">
				</div>	
				<div class="form-group" style="height: 180px;">
					<label class="control-label d-block" for="contact_no">Select user permissions</label> 
					{foreach from = $permissions item = permission key = key} 
						{if $permission->id != 1}
						<span class="d-block col-md-6 col-sm-6"><input class="permission" name="permission[]" type="checkbox" value="{$permission->id}"> {ucwords($permission->name)}</span>
						{/if}
					{/foreach}
				</div>	
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Update
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="Close">Close
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<div class="modal fade" id="deleteCustomer" tabindex="-1" role="dialog" aria-labelledby="deleteCustomer" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel"><span class="fa fa-exclamation" style="color:#D91E18;"></span></h4>
			</div>
			<div class="modal-body">
				<div id="loader4" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message4" hidden></div>
				<div class="form-group">
					<p>Are you sure you want to delete this customer?</p>
				</div>
			</div>
			<div class="modal-footer mt-3">
				<button id="yesBtn" type="button" class="btn btn-danger" title="Yes">Yes
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" title="No">No
				</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="{$template_base_url}/datatables/jquery.dataTables.min.js"></script>
<script src="{$template_base_url}/datatables/dataTables.bootstrap.min.js"></script> 
<script src="{$template_base_url}/datatables/dataTables.responsive.min.js"></script> 
<script src="{$template_base_url}/datatables/responsive.bootstrap.min.js"></script>  
<script src="{$template_base_url}/datatables/dataTables.searchHighlight.min.js"></script>    
<script src="{$template_base_url}/datatables/jquery.highlight.js"></script> 
<script src="{$template_base_url}/js/fnReloadAjax.js"></script> 
<script src="{$template_base_url}/js/main.js"></script> 
<script type="text/javascript">

$(document).ready(function(){	
	var table = $('#example');

  		table.dataTable( {
		searchHighlight: true,
		oLanguage: {
			"bDeferRender": true,
		"bProcessing": true,	
			sProcessing: "<img src='{$img_dir}ajax-loader.gif'>"
		},
		"sAjaxSource": '{$base}customers/allCustomers',
		"bDeferRender": true,
		"bProcessing": true,
		"aoColumns": [
		{
			"mDataProp": "first_name",
		},
		{
			"mDataProp": "last_name"

		}, 	
		{
			"mDataProp": "email"

		}, 
		{
			"mDataProp": "company_name"

		}, 
		{
			"mDataProp": "id",
			"mRender": function(data, type, row) {
				return '<a href="javascript:;" class="btn btn-sm btn-warning edit" title="Edit" id="'+row.id+'"><i class="fa fa-edit"></i></a> <a href="javascript:;" class="btn btn-sm btn-danger delete" title="Delete" id="'+row.id+'"><i class="icon fa fa-trash"></i></a>';
			}	
		}
		]});	

		$('body').on('click','.add',function(e){
			e.preventDefault();
			$('#message2').html('');
			$('#addCustomer').modal('show');
		});

		$('body').on('click','.edit',function(e){
			e.preventDefault();
			$('#message3').html('');
			$('#message').html('');
			$('#loader').show();
			var target = $(this);
			var id = target.attr('id');
			var data = convertToObject(id);
			$.getJSON("{$base}customers/getByID",data,function(result){
				$('#loader').hide();
				if(result.err == 1){
					$('#message').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#table-holder").scrollTop(0).animate(1000);
					});
				} else {	
					$('#id').val(result.id);
					$('#first_name2').val(result.first_name);
					$('#last_name2').val(result.last_name);
					$('#username2').val(result.username);
					$('#email2').val(result.email);
					$('#contact_no2').val(result.contact_no);
					$('#company_name2').val(result.company_name);
					$('#company_address2').val(result.company_address);
					$('.permission').each(function(i){
						if (result.permission.indexOf($(this).val()) != -1) {
							$(this).prop('checked', true);
						} else {
							$(this).prop('checked', false);
						}
					});
					$('#updateCustomer').modal('show');
				}
			});

		});

		$('#add-customer-form').submit(function(e){
			e.preventDefault();
	
			$('#loader2').show();
			$('#message2').html('');
			var self = this;
			$(self).find('button').prop('disabled', true);
			$.post("{$base}customers/create",$(self).serialize(),function(result){
				$('#loader2').hide();
				$(self).find('button').prop('disabled', false);
				if(result.err == 0){
					$('#message2').html(successMsg(result.msg)).fadeIn(1000,function(){
						$("#addCustomer").scrollTop(0).animate(1000);
						table.fnReloadAjax();
					});
				}
				else{
					$('#message2').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#addCustomer").scrollTop(0).animate(1000);
					});
				}
			},"json");
		});

		$('#update-customer-form').submit(function(e){
			e.preventDefault();

			$('#loader3').show();
			$('#message3').html('');
			var self = this;
			$(self).find('button').prop('disabled', true);
			$.post("{$base}customers/update",$(self).serialize(),function(result){
				$('#loader3').hide();
				$(self).find('button').prop('disabled', false);
				if(result.err == 0){
					$('#message3').html(successMsg(result.msg)).fadeIn(1000,function(){
						$("#updateCustomer").scrollTop(0).animate(1000);
						table.fnReloadAjax();
					});
				}
				else{
					$('#message3').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#updateCustomer").scrollTop(0).animate(1000);
					});
				}
			},"json");
		});

		$('body').on('click','.delete',function(e){
			e.preventDefault();
			$('#message2').html('');
			$('#message3').html('');
			$('#message4').html('');
			var id = $(this).attr('id');
			$('#yesBtn').attr('ino', id);
			$('#loader4').show();
			$('body').on("click", "#yesBtn", function(e){
				var self = this;
				var id = $(self).attr('ino');
				var data = convertToObject(id);
				$(self).closest('#deleteCustomer').find('button').prop('disabled', true);
				$.post("{$base}customers/delete",data,function(result){
					$('#loader4').hide();
					$(self).hide();
					$(self).next().hide();
					$(self).closest('#deleteCustomer').find('button').prop('disabled', false);
					if(result.err == 0){
						$('#message4').html(successMsg(result.msg)).fadeIn(1000,function(){
							$("#deleteCustomer").scrollTop(0).animate(1000);
							table.fnReloadAjax();
						});
					}
					else{
						$('#message4').html(errorMsg(result.msg)).fadeIn(1000,function(){
							$("#deleteCustomer").scrollTop(0).animate(1000);
						});
					}
				},"json");
			});	
			$('#deleteCustomer').find('button').show();
			$('#deleteCustomer').modal('show');
		});
		
		$('#deleteCustomer').on('hidden.bs.modal', function (e) {
			$('#message2').html('');
			$('#message3').html('');
			$('#message4').html('');
		});
	
});	

</script>