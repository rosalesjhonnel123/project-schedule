<section class="page-section woocommerce-account woocommerce-page woocommerce-edit-account">
	<div class="container woocommerce">			
		<div class="row">
			<div class="col-md-9">
				<div id="table-holder">
					<h1 class="text-left mb-5 font-weight-700">{$title}</h1>
					<div class="row">
						<div class="col-md-6 mb-5">
							<label class="control-label" for="status">Select Status</label>  
							<select id="select-status" class="form-control">
								<option value="active" {if $status == 'active'}selected="selected"{/if}>Active</option>
								<option value="inactive"  {if $status == 'inactive'}selected="selected"{/if}>In Active</option>
							</select>
						</div>
					</div>
					<button type="button" class="btn btn-info add create-btn mb-5">CREATE CUSTOM SIGN</button>
					<div id="loader" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
					<div id="message" hidden></div>
					<table id="example" class="table table-hover dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Project Name</th>
								<th>Custom Sign Name</th>
								<th>Plate Size</th>
								<th>Finish Material</th>
								<th>Writing Option</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
							<tbody>
								
							</tbody>	
						</thead>	
					</table>	
				</div>	
			</div>
			<div class="col-md-3">
				{include file='includes/nav.tpl'}
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="addCustomSign" tabindex="-1" role="dialog" aria-labelledby="addCustomSign" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="add-custom-sign-form" role="form" method="post" action="">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title">Create Custom Sign</h4>
				<hr/>
				<div id="loader2" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message2" hidden></div>
				<div class="form-group">
					<label class="control-label" for="custom_sign_name">Sign Image</label> 
					<div class="image-container" id="canvas" style="width:400px;height: 75px;">
						<img id="sign-image" src="{$img_dir}default.png" alt="sign image" class="d-block" width="400" height="75"/>
						<div id="line1"></div>
						<div id="line2-1"></div>
						<div id="line2-2"></div>
						<div id="line3-1"></div>
						<div id="line3-2"></div>
						<div id="line3-3"></div>
					</div>
                    <label class="btn btn-default mt-3">
                        <i class="fa fa-image"></i> Select sign picture<input type="file" style="display: none;" name="image" id="image" onchange="readURL(this);">
                    </label>
				</div>
			</div>
			<div class="modal-body">
				{if $role == 'administrator'}
					<div class="form-group">
						<label class="control-label" for="customer_id">Select Customer</label>  
						<select name="customer_id" id="customer_id" class="form-control">
							{foreach from = $customers item = customer}
								<option value="{$customer['id']}">{$customer['full_name']}</option>
							{/foreach}
						</select> 
					</div>
				{/if}
				<div class="form-group">
					<label class="control-label" for="project_id">Select Project</label>
					<select name="project_id" id="project_id" class="form-control">
						{foreach from = $projects item = project}
							<option value="{$project['id']}">{$project['project_name']}</option>
						{/foreach}
					</select> 
				</div>
				<div class="form-group">
					<label class="control-label" for="custom_sign_name"><input type="checkbox" id="is_custom_name" style="margin-right:8px;">Custom Sign Name</label>  
					<input id="custom_sign_name" name="custom_sign_name" type="text" placeholder="Enter Custom Sign Name here" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label" for="line1">Select Number of Line</label>  
					<select name="select_line" id="select_line" class="form-control">
						<option value="line1">1 Line</option>
						<option value="line2">2 Lines</option>
						<option value="line3">3 Lines</option>
					</select>
				</div>
				<div class="line-1">
					<div class="form-group">
						<label class="control-label" for="line1">Select Color for Line 1</label>  
						<select name="color-line1-1" id="color-line1-1" class="form-control">
							<option value="black">Black</option>
							<option value="red">Red</option>
							<option value="white">White</option>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label" for="line1">Line 1 Text</label>  
						<input id="custom-line1-1" name="line1" type="text" placeholder="Enter Line 1 Text here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_heignt">Letter Height for Line 1 (In MM)</label>  
						  <input id="custom-line1-letter_heignt" type="text" placeholder="Enter line 1 height in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_left">Letter left for Line 1 (In MM)</label>  
						  <input id="custom-line1-letter_left" type="text" placeholder="Enter line 1 left in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_top">Letter top for Line 1 (In MM)</label>  
						  <input id="custom-line1-letter_top" type="text" placeholder="Enter line 1 top in mm here" class="form-control">
					</div>
				</div>
				<div class="line-2" hidden>
					<div class="form-group">
						<label class="control-label" for="line1">Select Color for Line 1</label>  
						<select name="color-line2-1" id="color-line2-1" class="form-control">
							<option value="black">Black</option>
							<option value="red">Red</option>
							<option value="white">White</option>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label" for="line2-1">Line 1 Text</label>  
						<input id="custom-line2-1" name="line21" type="text" placeholder="Enter Line 1 Text here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_heignt">Letter Height for Line 1 (In MM)</label>  
						  <input id="custom-line2-1-letter_heignt" type="text" placeholder="Enter line 1 height in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_left">Letter left for Line 1 (In MM)</label>  
						  <input id="custom-line2-1-letter_left" type="text" placeholder="Enter line 1 left in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_top">Letter top for Line 1 (In MM)</label>  
						  <input id="custom-line2-1-letter_top" type="text" placeholder="Enter line 1 top in mm here" class="form-control">
					</div>
					<div class="form-group">
						<label class="control-label" for="line1">Select Color for Line 2</label>  
						<select name="color-line2-2" id="color-line2-2" class="form-control">
							<option value="black">Black</option>
							<option value="red">Red</option>
							<option value="white">White</option>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label" for="line2-2">Line 2 Text</label>  
						<input id="custom-line2-2" name="line22" type="text" placeholder="Enter Line 2 Text here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_heignt">Letter Height for Line 2 (In MM)</label>  
						  <input id="custom-line2-2-letter_heignt" type="text" placeholder="Enter line 2 height in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_left">Letter left for Line 2 (In MM)</label>  
						  <input id="custom-line2-2-letter_left" type="text" placeholder="Enter line 2 left in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_top">Letter top for Line 2 (In MM)</label>  
						  <input id="custom-line2-2-letter_top" type="text" placeholder="Enter line 2 top in mm here" class="form-control">
					</div>
				</div>
				<div class="line-3" hidden>
					<div class="form-group">
						<label class="control-label" for="line1">Select Color for Line 1</label>  
						<select name="color-line3-1" id="color-line3-1" class="form-control">
							<option value="black">Black</option>
							<option value="red">Red</option>
							<option value="white">White</option>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label" for="line3-1">Line 1 Text</label>  
						<input id="custom-line3-1" name="line31" type="text" placeholder="Enter Line 1 Text here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_heignt">Letter Height for Line 1 (In MM)</label>  
						  <input id="custom-line3-1-letter_heignt" type="text" placeholder="Enter line 1 height in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_left">Letter left for Line 1 (In MM)</label>  
						  <input id="custom-line3-1-letter_left" type="text" placeholder="Enter line 1 left in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_top">Letter top for Line 1 (In MM)</label>  
						  <input id="custom-line3-1-letter_top" type="text" placeholder="Enter line 1 top in mm here" class="form-control">
					</div>
					<div class="form-group">
						<label class="control-label" for="line1">Select Color for Line 2</label>  
						<select name="color-line3-2" id="color-line3-2" class="form-control">
							<option value="black">Black</option>
							<option value="red">Red</option>
							<option value="white">White</option>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label" for="line3-2">Line 2 Text</label>  
						<input id="custom-line3-2" name="line32" type="text" placeholder="Enter Line 2 Text here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_heignt">Letter Height for Line 2 (In MM)</label>  
						  <input id="custom-line3-2-letter_heignt" type="text" placeholder="Enter line 2 height in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_left">Letter left for Line 2 (In MM)</label>  
						  <input id="custom-line3-2-letter_left" type="text" placeholder="Enter line 2 left in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_top">Letter top for Line 2 (In MM)</label>  
						  <input id="custom-line3-2-letter_top" type="text" placeholder="Enter line 2 top in mm here" class="form-control">
					</div>
					<div class="form-group">
						<label class="control-label" for="line1">Select Color for Line 3</label>  
						<select name="color-line3-3" id="color-line3-3" class="form-control">
							<option value="black">Black</option>
							<option value="red">Red</option>
							<option value="white">White</option>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label" for="line3-3">Line 3 Text</label>  
						<input id="custom-line3-3" name="line33" type="text" placeholder="Enter Line 3 Text here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_heignt">Letter Height for Line 3 (In MM)</label>  
						  <input id="custom-line3-3-letter_heignt" type="text" placeholder="Enter line 3 height in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_left">Letter left for Line 3 (In MM)</label>  
						  <input id="custom-line3-3-letter_left" type="text" placeholder="Enter line 3 left in mm here" class="form-control">
					</div>
					<div class="form-group">
						 <label class="control-label" for="letter_top">Letter top for Line 3 (In MM)</label>  
						  <input id="custom-line3-3-letter_top" type="text" placeholder="Enter line 3 top in mm here" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label" for="plate_size_id">Select Plate Size</label>  
					<select name="plate_size_id" id="plate_size_id" class="form-control">
						{foreach from = $plate_sizes item = plate_size}
							<option value="{$plate_size->id}">{$plate_size->name}</option>
						{/foreach}
					</select> 
				</div>
				<div class="form-group">
					<label class="control-label" for="finish_material_name">Select Finish Material</label>  
					<select name="finish_material_id" id="finish_material_id" class="form-control">
						{foreach from = $finish_materials item = finish_material}
							<option value="{$finish_material->id}">{$finish_material->name}</option>
						{/foreach}
					</select> 
				</div>
				<div class="form-group">
					<label class="control-label" for="writing_option_name">Select Writing Option</label>  
					<select name="writing_option_id" class="form-control">
						{foreach from = $writing_options item = writing_option}
							<option value="{$writing_option->id}">{$writing_option->name}</option>
						{/foreach}
					</select> 
				</div>
				<div class="form-group">
					<label class="control-label" for="comments">Comments</label>  
					<textarea name="comments" id="comments1" class="form-control" rows="5"></textarea>
				</div>
			</div>		
			
			<div class="modal-footer">
				<button type="submit" id="submit" class="btn btn-success">SUBMIT
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<div class="modal fade" id="updateCustomSign" tabindex="-1" role="dialog" aria-labelledby="updateCustomSign" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="update-custom-sign-form" role="form" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title">Edit Custom Sign</h4>
			</div>
			<div class="modal-body">
				<div id="loader3" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message3" hidden></div>
				<input type="hidden" name="id" id="id"></input>
				<input type="hidden" name="currentimage" id="currentimage"></input>
				<div class="form-group">
					<label class="control-label" for="sign_image">Sign Image</label> 
					<img id="sign-image2" src="{$img_dir}default.png" alt="sign image" class="d-block" width="250" height="139"/>
                    <label class="btn btn-default mt-3">
                        <i class="fa fa-image"></i> Select sign picture<input type="file" style="display: none;" name="image" id="image2" onchange="readURL(this);">
                    </label>
				</div>
				{if $role == 'administrator'}
					<div class="form-group">
						<label class="control-label" for="customer_id">Select Customer</label>  
						<select name="customer_id" id="customer_id2" class="form-control">
							{foreach from = $customers item = customer}
								<option value="{$customer['id']}">{$customer['full_name']}</option>
							{/foreach}
						</select> 
					</div>
				{/if}
				<div class="form-group">
					<label class="control-label" for="project_id">Select Project</label>  
					<select name="project_id" id="project_id2" class="form-control">
						{foreach from = $projects item = project}
							<option value="{$project['id']}">{$project['project_name']}</option>
						{/foreach}
					</select> 
				</div>
				<div class="form-group">
					 <label class="control-label" for="custom_sign_name">Custom Sign Name</label>  
					  <input id="custom_sign_name2" name="custom_sign_name" type="text" placeholder="Enter Custom Sign Name here" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label" for="plate_size_id">Select Plate Size</label>  
					<select name="plate_size_id" id="plate_size_id2" class="form-control">
						{foreach from = $plate_sizes item = plate_size}
							<option value="{$plate_size->id}">{$plate_size->name}</option>
						{/foreach}
					</select> 
				</div>
				<div class="form-group">
					<label class="control-label" for="finish_material_name">Select Finish Material</label>  
					<select name="finish_material_id" id="finish_material_id2" class="form-control">
						{foreach from = $finish_materials item = finish_material}
							<option value="{$finish_material->id}">{$finish_material->name}</option>
						{/foreach}
					</select> 
				</div>
				<div class="form-group">
					<label class="control-label" for="writing_option_name">Select Writing Option</label>  
					<select name="writing_option_id" id="writing_option_id2" class="form-control">
						{foreach from = $writing_options item = writing_option}
							<option value="{$writing_option->id}">{$writing_option->name}</option>
						{/foreach}
					</select> 
				</div>
				<div class="form-group">
					<label class="control-label" for="status">Status</label>  
					<select id="status" name="status" class="form-control">
						<option value="0">In Active</option>
						<option value="1">Active</option>
					</select>
				</div>
				<div class="form-group">
					<label class="control-label" for="comments">Comments</label>  
					<textarea name="comments" id="comments2" class="form-control" rows="5"></textarea>
				</div>
			</div>		
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">UPDATE
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>
<div class="modal fade" id="updateStatus" tabindex="-1" role="dialog" aria-labelledby="updateStatus" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="update-status-form" role="form" method="post" action="">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title">Approved Custom Sign</h4>
			</div>
			<div class="modal-body">
				<div id="loader4" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message4" hidden></div>
				<input type="hidden" name="custom_sign_id" id="custom_sign_id"></input>
				<div class="form-group">
					<label class="control-label" for="sign_image">Sign Image</label> 
					<img id="sign-image3" src="{$img_dir}default.png" alt="sign image" class="d-block" width="250" height="139"/>
				</div>
				<div class="form-group">
					<p class="d-block">
						<input id="approved" name="is_approved" type="radio" value="1"> Approved
					</p>
					<p class="d-block">
						<input id="not_approved" name="is_approved" type="radio" value="0"> Not Approved
					</p>
				</div>
				<div class="form-group">
					<label class="control-label" for="comments">Comments</label>  
					<textarea name="comments" id="comments" class="form-control" rows="5"></textarea>
				</div>
			</div>		
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">SUBMIT
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<div class="modal fade" id="deleteCustomSign" tabindex="-1" role="dialog" aria-labelledby="deleteCustomSign" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel"><span class="fa fa-exclamation" style="color:#D91E18;"></span></h4>
			</div>
			<div class="modal-body">
				<div id="loader5" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message5" hidden></div>
				<div class="form-group">
					<p>Are you sure you want to delete this custom sign?</p>
				</div>
			</div>
			<div class="modal-footer mt-3">
				<button id="yesBtn" type="button" class="btn btn-danger" title="Yes">Yes
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" title="No">No
				</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js"></script>
<script src="{$template_base_url}/datatables/jquery.dataTables.min.js"></script>
<script src="{$template_base_url}/datatables/dataTables.bootstrap.min.js"></script> 
<script src="{$template_base_url}/datatables/dataTables.responsive.min.js"></script> 
<script src="{$template_base_url}/datatables/responsive.bootstrap.min.js"></script>  
<script src="{$template_base_url}/datatables/dataTables.searchHighlight.min.js"></script>    
<script src="{$template_base_url}/datatables/jquery.highlight.js"></script> 
<script src="{$template_base_url}/js/fnReloadAjax.js"></script> 
<script src="{$template_base_url}/js/main.js"></script> 
<script type="text/javascript">
function dataURItoBlob(dataURI) {
	// convert base64/URLEncoded data component to raw binary data held in a string
	var byteString;
	if (dataURI.split(',')[0].indexOf('base64') >= 0)
	  byteString = atob(dataURI.split(',')[1]);
	else
	  byteString = unescape(dataURI.split(',')[1]);

	// separate out the mime component
	var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
	console.log(mimeString);
	// write the bytes of the string to a typed array
	var ia = new Uint8Array(byteString.length);
	for (var i = 0; i < byteString.length; i++) {
	  ia[i] = byteString.charCodeAt(i);
	}

	return new Blob([ia], {
	  type: mimeString
	});
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#sign-image, #sign-image2')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
$(document).ready(function(){	
	var table = $('#example');

  		table.dataTable( {
  			searchHighlight: true,
  			"order": [1,'desc'],
  			oLanguage: {
  				"bDeferRender": true,
				"bProcessing": true,	
       			sProcessing: "<img src='{$img_dir}ajax-loader.gif'>"
    		},
			"sAjaxSource": '{$base}customsigns/allCustomSignsAjax?status={$status}',
			"bDeferRender": true,
			"bProcessing": true,
			"aoColumns": [
			{
				"mDataProp": "project_name"
			},
			{
				"mDataProp": "custom_sign_name"
			},
			{
				"mDataProp": "plate_size_name"
			},
			{
				"mDataProp": "finish_material_name"
			},
			{
				"mDataProp": "writing_option_name"
			},
			{
				"mDataProp": "id",
				"mRender": function(data, type, row) {
					if (row.status == 1) {
						return 'Active';
					} else {
						return 'In Active';
					}
				}	
			},	
			{
				"mDataProp": "id",
				"mRender": function(data, type, row) {
					return '<a href="javascript:;" class="btn btn-sm btn-default update-status" title="Update Status" id="'+row.id+'">'+(row.is_approved == 1 ? 'Approved' : 'Not Approved') +'</a> <a href="javascript:;" class="btn btn-sm btn-warning edit" title="Edit" id="'+row.id+'"><i class="fa fa-edit"></i></a> <a href="javascript:;" class="btn btn-sm btn-danger delete" title="Delete" id="'+row.id+'"><i class="icon fa fa-trash"></i></a>';
				}	
			}

			]});

		$('body').on('click','.add',function(e){
			e.preventDefault();
			
			$('#message2').html('');
			$('#addCustomSign').modal('show');
			$('#image').val('');
			$('#sign-image').attr('src', '{$img_dir}'+'default.png');
			$('#project_id').html('');
			let role = "{$role}";
			if (role == 'administrator') {
				$('#loader').show();
				let customer_id = $('#customer_id').find("option:selected").val();
				var data = convertToObject(customer_id);
				$.getJSON("{$base}projects/getProjectsByCustomerId",data,function(result){
					$('#loader').hide();
					$.each(result.data, function(key, value) {   
						$('#project_id')
					      .append($('<option>', { value : value.id })
					      .text(value.project_name)); 
					});
				});
			}
		});

		$('body').on('click','.edit',function(e){
			e.preventDefault();
			
			$('#loader').show();
			$('#message3').html('');
			$('#message').html('');
			var target = $(this);
			var id = target.attr('id');
			var data = convertToObject(id);
			$('#project_id2').html('');
			$.getJSON("{$base}customsigns/getByID",data,function(result){
				$('#loader').hide();
				if(result.err == 1){
					$('#message').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#table-holder").scrollTop(0).animate(1000);
					});
				} else {	
					let plate_size = result[0].plate_size_name.split(' X ');
					let width = plate_size[0].replace("MM", "");
					let height = plate_size[1].replace("MM", "");
				
					$('#id').val(result[0].id);
					$('#currentimage').val(result[0].image);
					$('#sign-image2').attr('width', width);
					$('#sign-image2').attr('height', height);
					$('#sign-image2').css('max-width', width + 'px');
					$('#sign-image2').css('width', '100%');
					$('#sign-image2').attr('src', '{$img_dir}'+result[0].image);
					$('#custom_sign_name2').val(result[0].custom_sign_name);
					$('#plate_size_id2').val(result[0].plate_size_id);
					$('#finish_material_id2').val(result[0].finish_material_id);
					$('#writing_option_id2').val(result[0].writing_option_id);
					$('#customer_id2').val(result[0].customer_id);
					$('#status').val(result[0].status);
					$('#comments2').val(result[0].comments);
					var data = convertToObject(result[0].customer_id);
					var project_id = result[0].project_id;
					let role = "{$role}";
					if (role == 'administrator') {
						$.getJSON("{$base}projects/getProjectsByCustomerId",data,function(result){
							$('#loader').hide();
							$.each(result.data, function(key, value) {   
								$('#project_id2')
							      .append($('<option>', { value : value.id })
							      .text(value.project_name)); 
							});
						});	
					}
					$('#project_id2').val(project_id);
					$('#updateCustomSign').modal('show');
				}
			});

		});

		$('#add-custom-sign-form').submit(function(e){
			e.preventDefault();

			$('#loader2').show();
			$('#message2').html('');
			var self = this;
			$(self).find('button').prop('disabled', true);
			var formdata = new FormData($(self)[0]);
			var blob = '';
			  html2canvas(document.querySelector("#canvas")).then(function (canvas) {              
                var dataURL = canvas.toDataURL();
        
                var blob = dataURItoBlob(dataURL);
                formdata.set('image', blob);
                $.ajax({
					type: "POST",
					url: "{$base}customsigns/create",
					data:formdata,
					dataType:'json',
					processData: false,
	        		contentType: false,
	        		success: function(result){
	        			$(self).find('button').prop('disabled', false);
	        			$('#loader2').hide();
	        			if(result.err == 0){
							$('#message2').html(successMsg(result.msg)).fadeIn(1000,function(){
								$("#addCustomSign").scrollTop(0).animate(1000);	
								table.fnReloadAjax();
								$('#add-custom-sign-form')[0].reset();  
								$('#line1').text('');
	                            $('#line2-1').text('');
	                            $('#line2-2').text('');
	                            $('#line3-1').text('');
	                            $('#line3-2').text('');
	                            $('#line3-3').text('');
							});
						}
						else{
							$('#message2').html(errorMsg(result.msg)).fadeIn(1000,function(){
								$("#addCustomSign").scrollTop(0).animate(1000);	
							});
						}
	        		}
				});
            });
           
			
		});

		$('#update-custom-sign-form').submit(function(e){
			e.preventDefault();

			$('#loader3').show();
			$('#message3').html('');
			var self = this;
			$(self).find('button').prop('disabled', true);
			var formdata = new FormData($(self)[0]);
			$.ajax({
				type: "POST",
				url: "{$base}customsigns/update",
				data:formdata,
				dataType:'json',
				processData: false,
        		contentType: false,
        		success: function(result){
        			$(self).find('button').prop('disabled', false);
        			$('#loader3').hide();
        			if(result.err == 0){
						$('#message3').html(successMsg(result.msg)).fadeIn(1000,function(){
							$("#updateCustomSign").scrollTop(0).animate(1000);	
							table.fnReloadAjax();
						});
					}
					else{
						$('#message3').html(errorMsg(result.msg)).fadeIn(1000,function(){
							$("#updateCustomSign").scrollTop(0).animate(1000);	
						});
					}
        		}
			});
		});
		
		$('#plate_size_id').on('change', function(e){
			$('#loader2').show();
			$('#message2').html('');
			var target = $(this);
			var plate_size_id = $(this).val();
			var finish_material_id = $('#finish_material_id').find("option:selected").val();
			var data = convertToObject2(plate_size_id, finish_material_id);
			$.getJSON("{$base}finishmaterialtemplates/getByPlateSizeIdFinishMaterialId", data,function(result){
				$('#loader2').hide();
				let plate_size = $('#plate_size_id').find("option:selected").text().split(' X ');
				let img = 'default.png';

				if (result[0] !== null) {
					plate_size = result[0].plate_size.split(' X ');
					img = result[0].image;
				} else {
					$('#message2').html('<p>Note: No template available. Kindly upload sign picture or use default default.</p>');
					$('#message2').show();
				}

				width = plate_size[0].replace("MM", "");
				height = plate_size[1].replace("MM", "");
			
				$('#canvas').css('max-width', width+'px');
				$('#canvas').css('height', height+'px');
				$('#canvas').css('width', '100%');
				$('#sign-image').attr('width', width);
				$('#sign-image').attr('height', height);
				$('#sign-image').css('width', '100%');
				$('#sign-image').css('max-width', width + 'px');
				$('#sign-image').attr('src', "{$img_dir}" + img);
			});
		});

		$('#finish_material_id').on('change', function(e){
			$('#loader2').show();
			$('#message2').html('');
			var target = $(this);
			var plate_size_id = $('#plate_size_id').find("option:selected").val();
			var finish_material_id = $(this).val();
			var data = convertToObject2(plate_size_id, finish_material_id);
			$.getJSON("{$base}finishmaterialtemplates/getByPlateSizeIdFinishMaterialId", data,function(result){
				$('#loader2').hide();
				let plate_size = $('#plate_size_id').find("option:selected").text().split(' X ');
				let img = 'default.png';

				if (result[0] !== null) {
					plate_size = result[0].plate_size.split(' X ');
					img = result[0].image;
				} else {
					$('#message2').html('<p>Note: No template available. Kindly upload sign picture or use default default.</p>');
					$('#message2').show();
				}

				width = plate_size[0].replace("MM", "");
				height = plate_size[1].replace("MM", "");
			
				$('#canvas').css('max-width', width+'px');
				$('#canvas').css('height', height+'px');
				$('#canvas').css('width', '100%');
				$('#sign-image').attr('width', width);
				$('#sign-image').attr('height', height);
				$('#sign-image').css('width', '100%');
				$('#sign-image').css('max-width', width + 'px');
				$('#sign-image').attr('src', "{$img_dir}" + img);
			});
		});

		$('body').on('click','.update-status',function(e){
			e.preventDefault();
			$('#message4').html('');
			$('#loader').show();
			$('#message').html('');
			var target = $(this);
			var id = target.attr('id');
			var data = convertToObject(id);
			$.getJSON("{$base}customsigns/getByID",data,function(result){
				$('#loader').hide();
				if(result.err == 1){
					$('#message').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#table-holder").scrollTop(0).animate(1000);
					});
				} else {
					let plate_size = result[0].plate_size_name.split(' X ');
					let width = plate_size[0].replace("MM", "");
					let height = plate_size[1].replace("MM", "");

					$('#custom_sign_id').val(result[0].id);
					$('#sign-image3').attr('width', width);
					$('#sign-image3').attr('height', height);
					$('#sign-image3').css('width', '100%');
					$('#sign-image3').css('max-width', width + 'px');
					$('#sign-image3').attr('src', "{$img_dir}" + result[0].image);
					$('#comments').val(result[0].comments);
					if (result[0].is_approved == 1) {
						$("#approved").attr('checked', true);
						$("#not_approved").attr('checked', false);
					} else {
						$("#approved").attr('checked', false);
						$("#not_approved").attr('checked', true);
					}
					$('#updateStatus').modal('show');
				}
			});

		});

		$('#update-status-form').submit(function(e){
			e.preventDefault();

			$('#loader4').show();
			$('#message4').html('');
			var self = this;
			$(self).find('button').prop('disabled', true);
			$.post("{$base}customsigns/update_status",$(self).serialize(),function(result){
				$('#loader4').hide();
				$(self).find('button').prop('disabled', false);
				if(result.err == 0){
					$('#message4').html(successMsg(result.msg)).fadeIn(1000,function(){
						$("#updateStatus").scrollTop(0).animate(1000);
						table.fnReloadAjax();
					});
				}
				else{
					$('#message4').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#updateStatus").scrollTop(0).animate(1000);
					});
				}
				
			},"json");
		});

		$('#customer_id').on('change', function(e){
			$('#loader2').show();
			let customer_id = $(this).val();
			var data = convertToObject(customer_id);
			$('#project_id').html('');
			$.getJSON("{$base}projects/getProjectsByCustomerId",data,function(result){
				$('#loader2').hide();
				$.each(result.data, function(key, value) {   
					$('#project_id')
				      .append($('<option>', { value : value.id })
				      .text(value.project_name)); 
				});
			});
		})

		$('#customer_id2').on('change', function(e){
			$('#loader3').show();
			$('#message3').html('');
			let customer_id = $(this).val();
			var data = convertToObject(customer_id);
			$('#project_id2').html('');
			$.getJSON("{$base}projects/getProjectsByCustomerId",data,function(result){
				$('#loader3').hide();
				$.each(result.data, function(key, value) {   
					$('#project_id2')
				      .append($('<option>', { value : value.id })
				      .text(value.project_name)); 
				});
			});
		})

		$('body').on('click','.delete',function(e){
			e.preventDefault();
			$('#message2').html('');
			$('#message3').html('');
			$('#message4').html('');
			$('#message5').html('');
			var id = $(this).attr('id');
			$('#yesBtn').attr('ino', id);
			$('#loader5').show();
			$('body').on("click", "#yesBtn", function(e){
				var self = this;
				var id = $(self).attr('ino');
				var data = convertToObject(id);
				$(self).closest('#deleteCustomSign').find('button').prop('disabled', true);
				$.post("{$base}customsigns/delete",data,function(result){
					$('#loader5').hide();
					$(self).hide();
					$(self).next().hide();
					$(self).closest('#deleteCustomSign').find('button').prop('disabled', false);
					if(result.err == 0){
						$('#message5').html(successMsg(result.msg)).fadeIn(1000,function(){
							$("#deleteCustomSign").scrollTop(0).animate(1000);
							table.fnReloadAjax();
						});
					}
					else{
						$('#message5').html(errorMsg(result.msg)).fadeIn(1000,function(){
							$("#deleteCustomSign").scrollTop(0).animate(1000);
						});
					}
				},"json");
			});	
			$('#deleteCustomSign').find('button').show();
			$('#deleteCustomSign').modal('show');
		});
		
		$('#deleteCustomSign').on('hidden.bs.modal', function (e) {
			$('#message2').html('');
			$('#message3').html('');
			$('#message4').html('');
			$('#message5').html('');
		});

		if ($('#select_line').val() == 'line1') {
			$('#line2-1').text('');
			$('#line2-2').text('');
			$('#line3-1').text('');
			$('#line3-2').text('');
			$('#line3-3').text('');
			$('.line-1').show();
			$('.line-2').hide();
			$('.line-3').hide();
		} else if ($(this).val() == 'line2') {
			$('#line1').text('');
			$('#line3-1').text('');
			$('#line3-2').text('');
			$('#line3-3').text('');
			$('.line-2').show();
			$('.line-1').hide();
			$('.line-3').hide();
		} else if ($(this).val() == 'line3') {
			$('#line1').text('');
			$('#line2-1').text('');
			$('#line2-2').text('');
			$('.line-3').show();
			$('.line-2').hide();
			$('.line-1').hide();
		}

		$('#select_line').on('change', function(){
			if ($(this).val() == 'line1') {
				$('#line2-1').text('');
				$('#line2-2').text('');
				$('#line3-1').text('');
				$('#line3-2').text('');
				$('#line3-3').text('');
				$('.line-1').show();
				$('.line-2').hide();
				$('.line-3').hide();
			} else if ($(this).val() == 'line2') {
				$('#line1').text('');
				$('#line3-1').text('');
				$('#line3-2').text('');
				$('#line3-3').text('');
				$('.line-2').show();
				$('.line-1').hide();
				$('.line-3').hide();
			} else if ($(this).val() == 'line3') {
				$('#line1').text('');
				$('#line2-1').text('');
				$('#line2-2').text('');
				$('.line-3').show();
				$('.line-2').hide();
				$('.line-1').hide();
			}
		});

		if ($('#custom-line1-1').parent().parent().parent().find('#is_custom_name').is(":checked")) {
			$('#line1').text($(this).val());
		}

		$('#custom-line1-1').on('keyup', function(){
			if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
				$('#line1').text($(this).val());
			}
		});
		if ($('#custom-line2-1').parent().parent().parent().find('#is_custom_name').is(":checked")) {
				$('#line2-1').text($(this).val());
			}
		$('#custom-line2-1').on('keyup', function(){
			if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
				$('#line2-1').text($(this).val());
			}
		});

		if ($('#custom-line2-2').parent().parent().parent().find('#is_custom_name').is(":checked")) {
				$('#line2-2').text($(this).val());
			}

		$('#custom-line2-2').on('keyup', function(){
			if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
				$('#line2-2').text($(this).val());
			}
		});

		if ($('#custom-line3-1').parent().parent().parent().find('#is_custom_name').is(":checked")) {
			$('#line3-1').text($(this).val());
		}

		$('#custom-line3-1').on('keyup', function(){
			if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
				$('#line3-1').text($(this).val());
			}
		});

		if ($('#custom-line3-2').parent().parent().parent().find('#is_custom_name').is(":checked")) {
			$('#line3-2').text($(this).val());
		}
		$('#custom-line3-2').on('keyup', function(){
			if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
				$('#line3-2').text($(this).val());
			}
		});

		if ($('#custom-line3-3').parent().parent().parent().find('#is_custom_name').is(":checked")) {
			$('#line3-3').text($(this).val());
		}

		$('#custom-line3-3').on('keyup', function(){
			if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
				$('#line3-3').text($(this).val());
			}
		});

		$('#is_custom_name').on('click', function(){
			if ($(this).is(":checked")) {
				if ($('#select_line').find("option:selected").val() == 'line1') {
					$('#line2-1').text('');
					$('#line2-2').text('');
					$('#line3-1').text('');
					$('#line3-2').text('');
					$('#line3-3').text('');
					$('.line-1').show();
					$('.line-2').hide();
					$('.line-3').hide();
					$('#line1').text($('#custom-line1-1').val());
				} else if ($('#select_line').find("option:selected").val() == 'line2') {
					$('#line1').text('');
					$('#line3-1').text('');
					$('#line3-2').text('');
					$('#line3-3').text('');
					$('.line-2').show();
					$('.line-1').hide();
					$('.line-3').hide();
					$('#line2-1').text($('#custom-line2-1').val());
					$('#line2-2').text($('#custom-line2-2').val());
				} else if ($('#select_line').find("option:selected").val() == 'line3') {
					$('#line1').text('');
					$('#line2-1').text('');
					$('#line2-2').text('');
					$('.line-3').show();
					$('.line-2').hide();
					$('.line-1').hide();
					$('#line3-1').text($('#custom-line3-1').val());
					$('#line3-2').text($('#custom-line3-2').val());
					$('#line3-3').text($('#custom-line3-3').val());
				}
			} else {
				$('#line1').text('');
				$('#line2-1').text('');
				$('#line2-2').text('');
				$('#line3-1').text('');
				$('#line3-2').text('');
				$('#line3-3').text('');
			}
		});

		let px = '3.78';
		$('#custom-line1-letter_heignt').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line1').css('font-size', mm.toFixed(2) +'px');
		});

		$('#custom-line2-1-letter_heignt').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line2-1').css('font-size', mm.toFixed(2) +'px');
		});

		$('#custom-line2-2-letter_heignt').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line2-2').css('font-size', mm.toFixed(2) +'px');
		});

		$('#custom-line3-1-letter_heignt').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line3-1').css('font-size', mm.toFixed(2) +'px');
		});

		$('#custom-line3-2-letter_heignt').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line3-2').css('font-size', mm.toFixed(2) +'px');
		});

		$('#custom-line3-3-letter_heignt').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line3-3').css('font-size', mm.toFixed(2) +'px');
		});


		$('#custom-line1-letter_left').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line1').css('left', mm.toFixed(2) +'px');
		});

		$('#custom-line2-1-letter_left').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line2-1').css('left', mm.toFixed(2) +'px');
		});

		$('#custom-line2-2-letter_left').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line2-2').css('left', mm.toFixed(2) +'px');
		});

		$('#custom-line3-1-letter_left').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line3-1').css('left', mm.toFixed(2) +'px');
		});

		$('#custom-line3-2-letter_left').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line3-2').css('left', mm.toFixed(2) +'px');
		});

		$('#custom-line3-3-letter_left').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line3-3').css('left', mm.toFixed(2) +'px');
		});

		$('#custom-line1-letter_top').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line1').css('top', mm.toFixed(2) +'px');
		});

		$('#custom-line2-1-letter_top').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line2-1').css('top', mm.toFixed(2) +'px');
		});

		$('#custom-line2-2-letter_top').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line2-2').css('top', mm.toFixed(2) +'px');
		});

		$('#custom-line3-1-letter_top').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line3-1').css('top', mm.toFixed(2) +'px');
		});

		$('#custom-line3-2-letter_top').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line3-2').css('top', mm.toFixed(2) +'px');
		});

		$('#custom-line3-3-letter_top').on('keyup', function(){
			let val = $(this).val();
			let mm = val * parseFloat(px);

			$('#line3-3').css('top', mm.toFixed(2) +'px');
		});


		$('#select-status').on('change', function(){
			location.href = "{$base}customsigns?status=" + $(this).val();
		});

		$('#line1').css('color', $('#color-line1-1').val());
		$('#color-line1-1').on('change', function(){
			let color = $(this).val();
			$('#line1').css('color', color);
		});

		$('#line2-1').css('color', $('#color-line2-1').val());
		$('#color-line2-1').on('change', function(){
			let color = $(this).val();
			$('#line2-1').css('color', color);
		});
		$('#line2-2').css('color', $('#color-line2-2').val());
		$('#color-line2-2').on('change', function(){
			let color = $(this).val();
			$('#line2-2').css('color', color);
		});
		$('#line3-1').css('color', $('#color-line3-1').val());
		$('#color-line3-1').on('change', function(){
			let color = $(this).val();
			$('#line3-1').css('color', color);
		});
		$('#line3-2').css('color', $('#color-line3-2').val());
		$('#color-line3-2').on('change', function(){
			let color = $(this).val();
			$('#line3-2').css('color', color);
		});
		$('#line3-3').css('color', $('#color-line3-3').val());
		$('#color-line3-3').on('change', function(){
			let color = $(this).val();
			$('#line3-3').css('color', color);
		});
});	

</script>