<section class="page-section woocommerce-account woocommerce-page woocommerce-edit-account">
	<div class="container woocommerce">			
		<div class="row">
			<div class="col-md-9">
				<div id="table-holder">
					<h1 class="text-left mb-5 font-weight-700">{$title}</h1>
					<div class="row">
						<div class="col-md-6 mb-5">
							<label class="control-label" for="status">Select Status</label>  
							<select id="select-status" class="form-control">
								<option value="active" {if $status == 'active'}selected="selected"{/if}>Active</option>
								<option value="inactive"  {if $status == 'inactive'}selected="selected"{/if}>In Active</option>
							</select>
						</div>
					</div>
					<button type="button" class="btn btn-info add create-btn mb-5">CREATE FINISH MATERIAL TEMPLATE</button>
					<div id="loader" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
					<div id="message" hidden></div>
					<table id="example" class="table table-hover dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Finish Material</th>
								<th>Plate Size</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
							<tbody>
								
							</tbody>	
						</thead>	
					</table>	
				</div>	
			</div>
			<div class="col-md-3">
				{include file='includes/nav.tpl'}
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="addFinishMaterialTemplate" tabindex="-1" role="dialog" aria-labelledby="addFinishMaterialTemplate" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="add-finish-material-template-form" role="form" method="post" action="">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel">Create Finish Material Template</h4>
			</div>
			<div class="modal-body">
				<div id="loader2" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message2" hidden></div>
				<div class="form-group">
					<label class="control-label" for="template_background">Template Background</label> 
					<img id="template-background" src="{$img_dir}default.png" alt="template background" class="d-block" width="400" height="75"/>
                    <label class="btn btn-default mt-3">
                        <i class="fa fa-image"></i> Select template background<input type="file" style="display: none;" name="image" id="image" onchange="readURL(this);">
                    </label>
				</div>
				<div class="form-group">
					<label class="control-label" for="plate_size_id">Select Plate Size</label>  
					<select name="plate_size_id" id="plate_size_id" class="form-control">
						{foreach from = $plate_sizes item = plate_size}
							<option value="{$plate_size->id}">{$plate_size->name}</option>
						{/foreach}
					</select> 
				</div>
				<div class="form-group">
					<label class="control-label" for="finish_material_name">Select Finish Material</label>  
					<select name="finish_material_id" class="form-control">
						{foreach from = $finish_materials item = finish_material}
							<option value="{$finish_material->id}">{$finish_material->name}</option>
						{/foreach}
					</select> 
				</div>
			</div>		
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">SUBMIT
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<div class="modal fade" id="updateFinishMaterialTemplate" tabindex="-1" role="dialog" aria-labelledby="updateFinishMaterialTemplate" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="update-finish-material-template-form" role="form" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel">Edit Finish Material Template</h4>
			</div>
			<div class="modal-body">
				<div id="loader3" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message3" hidden></div>
				<input type="hidden" name="id" id="id"></input>
				<input type="hidden" name="currentimage" id="currentimage"></input>
				<div class="form-group">
					<label class="control-label" for="template_background">Template Background</label> 
					<img id="template-background2" src="{$img_dir}default.png" alt="template background" class="d-block" width="400" height="75"/>
                    <label class="btn btn-default mt-3">
                        <i class="fa fa-image"></i> Select template background<input type="file" style="display: none;" name="image" id="image2" onchange="readURL(this);">
                    </label>
				</div>
				<div class="form-group">
					<label class="control-label" for="plate_size_id">Select Plate Size</label>  
					<select name="plate_size_id" id="plate_size_id2" class="form-control">
						{foreach from = $plate_sizes item = plate_size}
							<option value="{$plate_size->id}">{$plate_size->name}</option>
						{/foreach}
					</select> 
				</div>
				<div class="form-group">
					<label class="control-label" for="finish_material_name">Select Finish Material</label>  
					<select name="finish_material_id" id="finish_material_id2" class="form-control">
						{foreach from = $finish_materials item = finish_material}
							<option value="{$finish_material->id}">{$finish_material->name}</option>
						{/foreach}
					</select> 
				</div>
				<div class="form-group">
					<label class="control-label" for="status">Status</label>  
					<select id="status" name="status" class="form-control">
						<option value="0">In Active</option>
						<option value="1">Active</option>
					</select>
				</div>
			</div>		
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">UPDATE
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<div class="modal fade" id="deleteFinishMaterialTemplate" tabindex="-1" role="dialog" aria-labelledby="deleteFinishMaterialTemplate" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel"><span class="fa fa-exclamation" style="color:#D91E18;"></span></h4>
			</div>
			<div class="modal-body">
				<div id="loader4" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message4" hidden></div>
				<div class="form-group">
					<p>Are you sure you want to delete this finish material template?</p>
				</div>
			</div>
			<div class="modal-footer mt-3">
				<button id="yesBtn" type="button" class="btn btn-danger" title="Yes">Yes
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" title="No">No
				</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="{$template_base_url}/datatables/jquery.dataTables.min.js"></script>
<script src="{$template_base_url}/datatables/dataTables.bootstrap.min.js"></script> 
<script src="{$template_base_url}/datatables/dataTables.responsive.min.js"></script> 
<script src="{$template_base_url}/datatables/responsive.bootstrap.min.js"></script>  
<script src="{$template_base_url}/datatables/dataTables.searchHighlight.min.js"></script>    
<script src="{$template_base_url}/datatables/jquery.highlight.js"></script> 
<script src="{$template_base_url}/js/fnReloadAjax.js"></script> 
<script src="{$template_base_url}/js/main.js"></script> 
<script type="text/javascript">

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#template-background, #template-background2')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).ready(function(){	
	var table = $('#example');

  		table.dataTable( {
  			searchHighlight: true,
  			"order": [1,'desc'],
  			oLanguage: {
  				"bDeferRender": true,
				"bProcessing": true,	
       			sProcessing: "<img src='{$img_dir}ajax-loader.gif'>"
    		},
			"sAjaxSource": '{$base}finishmaterialtemplates/allFinishMaterialTemplates?status={$status}',
			"bDeferRender": true,
			"bProcessing": true,
			"aoColumns": [
			{
				"mDataProp": "finish_material"
			},
			{
				"mDataProp": "plate_size"
			},
			{
				"mDataProp": "id",
				"mRender": function(data, type, row) {
					if (row.status == 1) {
						return 'Active';
					} else {
						return 'In Active';
					}
				}	
			},		
			{
				"mDataProp": "id",
				"mRender": function(data, type, row) {
					return '<a href="javascript:;" class="btn btn-sm btn-warning edit" title="Edit" id="'+row.id+'"><i class="fa fa-edit"></i></a> <a href="javascript:;" class="btn btn-sm btn-danger delete" title="Delete" id="'+row.id+'"><i class="icon fa fa-trash"></i></a>';
				}	
			}

			]});	

		$('body').on('click','.add',function(e){
			e.preventDefault();
			
			$('#message2').html('');
			$('#addFinishMaterialTemplate').modal('show');
			$('#image').val('');
			$('#template-background').attr('src', '{$img_dir}'+'default.png');
		});

		$('body').on('click','.edit',function(e){
			e.preventDefault();
			
			$('#loader').show();
			$('#message3').html('');
			$('#message').html('');
			var target = $(this);
			var id = target.attr('id');
			var data = convertToObject(id);
			$.getJSON("{$base}finishmaterialtemplates/getByID",data,function(result){
				$('#loader').hide();
				if(result.err == 1){
					$('#message').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#table-holder").scrollTop(0).animate(1000);
					});
				} else {
					$('#id').val(result[0].id);
					$('#currentimage').val(result[0].image);
					$('#template-background2').attr('src', '{$img_dir}'+result[0].image);
					$('#plate_size_id2').val(result[0].plate_size_id);
					$('#finish_material_id2').val(result[0].finish_material_id);
					$('#status').val(result[0].status);
					$('#updateFinishMaterialTemplate').modal('show');
				}
			});

		});

		$('#add-finish-material-template-form').submit(function(e){
			e.preventDefault();

			$('#loader2').show();
			$('#message2').html('');
			var self = this;
			$(self).find('button').prop('disabled', true);
			var formdata = new FormData($(self)[0]);
			$.ajax({
				type: "POST",
				url: "{$base}finishmaterialtemplates/create",
				data:formdata,
				dataType:'json',
				processData: false,
        		contentType: false,
        		success: function(result){
        			$(self).find('button').prop('disabled', false);
        			$('#loader2').hide();
        			if(result.err == 0){
						$('#message2').html(successMsg(result.msg)).fadeIn(1000,function(){
							$("#addFinishMaterialTemplate").scrollTop(0).animate(1000);	
							table.fnReloadAjax();
						});
					}
					else{
						$('#message2').html(errorMsg(result.msg)).fadeIn(1000,function(){
							$("#addFinishMaterialTemplate").scrollTop(0).animate(1000);	
						});
					}
        		}
			});
		});

		$('#update-finish-material-template-form').submit(function(e){
			e.preventDefault();

			$('#loader3').show();
			$('#message3').html('');
			var self = this;
			$(self).find('button').prop('disabled', true);
			var formdata = new FormData($(self)[0]);
			$.ajax({
				type: "POST",
				url: "{$base}finishmaterialtemplates/update",
				data:formdata,
				dataType:'json',
				processData: false,
        		contentType: false,
        		success: function(result){
        			$(self).find('button').prop('disabled', false);
        			$('#loader3').hide();
        			if(result.err == 0){
						$('#message3').html(successMsg(result.msg)).fadeIn(1000,function(){
							$("#updateFinishMaterialTemplate").scrollTop(0).animate(1000);	
							table.fnReloadAjax();
						});
					}
					else{
						$('#message3').html(errorMsg(result.msg)).fadeIn(1000,function(){
							$("#updateFinishMaterialTemplate").scrollTop(0).animate(1000);	
						});
					}
        		}
			});
		});

		$('#plate_size_id').on('change', function(e){
			let plate_size = $(this).find("option:selected").text().split(' X ');
			let width = plate_size[0].replace("MM", "");
			let height = plate_size[1].replace("MM", "");
			$('#template-background').attr('width', width);
			$('#template-background').attr('height', height);
			$('#template-background').css('width', '100%');
			$('#template-background').css('max-width', width + 'px');
			$('#template-background').attr('src', '{$img_dir}'+'default.png');
		});

		$('#plate_size_id2').on('change', function(e){
			let plate_size = $(this).find("option:selected").text().split(' X ');
			let width = plate_size[0].replace("MM", "");
			let height = plate_size[1].replace("MM", "");
			$('#template-background2').attr('width', width);
			$('#template-background2').attr('height', height);
			$('#template-background2').css('width', '100%');
			$('#template-background2').css('max-width', width + 'px');
			$('#template-background2').attr('src', '{$img_dir}'+'default.png');
		});

		$('body').on('click','.delete',function(e){
			e.preventDefault();
			$('#message2').html('');
			$('#message3').html('');
			$('#message4').html('');
			var id = $(this).attr('id');
			$('#yesBtn').attr('ino', id);
			$('#loader4').show();
			$('body').on("click", "#yesBtn", function(e){
				var self = this;
				var id = $(self).attr('ino');
				var data = convertToObject(id);
				$(self).closest('#deleteFinishMaterialTemplate').find('button').prop('disabled', true);
				$.post("{$base}finishmaterialtemplates/delete",data,function(result){
					$('#loader4').hide();
					$(self).hide();
					$(self).next().hide();
					$(self).closest('#deleteFinishMaterialTemplate').find('button').prop('disabled', false);
					if(result.err == 0){
						$('#message4').html(successMsg(result.msg)).fadeIn(1000,function(){
							$("#deleteFinishMaterialTemplate").scrollTop(0).animate(1000);
							table.fnReloadAjax();
						});
					}
					else{
						$('#message4').html(errorMsg(result.msg)).fadeIn(1000,function(){
							$("#deleteFinishMaterialTemplate").scrollTop(0).animate(1000);
						});
					}
				},"json");
			});	
			$('#deleteFinishMaterialTemplate').find('button').show();
			$('#deleteFinishMaterialTemplate').modal('show');
		});
		
		$('#deleteFinishMaterialTemplate').on('hidden.bs.modal', function (e) {
			$('#message2').html('');
			$('#message3').html('');
			$('#message4').html('');
		});

		$('#select-status').on('change', function(){
			location.href = "{$base}finishmaterialtemplates?status=" + $(this).val();
		});
	
});	

</script>