<section class="page-section woocommerce-account woocommerce-page woocommerce-edit-account">
    <div class="container woocommerce" id="edit-project-form">      
        <div class="col-md-9">
            <h1 class="text-left mb-5 font-weight-700">{$title}</h1>
            <form method="POST" role="form" action="">
                <div id="loader" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
                <div id="message" hidden></div>
                <input type="hidden" name="project_id" value="{$project->id}">
                <div class="row">
                {if $role == 'administrator'}    
                    <div class="form-group col-md-6">
                        <label class="control-label" for="customer_id">Select Customer</label>  
                        <select name="customer_id" id="customer_id" class="form-control">
                            {foreach from = $customers item = customer}
                                {if $project->customer_id == $customer['id']} 
                                    <option value="{$customer['id']}" selected="selected">{$customer['full_name']}</option>
                                {else}
                                    <option value="{$customer['id']}">{$customer['full_name']}</option> 
                                {/if}
                            {/foreach}
                        </select> 
                    </div>
                {/if}
                    <div class="form-group col-md-6">
                        <label class="control-label" for="status">Status</label>  
                        <select id="status" name="status" class="form-control">
                            <option value="0" {if $project->status == 0}selected="selected"{/if}>In Active</option>
                            <option value="1" {if $project->status == 1}selected="selected"{/if}>Active</option>
                        </select>
                    </div>
                </div> 
                <div class="row">
                    <div class="form-group col-md-6">
                         <label class="control-label" for="project_name">Project Name</label>  
                          <input id="project_name" name="project_name" type="text" placeholder="Enter project name here" class="form-control" value="{$project->project_name}"/>
                    </div>
                    <div class="form-group col-md-6">
                         <label class="control-label" for="project_address">Project Address</label>  
                          <input id="project_address" name="project_address" type="text" placeholder="Enter project address here" class="form-control" value="{$project->project_address}"/>
                    </div>
                </div> 
                <div class="row" id="levels">
                {foreach from = $project_levels item = project_level key=key}
                    <div class="form-group col-md-12 level" id="level{if $key eq 0}{else}-{$key+1}{/if}">
                        <label class="control-label d-block" for="level">Level</label> 
                        <select name="level[]" id="level" class="form-control l-form-control">
                        {foreach from = $levels item = level}
                            <option value="{$level->id}" {if $project_level->level_id eq $level->id}selected="selected"{/if}>{$level->name}</option>
                        {/foreach}  
                        </select>
                        <button type="button" class="btn btn-info add_sign">ADD SIGN</button>
                        <button type="button" class="btn btn-info clone">CLONE</button>
                        <button type="button" class="btn btn-info toggle-btn"  data-toggle="collapse">HIDE DETAILS</button>
                        {if $key eq 0}
                        <button type="button" class="btn btn-info remove_btn" style="display:none;">REMOVE</button>
                        {else}
                            <button type="button" class="btn btn-info remove_btn">REMOVE</button>
                        {/if}
                        <div class="data-holder collapse in">
                            <div class="level-signs">
                                {foreach from = $level_signs item = level_sign}
                                {if $level_sign['level_id'] eq $project_level->level_id}
                                <div ino="{$level_sign['sign_id']}" custom-sign="{$level_sign['is_custom']}" class="col-md-12 sign-holder" style="margin:15px 0px;border-top: solid 1px #cccc;"><div class="row" style="padding-top: 15px;"><div style="width: 30%;display: inline-block; text-align:center;border: solid 1px #ccc;vertical-align: top;"><img src="{$level_sign['image']}" style="width:100%; max-width:250px;height: auto;display:inline-block;"></div><div style="width:50%;display:inline-block;vertical-align: top;padding-left: 15px;">{$level_sign['name']}<span class="d-block"><p style="color: #a2a4a6; border-bottom: 1px solid #333; width: auto; padding: 0px 0 0px 0; margin-bottom: 0px;">{$level_sign['post_excerpt']}</p><p style="padding-top:8px;"><input type="text" class="location" placeholder="Enter Location here" value="{$level_sign['location']}"></p></span></div><div style="width:20%;display:inline-block;"><p style="margin-left: 10px; margin-bottom: 4px;">Quantity</p><p><span class="deduct">-</span><input type="text" value="{$level_sign['quantity']}" class="qty" style="width: 50px; margin: 0px 10px;text-align: center;"><span class="add-up">+<span></p><p class="remove_sign text-center" style="margin-top: 15px;border: solid 1px #eee;padding:4px;width:85px;">Remove</p></div></div></div>
                                {/if}
                                {/foreach}  
                            </div>
                            <div class="loader" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
                            <div class="signs row">
                            </div>
                            <div class="category-search" style="display:none;">
                                <input type="text" id="category-search-input" placeholder="Search..." autofocus required>
                                <button class="fa fa-search" id="category-search-btn" type="submit"></button>
                            </div>
                            <div class="product-search" style="display:none;">
                                <input type="text" id="product-search-input" placeholder="Search..." autofocus required>
                                <button class="fa fa-search" id="product-search-btn" category="" type="submit"></button>
                            </div>
                        </div>
                    </div> 
                {/foreach}
                </div>  
                <div class="row  mt-5">
                    <div class="form-group col-md-6">
                         <button type="button" class="btn btn-info btn-block" id="add-level">ADD ANOTHER LEVEL</button>
                    </div>
                    <div class="form-group col-md-6">
                        <button type="click" class="btn btn-info btn-block" id="submit">SAVE</button>
                    </div>
                </div> 
            </form>
        </div>
        <div class="col-md-3">
            {include file='includes/nav.tpl'}
        </div>
    </div>
</section>
<div class="modal fade" id="addCustomSign" tabindex="-1" role="dialog" aria-labelledby="addCustomSign" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <form id="add-custom-sign-form" role="form" method="post" action="">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
        <h4 class="modal-title">Create Custom Sign</h4>
        <hr/>
        <div id="loader2" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
        <div id="message2" hidden></div>
        <div class="form-group">
            <input type="hidden" name="custom_sign_customer_id" value="{$project->customer_id}">
            <input type="hidden" name="project_id" value="{$project->id}">
            <label class="control-label" for="custom_sign_name">Sign Image</label> 
            <div class="image-container" id="canvas" style="width:400px;height: 75px;">
                <img id="sign-image" src="{$img_dir}default.png" alt="sign image" class="d-block" width="400" height="75"/>
                <div id="line1"></div>
                <div id="line2-1"></div>
                <div id="line2-2"></div>
                <div id="line3-1"></div>
                <div id="line3-2"></div>
                <div id="line3-3"></div>
                </div>
                <label class="btn btn-default mt-3">
                    <i class="fa fa-image"></i> Select sign picture<input type="file" style="display: none;" name="image" id="image" onchange="readURL(this);">
                </label>
            </div>
      </div>
      <div class="modal-body">
        
         <div class="form-group">
                    <label class="control-label" for="custom_sign_name"><input type="checkbox" id="is_custom_name" style="margin-right:8px;">Custom Sign Name</label>  
                    <input id="custom_sign_name" name="custom_sign_name" type="text" placeholder="Enter Custom Sign Name here" class="form-control">
                </div>
                <div class="form-group">
                    <label class="control-label" for="line1">Select Number of Line</label>  
                    <select name="select_line" id="select_line" class="form-control">
                        <option value="line1">1 Line</option>
                        <option value="line2">2 Lines</option>
                        <option value="line3">3 Lines</option>
                    </select>
                </div>
                <div class="line-1">
                    <div class="form-group">
                        <label class="control-label" for="line1">Select Color for Line 1</label>  
                        <select name="color-line1-1" id="color-line1-1" class="form-control">
                            <option value="black">Black</option>
                            <option value="red">Red</option>
                            <option value="white">White</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="line1">Line 1 Text</label>  
                        <input id="custom-line1-1" name="line1" type="text" placeholder="Enter Line 1 Text here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_heignt">Letter Height for Line 1 (In MM)</label>  
                          <input id="custom-line1-letter_heignt" type="text" placeholder="Enter line 1 height in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_left">Letter left for Line 1 (In MM)</label>  
                          <input id="custom-line1-letter_left" type="text" placeholder="Enter line 1 left in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_top">Letter top for Line 1 (In MM)</label>  
                          <input id="custom-line1-letter_top" type="text" placeholder="Enter line 1 top in mm here" class="form-control">
                    </div>
                </div>
                <div class="line-2" hidden>
                    <div class="form-group">
                        <label class="control-label" for="line1">Select Color for Line 1</label>  
                        <select name="color-line2-1" id="color-line2-1" class="form-control">
                            <option value="black">Black</option>
                            <option value="red">Red</option>
                            <option value="white">White</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="line2-1">Line 1 Text</label>  
                        <input id="custom-line2-1" name="line21" type="text" placeholder="Enter Line 1 Text here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_heignt">Letter Height for Line 1 (In MM)</label>  
                          <input id="custom-line2-1-letter_heignt" type="text" placeholder="Enter line 1 height in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_left">Letter left for Line 1 (In MM)</label>  
                          <input id="custom-line2-1-letter_left" type="text" placeholder="Enter line 1 left in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_top">Letter top for Line 1 (In MM)</label>  
                          <input id="custom-line2-1-letter_top" type="text" placeholder="Enter line 1 top in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="line1">Select Color for Line 2</label>  
                        <select name="color-line2-2" id="color-line2-2" class="form-control">
                            <option value="black">Black</option>
                            <option value="red">Red</option>
                            <option value="white">White</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="line2-2">Line 2 Text</label>  
                        <input id="custom-line2-2" name="line22" type="text" placeholder="Enter Line 2 Text here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_heignt">Letter Height for Line 2 (In MM)</label>  
                          <input id="custom-line2-2-letter_heignt" type="text" placeholder="Enter line 2 height in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_left">Letter left for Line 2 (In MM)</label>  
                          <input id="custom-line2-2-letter_left" type="text" placeholder="Enter line 2 left in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_top">Letter top for Line 2 (In MM)</label>  
                          <input id="custom-line2-2-letter_top" type="text" placeholder="Enter line 2 top in mm here" class="form-control">
                    </div>
                </div>
                <div class="line-3" hidden>
                    <div class="form-group">
                        <label class="control-label" for="line1">Select Color for Line 1</label>  
                        <select name="color-line3-1" id="color-line3-1" class="form-control">
                            <option value="black">Black</option>
                            <option value="red">Red</option>
                            <option value="white">White</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="line3-1">Line 1 Text</label>  
                        <input id="custom-line3-1" name="line31" type="text" placeholder="Enter Line 1 Text here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_heignt">Letter Height for Line 1 (In MM)</label>  
                          <input id="custom-line3-1-letter_heignt" type="text" placeholder="Enter line 1 height in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_left">Letter left for Line 1 (In MM)</label>  
                          <input id="custom-line3-1-letter_left" type="text" placeholder="Enter line 1 left in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_top">Letter top for Line 1 (In MM)</label>  
                          <input id="custom-line3-1-letter_top" type="text" placeholder="Enter line 1 top in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="line1">Select Color for Line 2</label>  
                        <select name="color-line3-2" id="color-line3-2" class="form-control">
                            <option value="black">Black</option>
                            <option value="red">Red</option>
                            <option value="white">White</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="line3-2">Line 2 Text</label>  
                        <input id="custom-line3-2" name="line32" type="text" placeholder="Enter Line 2 Text here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_heignt">Letter Height for Line 2 (In MM)</label>  
                          <input id="custom-line3-2-letter_heignt" type="text" placeholder="Enter line 2 height in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_left">Letter left for Line 2 (In MM)</label>  
                          <input id="custom-line3-2-letter_left" type="text" placeholder="Enter line 2 left in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_top">Letter top for Line 2 (In MM)</label>  
                          <input id="custom-line3-2-letter_top" type="text" placeholder="Enter line 2 top in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="line1">Select Color for Line 3</label>  
                        <select name="color-line3-3" id="color-line3-3" class="form-control">
                            <option value="black">Black</option>
                            <option value="red">Red</option>
                            <option value="white">White</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="line3-3">Line 3 Text</label>  
                        <input id="custom-line3-3" name="line33" type="text" placeholder="Enter Line 3 Text here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_heignt">Letter Height for Line 3 (In MM)</label>  
                          <input id="custom-line3-3-letter_heignt" type="text" placeholder="Enter line 3 height in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_left">Letter left for Line 3 (In MM)</label>  
                          <input id="custom-line3-3-letter_left" type="text" placeholder="Enter line 3 left in mm here" class="form-control">
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="letter_top">Letter top for Line 3 (In MM)</label>  
                          <input id="custom-line3-3-letter_top" type="text" placeholder="Enter line 3 top in mm here" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="plate_size_id">Select Plate Size</label>  
                    <select name="plate_size_id" id="plate_size_id" class="form-control">
                        {foreach from = $plate_sizes item = plate_size}
                            <option value="{$plate_size->id}">{$plate_size->name}</option>
                        {/foreach}
                    </select> 
                </div>
                <div class="form-group">
                    <label class="control-label" for="finish_material_name">Select Finish Material</label>  
                    <select name="finish_material_id" id="finish_material_id" class="form-control">
                        {foreach from = $finish_materials item = finish_material}
                            <option value="{$finish_material->id}">{$finish_material->name}</option>
                        {/foreach}
                    </select> 
                </div>
                <div class="form-group">
                    <label class="control-label" for="writing_option_name">Select Writing Option</label>  
                    <select name="writing_option_id" class="form-control">
                        {foreach from = $writing_options item = writing_option}
                            <option value="{$writing_option->id}">{$writing_option->name}</option>
                        {/foreach}
                    </select> 
                </div>
                <div class="form-group">
                    <label class="control-label" for="comments">Comments</label>  
                    <textarea name="comments" id="comments" class="form-control" rows="5"></textarea>
                </div>
      </div>    
      
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">SUBMIT
        </button>
        <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
        </button>
      </div>
      </form> 
    </div>
  </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js"></script>
<script src="{$template_base_url}/js/main.js"></script> 
<script type="text/javascript">
function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
    else
      byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    console.log(mimeString);
    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {
      type: mimeString
    });
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#sign-image')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
$(document).ready(function(){   
    var cloneCount = "{count($project_levels) + 1}";
    $("#add-level").click(function(e){
        e.preventDefault();
        var div_clone = $("#level").clone();
        var _elm = div_clone.clone();
        _elm.find('.remove_btn').show();
        _elm.attr('id', 'level-'+ cloneCount++);
        _elm.find('.signs').html('');
        _elm.find('.toggle-btn').hide();
        _elm.find('.level-signs').html('');
        _elm.appendTo("#levels");
    });

    $(document).on("click", ".clone", function(e) {
        e.preventDefault();
        var div_clone = $(this).parent().clone();
        var _elm = div_clone.clone();
        _elm.find('.remove_btn').show();
        _elm.attr('id', 'level-'+ cloneCount++);
        _elm.find('.toggle-btn').hide();
        _elm.appendTo("#levels");
    });

    $(document).on("click", ".remove_btn", function(e) {
        e.preventDefault();
        $(this).closest('.level').remove();
    });

    $(document).on("click", ".remove_sign", function(e) {
        e.preventDefault();
        $(this).closest('.sign-holder').remove();
    });

    function load_categories(el, page, first_load = false)
    {
        if (first_load) {
            var holder = $(el).parent();
        } else {
            var holder = $(el).parent().parent().parent().parent().parent();
        }
        holder.find('.signs').html(''); 
        $.get("{$base}projects/getCategories?current_page=" + page, {},function(result){
            holder.find('.toggle-btn').text('HIDE DETAILS');
            holder.find('.data-holder').addClass('in');
            holder.find('.toggle-btn').show();
            holder.find('.loader').hide();
            let html = '';
            html = result;
            holder.find('.signs').html(html);   
            holder.find('.category-search').show();
        });
    }

    function load_categories_search_name(el, page, cat_name, first_load = false)
    {
        if (first_load) {
            var holder = $(el).parent();
        } else {
            var holder = $(el).parent().parent();
        }
        holder.find('.signs').html(''); 
        $.get("{$base}projects/getBySign?cat_name=" + cat_name + "&current_page=" + page, {},function(result){
            holder.find('.loader').hide();            
            let html = '';
            html = result;
            holder.find('.signs').html(html);
            console.log(holder.find('.signs'));  
        });
    }

    $(document).on("click", ".add_sign", function(e) {
        e.preventDefault();
        var self = this;
        $(self).parent().find('.signs').html('');
        $(self).parent().find('.loader').show();
        $(self).parent().find('.product-search').hide();
        $(self).parent().find('.toggle-btn').hide();
        load_categories(self, 1, true);
    });

    function load_products_category(el, page, category_id, first_load = false)
    {
        if (first_load) {
            var holder = $(el).parent().parent().parent().parent();
        } else {
            var holder = $(el).parent().parent().parent().parent().parent();
        }
        holder.find('.signs').html(''); 
        $.get("{$base}projects/searchProductsByCategory?category_id=" + category_id + '&current_page=' + page, {},function(result){
            holder.find('.loader').hide();
            holder.find('.signs').show();  
            let html = '';
            html = result;
            holder.find('.product-search').attr('is_custom', 0);
            holder.find('.category-search').hide();
            holder.find('.product-search').show();
            holder.find('.signs').html(html);    
            $('#product-search-btn').attr('category', category_id);
        });
    }

    function load_products_signs(el, page, first_load = false)
    {
        if (first_load) {
            var holder = $(el).parent().parent().parent().parent();
        } else {
            var holder = $(el).parent().parent().parent().parent().parent();
        }
        holder.find('.signs').html(''); 
        $.get("{$base}customsigns/allCustomSigns?current_page=" + page, {},function(result){
            holder.find('.loader').hide();
            holder.find('.signs').show(); 
            let html = '';
            html = result;
            holder.find('.product-search').attr('is_custom', 1);
            holder.find('.category-search').hide();
            holder.find('.product-search').show();
            holder.find('.signs').html(html);  
        });
    }
   
    $(document).on("click", ".select_category", function(e) {
        let id = $(this).attr('id');
        let is_custom = $(this).attr('custom');
        let text = $(this).text();
        var self = this;
        $(self).parent().parent().parent().parent().find('.signs').hide();  
        $(self).parent().parent().parent().parent().find('.loader').show();
        if (is_custom == 0) {
            load_products_category(self, 1, id, true);
        } else {
            load_products_signs(self, 1, true);
        }
    });

    $(document).on("click", ".select_sign", function(e) {
        let id = $(this).attr('id');
        let text = $(this).text();
        let img = $(this).prev().find('img').attr('src');
        let excerpt = $(this).next().find('p').text();
        let isTrue = false;
        $(this).parent().parent().parent().parent().find('.level-signs .sign-holder').each(function(i, value){
            if ($(this).attr('ino') == id && !$(this).attr('is-custom')) {
                let qty = parseInt($(this).find('.qty').val()) + 1;
                $(this).find('.qty').val(qty);
                isTrue = true;
                return false;
            } 
            if ($(this).attr('ino') == id && $(this).attr('is-custom')) {
                let qty = parseInt($(this).find('.qty').val()) + 1;
                $(this).find('.qty').val(qty);
                isTrue = true;
                return false;
            } 
        });
        if (!isTrue) {
            $(this).parent().parent().parent().parent().find('.level-signs').append('<div ino="'+id+'" class="col-md-12 sign-holder" style="margin:15px 0px;border-top: solid 1px #cccc;" custom-sign="'+$(this).attr('is-custom')+'"><div class="row" style="padding-top: 15px;"><div style="width: 30%;display: inline-block; text-align:center;border: solid 1px #ccc;vertical-align: top;"><img src="'+img+'" style="width:100%; max-width:250px;height: auto;display:inline-block;"></div><div style="width:50%;display:inline-block;vertical-align: top;padding-left: 15px;">'+text+'<span class="d-block"><p style="color: #a2a4a6; border-bottom: 1px solid #333; width: auto; padding: 0px 0 0px 0; margin-bottom: 0px;">'+excerpt+'</p><p style="padding-top:8px;"><input type="text" class="location" placeholder="Enter Location here"></p></span></div><div style="width:20%;display:inline-block;"><p style="margin-left: 10px; margin-bottom: 4px;">Quantity</p><p><span class="deduct">-</span><input type="text" value="1" class="qty" style="width: 50px; margin: 0px 10px;text-align: center;"><span class="add-up">+<span></p><p class="remove_sign text-center" style="margin-top: 15px;border: solid 1px #eee;padding:4px;width:85px;">REMOVE</p></div></div></div>');
        }
        $(this).parent().parent().parent().parent().find('.product-search').hide();
        $(this).parent().parent().parent().parent().find('.signs').html('');
    });

    $('#submit').click(function(e){
        e.preventDefault();
        $("#message").html('');
        $('#loader').show();
        $("html").scrollTop(0).animate(1000);
        let level_signs = [];    
        $('.level-signs').each(function(i, value){
            let signs = '';
            let span = $(this).find('.sign-holder');
            span.each(function(i, value){
                let das = '';
                if ((span.length - 1) > i) {
                    das = '-';
                }
                signs += $(this).attr('ino') + 'custom' + $(this).attr('custom-sign') + 'location' + $(this).find('.location').val() + ':' + $(this).find('.qty').val() + das;
            });
            level_signs[i] = signs;
        });
        var self = this;
        $(self).closest('form').find('button').prop('disabled', true);
        var formdata = $(self).closest('form').serialize();
        $.ajax({
            type: "POST",
            url: "{$base}projects/update",
            data:formdata + '&level_signs=' + level_signs,
            dataType: "json",
            success: function(result){
                $('#loader').hide();
                $(self).closest('form').find('button').prop('disabled', false);
                if(result.err == 0){
                    $('#message').html(successMsg(result.msg)).fadeIn(1000);
                    $('.signs').html('');
                    $('.category-search').hide();
                    $('.product-search').hide(); 
                }
                else{
                    $('#message').html(errorMsg(result.msg)).fadeIn(1000);
                }
            }
        });
        
    });

    $(document).on("click", "#category-search-btn", function(e) {
        e.preventDefault();
        var self = this;
        let val = $(self).prev().val();
        $(self).parent().parent().find('.signs').html('');
        $(self).parent().parent().find('.loader').show();
        load_categories_search_name(self, 1, val);
    });

    function load_products_category_search_by_sign_name(el, page, sign_name, category_id, first_load = false)
    {
        if (first_load) {
            var holder = $(el).parent().parent();
        } else {
            var holder = $(el).parent().parent().parent();
        }
        holder.find('.signs').html(''); 
        $.get("{$base}projects/searchBySignName?sign_name=" + sign_name + '&category_id=' + category_id + '&current_page=' + page, {},function(result){
            holder.find('.loader').hide();
            let html = '';
            html = result;
            holder.find('.signs').html(html);      
        });
    }

    function load_products_category_search_by_product_name(el, page, product_name, category_id, first_load = false)
    {
        if (first_load) {
            var holder = $(el).parent().parent();
        } else {
            var holder = $(el).parent().parent().parent().parent().parent();
        }
        holder.find('.signs').html(''); 
        $.get("{$base}projects/searchProductsByName?product_name=" + product_name + '&category_id=' + category_id + '&current_page=' + page, {},function(result){
            holder.find('.loader').hide();
            let html = '';
            html = result;
            holder.find('.signs').html(html);      
        });
    }

    $(document).on("click", "#product-search-btn", function(e) {
        e.preventDefault();
        var self = this;
        let is_custom = $(self).parent().attr('is_custom');
        let val = $(self).prev().val();
        let category = $(self).attr('category');
        $(self).parent().parent().find('.signs').html('');
        $(self).parent().parent().find('.loader').show();
        if (is_custom == 1) {
            load_products_category_search_by_sign_name(self, 1, val, category, true);
        } else {
            load_products_category_search_by_product_name(self, 1, val, category, true);
        }   
    });

    $(document).on('click', 'ul.category-page-numbers li a.active',function(e){
        e.preventDefault();
        var page = $(this).attr('p');
        var self = this;
        var container = $(self).parent().parent().parent().attr('id');
        if (container == 'categories') {
            load_categories(self, page);
        } else {
            load_categories(self, page);
        }
    });

    $(document).on('click', 'ul.product-page-numbers li a.active',function(e){
        e.preventDefault();
        var page = $(this).attr('p');
        var self = this;
        var id = $(self).attr('ino');
        var container = $(self).parent().parent().parent().attr('id');
        if (container == 'product-categories') {
            load_products_category(self, page, id);
        } else if (container == 'product-categories-search-sign') {
            load_products_category_search_by_sign_name(self, page, $('#product-search-btn').prev().val(), id);
        } else if (container == 'product-categories-search-product') {
            load_products_category_search_by_product_name(self, page, $('#product-search-btn').prev().val(), id);
        } else {
            load_products_category(self, page, id);
        }
    });

    $(document).on('click', '.add-up', function(e){
        e.preventDefault();
        let qty = parseInt($(this).prev().val()) + 1;
        $(this).prev().val(qty);
    });

    $(document).on('click', '.deduct', function(e){
        e.preventDefault();
        let qty = (parseInt($(this).next().val()) > 1) ? (parseInt($(this).next().val()) - 1) : 1;
        $(this).next().val(qty);
    });
    $(document).on('click', '.toggle-btn', function(e){
        e.preventDefault();
        let txt = $(this).text();
        if (txt == 'HIDE DETAILS') {
            $(this).text('SHOW DETAILS');
        } else {
            $(this).text('HIDE DETAILS');
        }
        $(this).parent().find('.data-holder').collapse('toggle'); // toggle collapse
    });
    $('body').on('click','.add_custom_sign',function(e){
        e.preventDefault();          
        $('#message2').html('');
        $('#addCustomSign').modal('show');
        $('#image').val('');
        $('#sign-image').attr('src', '{$img_dir}'+'default.png');
        let img = '{$img_dir}'+'default.png';
        let text = '';
        let excerpt = '';
        var self = this;
        localStorage.setItem("index", $(self).parent().parent().parent().parent().index());
    });
    $('#add-custom-sign-form').submit(function(e){
        e.preventDefault();
        let index = localStorage.getItem("index");
        let holder = $('.level:eq("'+index+'")');
        $('#custom_sign_customer_id').val($('#customer_id').find("option:selected").val());
            var formdata = new FormData($(this)[0]);
            var blob = '';
            html2canvas(document.querySelector("#canvas")).then(function (canvas) {              
            var dataURL = canvas.toDataURL();
            var blob = dataURItoBlob(dataURL);
            formdata.set('image', blob);
            $.ajax({
                type: "POST",
                url: "{$base}projects/create_custom_sign",
                data:formdata,
                dataType:'json',
                processData: false,
                contentType: false,
                success: function(result){
                    $('#loader2').hide();
                    if(result.err == 0){
                        let sign_id = result.data.id;
                        img = '{$img_dir}' + result.data.image;
                        text = result.data.custom_sign_name;
                        excerpt = result.data.plate_size_name;
                        $('#message2').html(successMsg(result.msg)).fadeIn(1000,function(){
                            $("#addCustomSign").modal('hide');   
                             holder.find('.level-signs').append('<div ino="'+sign_id+'" class="col-md-12 sign-holder" style="margin:15px 0px;border-top: solid 1px #cccc;" custom-sign="1"><div class="row" style="padding-top: 15px;"><div style="width: 30%;display: inline-block; text-align:center;border: solid 1px #ccc;vertical-align: top;"><img src="'+img+'" style="width:100%; max-width:250px;height: auto;display:inline-block;"></div><div style="width:50%;display:inline-block;vertical-align: top;padding-left: 15px;">'+text+'<span class="d-block"><p style="color: #a2a4a6; border-bottom: 1px solid #333; width: auto; padding: 0px 0 0px 0; margin-bottom: 0px;">'+excerpt+'</p><p style="padding-top:8px;"><input type="text" class="location" placeholder="Enter Location here"></p></span></div><div style="width:20%;display:inline-block;"><p style="margin-left: 10px; margin-bottom: 4px;">Quantity</p><p><span class="deduct">-</span><input type="text" value="1" class="qty" style="width: 50px; margin: 0px 10px;text-align: center;"><span class="add-up">+<span></p><p class="remove_sign text-center" style="margin-top: 15px;border: solid 1px #eee;padding:4px;width:85px;">REMOVE</p></div></div></div>');  
                             $('#add-custom-sign-form')[0].reset();  
                             localStorage.removeItem("index");  
                             holder.find('.signs').html('');
                             holder.find('.category-search').hide();
                             holder.find('.product-search').hide();
                              $('#line1').text('');
                            $('#line2-1').text('');
                            $('#line2-2').text('');
                            $('#line3-1').text('');
                            $('#line3-2').text('');
                            $('#line3-3').text('');
                        });
                    }
                    else{
                        $('#message2').html(errorMsg(result.msg)).fadeIn(1000,function(){
                            $("#addCustomSign").scrollTop(0).animate(1000); 
                        });
                    }
                }
            });
        });
    });
    
    $('#plate_size_id').on('change', function(e){
        $('#loader2').show();
        $('#message2').html('');
        var target = $(this);
        var plate_size_id = $(this).val();
        var finish_material_id = $('#finish_material_id').find("option:selected").val();
        var data = convertToObject2(plate_size_id, finish_material_id);
        $.getJSON("{$base}finishmaterialtemplates/getByPlateSizeIdFinishMaterialId", data,function(result){
            $('#loader2').hide();
            let plate_size = $('#plate_size_id').find("option:selected").text().split(' X ');
            let img = 'default.png';

            if (result[0] !== null) {
                plate_size = result[0].plate_size.split(' X ');
                img = result[0].image;
            } 

            width = plate_size[0].replace("MM", "");
            height = plate_size[1].replace("MM", "");
        
            $('#canvas').css('max-width', width+'px');
            $('#canvas').css('height', height+'px');
            $('#canvas').css('width', '100%');
            $('#sign-image').attr('width', width);
            $('#sign-image').attr('height', height);
            $('#sign-image').css('width', '100%');
            $('#sign-image').css('max-width', width + 'px');
            $('#sign-image').attr('src', "{$img_dir}" + img);
    });
    });

    $('#finish_material_id').on('change', function(e){
        $('#loader2').show();
        $('#message2').html('');
        var target = $(this);
        var plate_size_id = $('#plate_size_id').find("option:selected").val();
        var finish_material_id = $(this).val();
        var data = convertToObject2(plate_size_id, finish_material_id);
        $.getJSON("{$base}finishmaterialtemplates/getByPlateSizeIdFinishMaterialId", data,function(result){
            $('#loader2').hide();
            let plate_size = $('#plate_size_id').find("option:selected").text().split(' X ');
            let img = 'default.png';

            if (result[0] !== null) {
                plate_size = result[0].plate_size.split(' X ');
                img = result[0].image;
            } 

            width = plate_size[0].replace("MM", "");
            height = plate_size[1].replace("MM", "");
        
            $('#canvas').css('max-width', width+'px');
            $('#canvas').css('height', height+'px');
            $('#canvas').css('width', '100%');
            $('#sign-image').attr('width', width);
            $('#sign-image').attr('height', height);
            $('#sign-image').css('width', '100%');
            $('#sign-image').css('max-width', width + 'px');
            $('#sign-image').attr('src', "{$img_dir}" + img);
        });
    });

        if ($('#select_line').val() == 'line1') {
            $('#line2-1').text('');
            $('#line2-2').text('');
            $('#line3-1').text('');
            $('#line3-2').text('');
            $('#line3-3').text('');
            $('.line-1').show();
            $('.line-2').hide();
            $('.line-3').hide();
        } else if ($(this).val() == 'line2') {
            $('#line1').text('');
            $('#line3-1').text('');
            $('#line3-2').text('');
            $('#line3-3').text('');
            $('.line-2').show();
            $('.line-1').hide();
            $('.line-3').hide();
        } else if ($(this).val() == 'line3') {
            $('#line1').text('');
            $('#line2-1').text('');
            $('#line2-2').text('');
            $('.line-3').show();
            $('.line-2').hide();
            $('.line-1').hide();
        }

        $('#select_line').on('change', function(){
            if ($(this).val() == 'line1') {
                $('#line2-1').text('');
                $('#line2-2').text('');
                $('#line3-1').text('');
                $('#line3-2').text('');
                $('#line3-3').text('');
                $('.line-1').show();
                $('.line-2').hide();
                $('.line-3').hide();
            } else if ($(this).val() == 'line2') {
                $('#line1').text('');
                $('#line3-1').text('');
                $('#line3-2').text('');
                $('#line3-3').text('');
                $('.line-2').show();
                $('.line-1').hide();
                $('.line-3').hide();
            } else if ($(this).val() == 'line3') {
                $('#line1').text('');
                $('#line2-1').text('');
                $('#line2-2').text('');
                $('.line-3').show();
                $('.line-2').hide();
                $('.line-1').hide();
            }
        });

        if ($('#custom-line1-1').parent().parent().parent().find('#is_custom_name').is(":checked")) {
            $('#line1').text($(this).val());
        }

        $('#custom-line1-1').on('keyup', function(){
            if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
                $('#line1').text($(this).val());
            }
        });
        if ($('#custom-line2-1').parent().parent().parent().find('#is_custom_name').is(":checked")) {
                $('#line2-1').text($(this).val());
            }
        $('#custom-line2-1').on('keyup', function(){
            if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
                $('#line2-1').text($(this).val());
            }
        });

        if ($('#custom-line2-2').parent().parent().parent().find('#is_custom_name').is(":checked")) {
                $('#line2-2').text($(this).val());
            }

        $('#custom-line2-2').on('keyup', function(){
            if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
                $('#line2-2').text($(this).val());
            }
        });

        if ($('#custom-line3-1').parent().parent().parent().find('#is_custom_name').is(":checked")) {
            $('#line3-1').text($(this).val());
        }

        $('#custom-line3-1').on('keyup', function(){
            if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
                $('#line3-1').text($(this).val());
            }
        });

        if ($('#custom-line3-2').parent().parent().parent().find('#is_custom_name').is(":checked")) {
            $('#line3-2').text($(this).val());
        }
        $('#custom-line3-2').on('keyup', function(){
            if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
                $('#line3-2').text($(this).val());
            }
        });

        if ($('#custom-line3-3').parent().parent().parent().find('#is_custom_name').is(":checked")) {
            $('#line3-3').text($(this).val());
        }

        $('#custom-line3-3').on('keyup', function(){
            if ($(this).parent().parent().parent().find('#is_custom_name').is(":checked")) {
                $('#line3-3').text($(this).val());
            }
        });

        $('#is_custom_name').on('click', function(){
            if ($(this).is(":checked")) {
                if ($('#select_line').find("option:selected").val() == 'line1') {
                    $('#line2-1').text('');
                    $('#line2-2').text('');
                    $('#line3-1').text('');
                    $('#line3-2').text('');
                    $('#line3-3').text('');
                    $('.line-1').show();
                    $('.line-2').hide();
                    $('.line-3').hide();
                    $('#line1').text($('#custom-line1-1').val());
                } else if ($('#select_line').find("option:selected").val() == 'line2') {
                    $('#line1').text('');
                    $('#line3-1').text('');
                    $('#line3-2').text('');
                    $('#line3-3').text('');
                    $('.line-2').show();
                    $('.line-1').hide();
                    $('.line-3').hide();
                    $('#line2-1').text($('#custom-line2-1').val());
                    $('#line2-2').text($('#custom-line2-2').val());
                } else if ($('#select_line').find("option:selected").val() == 'line3') {
                    $('#line1').text('');
                    $('#line2-1').text('');
                    $('#line2-2').text('');
                    $('.line-3').show();
                    $('.line-2').hide();
                    $('.line-1').hide();
                    $('#line3-1').text($('#custom-line3-1').val());
                    $('#line3-2').text($('#custom-line3-2').val());
                    $('#line3-3').text($('#custom-line3-3').val());
                }
            } else {
                $('#line1').text('');
                $('#line2-1').text('');
                $('#line2-2').text('');
                $('#line3-1').text('');
                $('#line3-2').text('');
                $('#line3-3').text('');
            }
        });

        let px = '3.78';
        $('#custom-line1-letter_heignt').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line1').css('font-size', mm.toFixed(2) +'px');
        });

        $('#custom-line2-1-letter_heignt').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line2-1').css('font-size', mm.toFixed(2) +'px');
        });

        $('#custom-line2-2-letter_heignt').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line2-2').css('font-size', mm.toFixed(2) +'px');
        });

        $('#custom-line3-1-letter_heignt').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line3-1').css('font-size', mm.toFixed(2) +'px');
        });

        $('#custom-line3-2-letter_heignt').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line3-2').css('font-size', mm.toFixed(2) +'px');
        });

        $('#custom-line3-3-letter_heignt').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line3-3').css('font-size', mm.toFixed(2) +'px');
        });


        $('#custom-line1-letter_left').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line1').css('left', mm.toFixed(2) +'px');
        });

        $('#custom-line2-1-letter_left').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line2-1').css('left', mm.toFixed(2) +'px');
        });

        $('#custom-line2-2-letter_left').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line2-2').css('left', mm.toFixed(2) +'px');
        });

        $('#custom-line3-1-letter_left').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line3-1').css('left', mm.toFixed(2) +'px');
        });

        $('#custom-line3-2-letter_left').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line3-2').css('left', mm.toFixed(2) +'px');
        });

        $('#custom-line3-3-letter_left').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line3-3').css('left', mm.toFixed(2) +'px');
        });

        $('#custom-line1-letter_top').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line1').css('top', mm.toFixed(2) +'px');
        });

        $('#custom-line2-1-letter_top').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line2-1').css('top', mm.toFixed(2) +'px');
        });

        $('#custom-line2-2-letter_top').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line2-2').css('top', mm.toFixed(2) +'px');
        });

        $('#custom-line3-1-letter_top').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line3-1').css('top', mm.toFixed(2) +'px');
        });

        $('#custom-line3-2-letter_top').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line3-2').css('top', mm.toFixed(2) +'px');
        });

        $('#custom-line3-3-letter_top').on('keyup', function(){
            let val = $(this).val();
            let mm = val * parseFloat(px);

            $('#line3-3').css('top', mm.toFixed(2) +'px');
        });


        $('#select-status').on('change', function(){
            location.href = "{$base}customsigns?status=" + $(this).val();
        });

        $('#line1').css('color', $('#color-line1-1').val());
        $('#color-line1-1').on('change', function(){
            let color = $(this).val();
            $('#line1').css('color', color);
        });

        $('#line2-1').css('color', $('#color-line2-1').val());
        $('#color-line2-1').on('change', function(){
            let color = $(this).val();
            $('#line2-1').css('color', color);
        });
        $('#line2-2').css('color', $('#color-line2-2').val());
        $('#color-line2-2').on('change', function(){
            let color = $(this).val();
            $('#line2-2').css('color', color);
        });
        $('#line3-1').css('color', $('#color-line3-1').val());
        $('#color-line3-1').on('change', function(){
            let color = $(this).val();
            $('#line3-1').css('color', color);
        });
        $('#line3-2').css('color', $('#color-line3-2').val());
        $('#color-line3-2').on('change', function(){
            let color = $(this).val();
            $('#line3-2').css('color', color);
        });
        $('#line3-3').css('color', $('#color-line3-3').val());
        $('#color-line3-3').on('change', function(){
            let color = $(this).val();
            $('#line3-3').css('color', color);
        });
}); 

</script>