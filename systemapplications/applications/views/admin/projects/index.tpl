<section class="page-section woocommerce-account woocommerce-page woocommerce-edit-account">
	<div class="container woocommerce">			
		<div class="row">
			<div class="col-md-9">
				<div id="table-holder">
					<h1 class="text-left mb-5 font-weight-700">{$title}</h1>
					<div class="row">
						<div class="col-md-6 mb-5">
							<label class="control-label" for="status">Select Status</label>  
							<select id="select-status" class="form-control">
								<option value="active" {if $status == 'active'}selected="selected"{/if}>Active</option>
								<option value="inactive"  {if $status == 'inactive'}selected="selected"{/if}>In Active</option>
							</select>
						</div>
					</div>
					<div id="loader" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
					<div id="message" hidden></div>
					{if $error.err == 1 && $error.msg !=''}
	                    <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">×</button><span class="msg"><strong>Error!</strong> {$error.msg}</span>
	                    </div> 
	                {/if}
					<table id="example" class="table table-hover dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Customer Name</th>
								<th>Project Name</th>
								<th>Project Address</th>
								<th>Status</th>
								<th>Date Added</th>
								<th>Action</th>
							</tr>
							<tbody>
								
							</tbody>	
						</thead>	
					</table>	
				</div>	
			</div>
			<div class="col-md-3">
				{include file='includes/nav.tpl'}
		</div>
		</div>
	</div>
</section>
<div class="modal fade" id="updateStatus" tabindex="-1" role="dialog" aria-labelledby="updateStatus" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="update-status-form" role="form" method="post" action="">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel">Approved Project</h4>
			</div>
			<div class="modal-body">
				<div id="loader2" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message2" hidden></div>
				<input type="hidden" name="project_id" id="id"></input>
				<div class="form-group">
					<p class="d-block">
						<input id="approved" name="is_approved" type="radio" value="1"> Approved
					</p>
					<p class="d-block">
						<input id="not_approved" name="is_approved" type="radio" value="0"> Not Approved
					</p>
				</div>
				<div class="form-group">
					<label class="control-label" for="comments">Comments</label>  
					<textarea name="comments" id="comments" class="form-control" rows="5"></textarea>
				</div>
			</div>		
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">SUBMIT
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>
<div class="modal fade" id="deleteProject" tabindex="-1" role="dialog" aria-labelledby="deleteProject" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel"><span class="fa fa-exclamation" style="color:#D91E18;"></span></h4>
			</div>
			<div class="modal-body">
				<div id="loader5" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message5" hidden></div>
				<div class="form-group">
					<p>Are you sure you want to delete this project?</p>
				</div>
			</div>
			<div class="modal-footer mt-3">
				<button id="yesBtn" type="button" class="btn btn-danger" title="Yes">Yes
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" title="No">No
				</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="{$template_base_url}/datatables/jquery.dataTables.min.js"></script>
<script src="{$template_base_url}/datatables/dataTables.bootstrap.min.js"></script> 
<script src="{$template_base_url}/datatables/dataTables.responsive.min.js"></script> 
<script src="{$template_base_url}/datatables/responsive.bootstrap.min.js"></script>  
<script src="{$template_base_url}/datatables/dataTables.searchHighlight.min.js"></script>    
<script src="{$template_base_url}/datatables/jquery.highlight.js"></script> 
<script src="{$template_base_url}/js/fnReloadAjax.js"></script> 
<script src="{$template_base_url}/js/main.js"></script> 
<script type="text/javascript">

$(document).ready(function(){	
var table = $('#example');

	table.dataTable( {
		searchHighlight: true,
		oLanguage: {
			"bDeferRender": true,
		"bProcessing": true,	
			sProcessing: "<img src='{$img_dir}ajax-loader.gif'>"
	},
	"sAjaxSource": '{$base}projects/allProjects?status={$status}',
	"bDeferRender": true,
	"bProcessing": true,
	"aoColumns": [
	{
		"mDataProp": "full_name",
	},
	{
		"mDataProp": "project_name"

	}, 	
	{
		"mDataProp": "project_address"

	}, 
	{	
		"mDataProp": "id",
		"mRender": function(data, type, row) {
			if (row.status == 1) {
				return 'Active';
			} else {
				return 'In Active';
			}
		}	
	},	
	{
		"mDataProp": "created_at"

	}, 
	{
		"mDataProp": "id",
		"mRender": function(data, type, row) {
			{if $is_admin == true}
				return '<a href="javascript:;" class="btn btn-sm btn-default update-status" title="Update Status" id="'+row.id+'">'+(row.is_approved == 1 ? 'Approved' : 'Not Approved') +'</a> <a href="{$base}projects/view?project_id='+row.id+'" class="btn btn-sm btn-info view" title="View More Info" id="'+row.id+'"><i class="fa fa-info"></i></a> <a href="{$base}projects/edit?project_id='+row.id+'" class="btn btn-sm btn-warning edit" title="Edit" id="'+row.id+'"><i class="fa fa-edit"></i></a> <a href="javascript:;" class="btn btn-sm btn-danger delete" title="Delete" id="'+row.id+'"><i class="icon fa fa-trash"></i></a>';
			{else}
				return '<a href="{$base}projects/view?project_id='+row.id+'" class="btn btn-sm btn-info view" title="View More Info" id="'+row.id+'"><i class="fa fa-info"></i></a> <a href="{$base}projects/edit?project_id='+row.id+'" class="btn btn-sm btn-warning edit" title="Edit" id="'+row.id+'"><i class="fa fa-edit"></i></a> <a href="javascript:;" class="btn btn-sm btn-danger delete" title="Delete" id="'+row.id+'"><i class="icon fa fa-trash"></i></a>';
			{/if}
		}	
	}
	]});	

	$('body').on('click','.update-status',function(e){
		e.preventDefault();
		$('#message2').html('');
		$('#loader').show();
		var target = $(this);
		var id = target.attr('id');
		var data = convertToObject(id);
		$.getJSON("{$base}projects/getByID",data,function(result){
			$('#loader').hide();
			$('#id').val(result[0].id);
			$('#comments').val(result[0].comments);
			if (result[0].is_approved == 1) {
				$("#approved").attr('checked', true);
				$("#not_approved").attr('checked', false);
			} else {
				$("#approved").attr('checked', false);
				$("#not_approved").attr('checked', true);
			}
			$('#updateStatus').modal('show');
		});

	});

	$('#update-status-form').submit(function(e){
		e.preventDefault();
		var self = this;
		$('#loader2').show();
		$('#message2').html('');
		$(self).find('button').prop('disabled', true);
		$.post("{$base}projects/update_status",$(self).serialize(),function(result){
			$('#loader2').hide();
			$(self).find('button').prop('disabled', false);
			if(result.err == 0){
				$('#message2').html(successMsg(result.msg)).fadeIn(1000,function(){
					$("#updateStatus").scrollTop(0).animate(1000);
					table.fnReloadAjax();
				});
			}
			else{
				$('#message2').html(errorMsg(result.msg)).fadeIn(1000,function(){
					$("#updateStatus").scrollTop(0).animate(1000);
				});
			}
			
		},"json");
	});

	$('body').on('click','.delete',function(e){
		e.preventDefault();
		$('#message2').html('');
		$('#message3').html('');
		$('#message4').html('');
		$('#message5').html('');
		var id = $(this).attr('id');
		$('#yesBtn').attr('ino', id);
		$('body').on("click", "#yesBtn", function(e){
			var self = this;
			var id = $(self).attr('ino');
			var data = convertToObject(id);
			$('#loader5').show();
			$(self).closest('#deleteProject').find('button').prop('disabled', true);
			$.post("{$base}projects/deleteProject",data,function(result){
				$('#loader5').hide();
				$(self).hide();
				$(self).next().hide();
				$(self).closest('#deleteProject').find('button').prop('disabled', false);
				if(result.err == 0){
					$('#message5').html(successMsg(result.msg)).fadeIn(1000,function(){
						$("#deleteProject").scrollTop(0).animate(1000);
						table.fnReloadAjax();
					});
				}
				else{
					$('#message5').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#deleteProject").scrollTop(0).animate(1000);
					});
				}
			},"json");
		});	
		$('#deleteProject').find('button').show();
		$('#deleteProject').modal('show');
	});
	
	$('#deleteProject').on('hidden.bs.modal', function (e) {
		$('#message2').html('');
		$('#message3').html('');
		$('#message4').html('');
		$('#message5').html('');
	});

	$('#select-status').on('change', function(){
		location.href = "{$base}projects?status=" + $(this).val();
	});
});	

</script>