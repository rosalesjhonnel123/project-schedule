<section class="page-section woocommerce-account woocommerce-page woocommerce-edit-account">
	<div class="container woocommerce">		
		<div class="col-md-9">
			<h1 class="text-left mb-5 font-weight-700">{$title}</h1>
			<div class="row text-center" style="margin-bottom:30px;">
				<p><h4>{$project->project_name}</h4><span class="d-block">{$project->project_address}</span></p>
			</div>
			<div class="row mb-5">
				<div class="form-group col-md-6">
					<label class="control-label">Select view quantity by:</label>  
					<select class="form-control" id="type">
						<option value="product" {if $type eq 'product'}selected="selected"{/if}>Product</option>
						<option value="level" {if $type eq 'level'}selected="selected"{/if}>Level</option>
					</select>
				</div>
				<div class="form-group col-md-6" style="margin-top:3.1rem;">
					<a href="javascript:;" class="btn btn-info btn-block" id="export-excel">EXPORT AS XLSX</a>
				</div>
			</div> 
			<div class="row">
				<div class="form-group col-md-12 level">
					<div class="level-signs">
						{if $type eq 'product'}
						{foreach from = $level_signs item = level_sign key = key}
							<div class="row text-left mb-5">
								<div style="width: 30%;display: inline-block; text-align:center;border: solid 1px #ccc;vertical-align: top;">
									{$data = explode('|',$key)}
									<img src="{$data[1]}" class="d-block" style="width:100%; max-width:250px;height: auto;display:inline-block;"/>
								</div>
								<div style="width:48%;display:inline-block;vertical-align: top;padding-left: 15px;">
									{$data[0]}
								</div>
								<div style="width:20%;display:inline-block;">
									QTY: {if $level_sign eq 1}{$level_sign}PC{else}{$level_sign}PCS{/if}
								</div>
							</div>
						{/foreach}	
						{else}
						{foreach from = $level_signs item = level_sign key = key}
							<span class="font-weight-700" style="color: #a2a4a6; border-bottom: 1px solid #eee; display: block; padding: 0px 0 0px 0; margin-bottom: 0px; text-align:left">{$key}</span>
							<div class="mt-3 mb-3">
							{foreach from = $level_sign item = value}
								<div class="row row-quantities mb-5">
									<div style="width: 30%;display: inline-block; text-align:center;border: solid 1px #ccc;vertical-align: top;">
										{$data = explode('|',$key)}
										<img src="{$value['product_image']}" class="d-block" style="width:100%; max-width:250px;height: auto;display:inline-block;"/>
									</div>
									<div style="width:48%;display:inline-block;vertical-align: top;padding-left: 15px;">
										{$value['product_name']}
									</div>
									<div style="width:20%;display:inline-block;">
										QTY: {if $value['quantity'] eq 1}{$value['quantity']}PC{else}{$value['quantity']}PCS{/if}
									</div>
								</div>
							{/foreach}	
							</div>
						{/foreach}	
						{/if}
					</div>
				</div>
			</div> 
		</div>
		<div class="col-md-3">
			{include file='includes/nav.tpl'}
		</div>
	</div>
</section>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
	$('#type').on('change', function(e){
		location.href= "{$base}projects/view_quantities?project_id={$project->id}&type=" + $(this).val();
	});

	$('#export-excel').on('click', function(e){
		e.preventDefault();
		var self = this;
		$(self).text('LOADING...');
		$.ajax({
			type: "GET",
			url: "{$base}projects/export_file",
			data: "project_id={$project->id}&type=" + $('#type').val(),
            dataType: "json",
    		success: function(result){
    			$(self).text('EXPORT AS XLSX');
    			location.href= "{site_url()}/"+$('#type').val()+"_view_quantities.xlsx";
    		}
		});
	});
});
</script>
