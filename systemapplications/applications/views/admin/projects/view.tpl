<section class="page-section woocommerce-account woocommerce-page woocommerce-edit-account">
	<div class="container woocommerce">		
		<div class="col-md-9">
			<h1 class="text-left mb-5 font-weight-700">{$title}</h1>
			<div class="row text-center">
				<p><h4>{$project->project_name}</h4><span class="d-block">{$project->project_address}</span></p>
			</div>
			{foreach from = $project_levels item = project_level key=key}
			<div class="row" id="levels{if $key eq 0}{else}-{$key+1}{/if}">
				<div class="form-group col-md-12 level" id="level">
					<label class="control-label d-block" for="level">Level: {$project_level->name} <button type="button" class="btn btn-info toggle-btn pull-right"  data-toggle="collapse">Hide Details</button></label> 
					<div class="data-holder collapse in">
						<div class="level-signs">
							{foreach from = $level_signs item = level_sign}
							{if $level_sign['level_id'] eq $project_level->level_id}
							<div ino="{$level_sign['level_id']}" class="col-md-12 sign-holder" style="margin:15px 0px;border-top: solid 1px #cccc;"><div class="row" style="padding-top: 15px;"><div style="width: 20%;display: inline-block; text-align:center;border: solid 1px #ccc;vertical-align: top;"><img src="{$level_sign['image']}" style="width:100%; max-width:100px;height: auto;display:inline-block;"></div><div style="width:50%;display:inline-block;vertical-align: top;padding-left: 15px;">{$level_sign['name']}<span class="d-block"><p style="color: #a2a4a6; border-bottom: 1px solid #333; width: auto; padding: 0px 0 0px 0; margin-bottom: 0px;">{$level_sign['post_excerpt']}</p></span></div><div style="width:30%;display:inline-block;"><p style="margin-left: 10px; margin-bottom: 4px;">Quantity</p><p><input type="text" value="{$level_sign['quantity']}" class="qty" style="width: 50px; margin: 0px 10px;text-align: center;" readonly="readonly"></p></div></div></div>
							{/if}
							{/foreach}	
						</div>
					</div>
				</div>
			</div> 
			{/foreach}	
			<div class="row  mt-5">
				<div class="form-group col-md-4">
					<a href="{$base}projects/print?project_id={$project->id}" class="btn btn-info btn-block" id="print-schedule">PRINT SCHEDULE</a>
				</div>
				<div class="form-group col-md-4">
					<button type="button" class="btn btn-info btn-block" id="email-schedule">EMAIL SCHEDULE</button>
				</div>
				<div class="form-group col-md-4">
					<a href="{$base}projects/view_quantities?project_id={$project->id}" class="btn btn-info btn-block" id="view-quantities">VIEW QUANTITIES</a>
				</div>
			</div> 
		</div>
		<div class="col-md-3">
			{include file='includes/nav.tpl'}
		</div>
	</div>
</section>
<div class="modal fade" id="sendEmail" tabindex="-1" role="dialog" aria-labelledby="sendEmail" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="send-email-form" role="form" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel">Compose Email</h4>
			</div>
			<div class="modal-body">
				<div id="loader6" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message6" hidden></div>
				<input type="hidden" name="id" id="id" value="{$project->id}"></input>
				<input type="hidden" name="user_name" id="user_name" value="{$current_user->display_name}"></input>
				<div class="form-group">
					 <label class="control-label" for="from_email">From Email</label>  
					  <input id="from_email" name="from_email" type="text" placeholder="Enter from email here" class="form-control" value="{$current_user->user_email}">
				</div>
				<div class="form-group">
					 <label class="control-label" for="to_email">To Email</label>  
					  <input id="to_email" name="to_email" type="text" placeholder="Enter to email here" class="form-control" value="{get_option('admin_email')}">
				</div>
				<div class="form-group">
					<label class="control-label" for="comments">Message</label>  
					<textarea name="message" id="message" class="form-control" rows="5"></textarea>
				</div>
			</div>		
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">SEND
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="{$template_base_url}/js/main.js"></script> 
<script type="text/javascript">

$(document).ready(function(){	
  	$(document).on('click', '.toggle-btn', function(e){
  		e.preventDefault();
  		let txt = $(this).text();
  		if (txt == 'Hide Details') {
  			$(this).text('Show Details');
  		} else {
  			$(this).text('Hide Details');
  		}
      	$(this).parent().parent().find('.data-holder').collapse('toggle'); // toggle collapse
    });

    $('#email-schedule').on('click', function(e){
    	e.preventDefault();
    	$('#sendEmail').modal('show');
    })

    $('#send-email-form').submit(function(e){
		e.preventDefault();

		$('#loader6').show();
		$('#message6').html('');
		$.post("{$base}projects/send_email",$(this).serialize(),function(result){
			$('#loader6').hide();
			if(result.err == 0){
				$('#message6').html(successMsg(result.msg)).fadeIn(1000,function(){
					$("#sendEmail").scrollTop(0).animate(1000);
				});
			}
			else{
				$('#message3').html(errorMsg(result.msg)).fadeIn(1000,function(){
					$("#sendEmail").scrollTop(0).animate(1000);
				});
			}		
		},"json");
	});
});	

</script>