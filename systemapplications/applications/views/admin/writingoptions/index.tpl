<section class="page-section woocommerce-account woocommerce-page woocommerce-edit-account">
	<div class="container woocommerce">			
		<div class="row">
			<div class="col-md-9">
				<div id="table-holder">
					<h1 class="text-left mb-5 font-weight-700">{$title}</h1>
					<div class="row">
						<div class="col-md-6 mb-5">
							<label class="control-label" for="status">Select Status</label>  
							<select id="select-status" class="form-control">
								<option value="active" {if $status == 'active'}selected="selected"{/if}>Active</option>
								<option value="inactive"  {if $status == 'inactive'}selected="selected"{/if}>In Active</option>
							</select>
						</div>
					</div>
					<button type="button" class="btn btn-info add create-btn mb-5">CREATE WRITING OPTION</button>
					<div id="loader" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
					<div id="message" hidden></div>
					<table id="example" class="table table-hover dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Name</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
							<tbody>
								
							</tbody>	
						</thead>	
					</table>	
				</div>	
			</div>
			<div class="col-md-3">
				{include file='includes/nav.tpl'}
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="addWritingOption" tabindex="-1" role="dialog" aria-labelledby="addWritingOption" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="add-writing-option-form" role="form" method="post" action="">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel">Create Writing Option</h4>
			</div>
			<div class="modal-body">
				<div id="loader2" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message2" hidden></div>
				<div class="form-group">
					 <label class="control-label" for="writing_option_name">Name</label>  
					  <input id="writing_option_name" name="writing_option_name" type="text" placeholder="Enter Name here" class="form-control input-md">
				</div>
			</div>		
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">SUBMIT
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<div class="modal fade" id="updateWritingOption" tabindex="-1" role="dialog" aria-labelledby="updateWritingOption" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="update-writing-option-form" role="form" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel">Edit Writing Option</h4>
			</div>
			<div class="modal-body">
				<div id="loader3" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message3" hidden></div>
				<input type="hidden" name="id" id="id"></input>
				<div class="form-group">
					 <label class="control-label" for="writing_option_name">Name</label>  
					  <input id="writing_option_name2" name="writing_option_name" type="text" placeholder="Enter name here" class="form-control input-md">
				</div>
				<div class="form-group">
					<label class="control-label" for="status">Status</label>  
					<select id="status" name="status" class="form-control">
						<option value="0">In Active</option>
						<option value="1">Active</option>
					</select>
				</div>
			</div>		
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">UPDATE
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" title="CLOSE">CLOSE
				</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<div class="modal fade" id="deleteWritingOption" tabindex="-1" role="dialog" aria-labelledby="deleteWritingOption" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#"><i class="fa fa-times"></i></a></button>
				<h4 class="modal-title" id="myModalLabel"><span class="fa fa-exclamation" style="color:#D91E18;"></span></h4>
			</div>
			<div class="modal-body">
				<div id="loader4" hidden><img src="{$img_dir}ajax-loader.gif" alt="" class="center-block"/><div>&nbsp;</div></div>
				<div id="message4" hidden></div>
				<div class="form-group">
					<p>Are you sure you want to delete this writing option?</p>
				</div>
			</div>
			<div class="modal-footer mt-3">
				<button id="yesBtn" type="button" class="btn btn-danger" title="Yes">Yes
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" title="No">No
				</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="{$template_base_url}/datatables/jquery.dataTables.min.js"></script>
<script src="{$template_base_url}/datatables/dataTables.bootstrap.min.js"></script> 
<script src="{$template_base_url}/datatables/dataTables.responsive.min.js"></script> 
<script src="{$template_base_url}/datatables/responsive.bootstrap.min.js"></script>  
<script src="{$template_base_url}/datatables/dataTables.searchHighlight.min.js"></script>    
<script src="{$template_base_url}/datatables/jquery.highlight.js"></script> 
<script src="{$template_base_url}/js/fnReloadAjax.js"></script> 
<script src="{$template_base_url}/js/main.js"></script> 
<script type="text/javascript">

$(document).ready(function(){	
	var table = $('#example');

  		table.dataTable( {
  			searchHighlight: true,
  			"order": [1,'desc'],
  			oLanguage: {
  				"bDeferRender": true,
				"bProcessing": true,	
       			sProcessing: "<img src='{$img_dir}ajax-loader.gif'>"
    		},
			"sAjaxSource": '{$base}writingoptions/allWritingOptions?status={$status}',
			"bDeferRender": true,
			"bProcessing": true,
			"aoColumns": [
			{
				"mDataProp": "name"
			},	
			{
			"mDataProp": "id",
				"mRender": function(data, type, row) {
					if (row.status == 1) {
						return 'Active';
					} else {
						return 'In Active';
					}
				}	
			},	
			{
				"mDataProp": "id",
				"mRender": function(data, type, row) {
					return '<a href="javascript:;" class="btn btn-sm btn-warning edit" title="Edit" id="'+row.id+'"><i class="fa fa-edit"></i></a> <a href="javascript:;" class="btn btn-sm btn-danger delete" title="Delete" id="'+row.id+'"><i class="icon fa fa-trash"></i></a>';
				}	
			}

			]});	

		$('body').on('click','.add',function(e){
			e.preventDefault();
			
			$('#message2').html('');
			$('#addWritingOption').modal('show');
		});

		$('body').on('click','.edit',function(e){
			e.preventDefault();
			
			$('#loader').show();
			$('#message3').html('');
			$('#message').html('');
			var target = $(this);
			var id = target.attr('id');
			var data = convertToObject(id);
			$.getJSON("{$base}writingoptions/getByID",data,function(result){
				$('#loader').hide();
				if(result.err == 1){
					$('#message').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#table-holder").scrollTop(0).animate(1000);
					});
				} else {
					$('#id').val(result[0].id);
					$('#writing_option_name2').val(result[0].name);
					$('#status').val(result[0].status);
					$('#updateWritingOption').modal('show');
				}
			});

		});

		$('#add-writing-option-form').submit(function(e){
			e.preventDefault();

			$('#loader2').show();
			$('#message2').html('');
			var self = this;
			$(self).find('button').prop('disabled', true);
			$.post("{$base}writingoptions/create",$(self).serialize(),function(result){
				$('#loader2').hide();
				$(self).find('button').prop('disabled', false);
				if(result.err == 0){
					$('#message2').html(successMsg(result.msg)).fadeIn(1000,function(){
						$("#addWritingOption").scrollTop(0).animate(1000);
						table.fnReloadAjax();
					});
				}
				else{
					$('#message2').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#addWritingOption").scrollTop(0).animate(1000);
					});
				}
			},"json");
		});

		$('#update-writing-option-form').submit(function(e){
			e.preventDefault();

			$('#loader3').show();
			$('#message3').html('');
			var self = this;
			$(self).find('button').prop('disabled', true);
			$.post("{$base}writingoptions/update",$(self).serialize(),function(result){
				$('#loader3').hide();
				$(self).find('button').prop('disabled', false);
				if(result.err == 0){
					$('#message3').html(successMsg(result.msg)).fadeIn(1000,function(){
						$("#updateWritingOption").scrollTop(0).animate(1000);
						table.fnReloadAjax();
					});
				}
				else{
					$('#message3').html(errorMsg(result.msg)).fadeIn(1000,function(){
						$("#updateWritingOption").scrollTop(0).animate(1000);
					});
				}
				
			},"json");
		});

		$('body').on('click','.delete',function(e){
			e.preventDefault();
			$('#message2').html('');
			$('#message3').html('');
			$('#message4').html('');
			var id = $(this).attr('id');
			$('#yesBtn').attr('ino', id);
			$('#loader4').show();
			$('body').on("click", "#yesBtn", function(e){
				var self = this;
				var id = $(self).attr('ino');
				var data = convertToObject(id);
				$(self).closest('#deleteWritingOption').find('button').prop('disabled', true);
				$.post("{$base}writingoptions/delete",data,function(result){
					$('#loader4').hide();
					$(self).hide();
					$(self).next().hide();
					$(self).closest('#deleteWritingOption').find('button').prop('disabled', false);
					if(result.err == 0){
						$('#message4').html(successMsg(result.msg)).fadeIn(1000,function(){
							$("#deleteWritingOption").scrollTop(0).animate(1000);
							table.fnReloadAjax();
						});
					}
					else{
						$('#message4').html(errorMsg(result.msg)).fadeIn(1000,function(){
							$("#deleteWritingOption").scrollTop(0).animate(1000);
						});
					}
				},"json");
			});	
			$('#deleteWritingOption').find('button').show();
			$('#deleteWritingOption').modal('show');
		});

		$('#deleteWritingOption').on('hidden.bs.modal', function (e) {
			$('#message2').html('');
			$('#message3').html('');
			$('#message4').html('');
		});

		$('#select-status').on('change', function(){
			location.href = "{$base}writingoptions?status=" + $(this).val();
		});
		
});	

</script>