<?php
@session_start();
@ini_set('display_errors', 'on');

$pageURL = 'http';
if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
if ($_SERVER["SERVER_PORT"] != "80") {
	$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
} else {
	$pageURL .= $_SERVER["SERVER_NAME"];
}

$base_url = $pageURL . str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

/* DEFINES */
define('IMG_DIR', $base_url.'wp-content/themes/twentyseventeen-child/systemapplications/images/');
// define('ROOT_IMG_DIR', $_SERVER['DOCUMENT_ROOT'].str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME'])."wp-content/themes/twentyseventeen-child/systemapplications/images/");
define('ROOT_IMG_DIR', getcwd()."/wp-content/themes/twentyseventeen-child/systemapplications/images/");
define('_CONTROLLER_DIR_', dirname(__FILE__).'/../applications/controllers/');
define('_MODEL_DIR_', dirname(__FILE__).'/../applications/models/');
define('_VIEW_DIR_', dirname(__FILE__).'/../applications/views/');
define('BASE',$base_url.'wp-content/themes/twentyseventeen-child');
define('BASE_URL',$base_url.'system-website/');
define('TEMPLATE_BASE_URL',  get_stylesheet_directory_uri());
function loadmyclass($className)
{	
	$classcontrollers = _CONTROLLER_DIR_;
	$classmodels = _MODEL_DIR_;	
	
	$file_in_controllers = file_exists($classcontrollers.$className.'.php');
	$file_in_models = file_exists($classmodels.$className.'.php');		
	
	if ($file_in_controllers) require_once($classcontrollers.$className.'.php');
	if ($file_in_models) require_once($classmodels.$className.'.php');	
}

spl_autoload_register('loadmyclass');


require_once(dirname(__FILE__).'/../core/router.php');
require_once(dirname(__FILE__).'/../core/url.php');

require_once(dirname(__FILE__).'/../smartyengine/libs/Smarty.class.php');
require_once(dirname(__FILE__).'/../core/model.php');
require_once(dirname(__FILE__).'/../core/view.php');
require_once(dirname(__FILE__).'/../core/controller.php');

require_once(dirname(__FILE__).'/../libraries/input.php');
require_once(dirname(__FILE__).'/../libraries/form_validation.php');
require_once(dirname(__FILE__).'/../libraries/session.php');
require_once(dirname(__FILE__).'/../libraries/upload.php');

