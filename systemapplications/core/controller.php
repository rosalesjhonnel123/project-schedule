<?php

class Controller {
	public $uri;
	public $view;
	public $session;
	public $input;
	public $form_validation;

	function __construct() {	
		$this->uri = new Url();	
		$this->view = new View();
		$this->session = new Session();
		$this->input = new Input();
		$this->form_validation = new Form_validation();
		$this->upload = new Upload();
		$this->view->assign('uri',$this->uri->segment(2));
		$this->view->assign('base', BASE_URL);	
		$this->view->assign('img_dir', IMG_DIR);
		$this->view->assign('template_base_url', TEMPLATE_BASE_URL);
	}

	public function redirect($location){
		header('Location:'.BASE_URL.$location);
	}

    protected function setCsvData($data, $filename)
    {
        header('Content-Type: application/xls');
        header('Content-Disposition: attachment; filename='.$filename);
        header('Pragma: no-cache');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        
        echo implode("\t", array_keys($data[0])) . "\r\n";
        //$file = fopen('php://output', 'w');
        //fputcsv($file, $rowHeader);
        foreach ($data as $row) {
            //fputcsv($file, $row);
            
            echo implode("\t", array_values($row)) . "\r\n";
        }
        //fclose($file);
    }
} 
?>