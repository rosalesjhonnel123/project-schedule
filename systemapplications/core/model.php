<?php
class Model {
    
    function __construct(){
    	global $wpdb;
        $this->db = $wpdb;
        $this->input = new Input();
        $this->session = new Session();
    }

    public function where($data){
        $arr = array();
        $end = end($data);
        
        foreach($data as $k => $v){
            $val = $this->input->cleandata($v);
            if($val == $end){
                $and = "";
            }
            else{
                $and = " AND";
            }
            if(intval($val)){
                $val = $val;
            }      
            else{
                $val = "'{$val}'";
            }
            array_push($arr,$k."=".$val.$and);

        }
      return $arr;
    }

    public function get_results($tablename,$arr,$args){
    	$where = implode(' ',$this->where($arr));
        return $this->db->get_results("SELECT * FROM {$tablename} WHERE {$where} {$args}",OBJECT);	 
    }  

    public function redirect($location){
        header('Location:'.BASE_URL.$location);
    }

    public function truncate($text, $chars = 25) {
        $text = $text." ";
        $text = substr($text,0,$chars);
        //$text = substr($text,0,strrpos($text,' '));
        $text = $text."...";
        return $text;
    }
   
 } 
?>