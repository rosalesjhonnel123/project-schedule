
<?php

class View
{
    protected $smarty;
    protected $tplext = '.tpl';
    
    public function __construct()
    {
        $this->smarty = new Smarty();
        $this->smarty->setTemplateDir(_VIEW_DIR_);
    }
    
    public function assign($key, $value)
    {
        $this->smarty->assign($key, $value);
    }

    public function display($templateName){
        ini_set('include_path','wp-content/themes/twentyseventeen-child');
        include 'header.php';
        $this->smarty->display($templateName . $this->tplext);
        include 'footer.php';
    }
}
?>