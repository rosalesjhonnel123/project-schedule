<?php
class Form_validation{

	public function check_input($type,$str){
	
		switch($type){
			case 'email': return $this->valid_email($str);
			break;
			case 'text': return $this->valid_input($str);
			break;
			case 'number': return $this->valid_number($str);
			break;
		}
	
	}

	public function valid_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? False : True;
		
	}

	public function valid_input($str)
	{
		return ( ! preg_match("/^[a-zA-Z0-9.]*$/", $str)) ? False : True;
	}
	
	public function valid_number($str)
	{
		return ( ! preg_match("/^[0-9]*$/", $str)) ? False : True;
	}
	
}

?>