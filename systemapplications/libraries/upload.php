<?php
class Upload{


	public function process($file){
		$filename = $file["name"];
	
		$filename = explode('.', $filename);
		$filename = time().'.'.$filename[1];	
		$sourcePath = $file['tmp_name']; // Storing source path of the file in a variable
		$targetPath = ROOT_IMG_DIR . $filename; // Target path where file is to be stored
		$move = move_uploaded_file($sourcePath,$targetPath) ; 
		if($move){
			return $filename;
		}
		else{
			return false;
		}
		
	}

	public function isValidImage($file){
		$filename = $file["name"];
		$validextensions = array("jpeg", "jpg", "png");
		$temporary = explode(".", $filename);
		$file_extension = end($temporary);
		if ((($file["type"] == "image/png") || ($file["type"] == "image/jpg") || ($file["type"] == "image/jpeg")
		) && ($file["size"] < 1048576)//Approx. 100kb files can be uploaded.
		&& in_array($file_extension, $validextensions)) 
		{
			return true;
		}
		else{
			return false;
		}
	}

	public function check($file){
		$filename = $file["name"];
	
	
		if(file_exists(ROOT_IMG_DIR . $filename)){
			
			return true;
		}
		else{
			return false;
		}
	}
}

?>