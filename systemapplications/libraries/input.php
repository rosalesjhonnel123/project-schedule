<?php
class Input {

	public function cleandata($data){
	   $data = htmlspecialchars($data, ENT_IGNORE, 'utf-8');
	   $data = strip_tags($data);
	   $data = stripslashes($data);
	   $data = trim($data);
	   return $data;
	}

	public function cleandata2($data){	
	   $data = stripslashes($data);
	   $data = trim($data);
	   return $data;
	}
	public function post($index = NULL){

		return $this->cleandata($_POST[$index]);
	}
	public function get($index = NULL){
		
		return $this->cleandata($_GET[$index]);
	}

	public function file($index = NULL){
		
		return $_FILES[$index];
	}

	public function post2($index = NULL){

		return $this->cleandata2($_POST[$index]);
	}



}

?>