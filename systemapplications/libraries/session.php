<?php
class Session{

	public function set_userdata($key, $val){
		$_SESSION[$key] = $val; 
	}

	public function get_userdata($key){
		return $_SESSION[$key]; 
	}

	public function unset_userdata($key){
		unset($_SESSION[$key]);
	}

	public function destroy(){
		@session_start();

		// Unset all of the session variables.
		$_SESSION = array();

		// If it's desired to kill the session, also delete the session cookie.
		// Note: This will destroy the session, and not just the session data!
		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}
		
		// Finally, destroy the session.
		@session_destroy();	
	}

}
?>