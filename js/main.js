function convertToObject(data){
	return {id:data};
}

function convertToObject2(plate_size_id, finish_material_id){
	return {'plate_size_id': plate_size_id, 'finish_material_id': finish_material_id};
}

function successMsg(msg){
	return '<div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="msg"><strong>Success!</strong> '+ msg +'</span></div>';
}
function errorMsg(msg){
	return '<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="msg"><strong>Error!</strong> '+ msg +'</span></div>';
}
